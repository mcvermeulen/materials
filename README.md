# Materials for the Advanced Software Development module

## Development

### Installation

Install the dependencies in the local `node_modules/` folder.

```sh
npm install
npx browserslist@latest --update-db
```

### `npm start -- --reload`

Runs the app in the development mode. Open https://localhost:8080 to view it in the browser. You may want to add the certificate to the trust store.

The page will reload if you make edits. You will also see any lint errors in the console.

### `npm run build`

Builds a copy of the website in the `build/` folder.
