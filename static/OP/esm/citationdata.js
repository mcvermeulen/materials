export const citationData = {
  items: [
    {
      id: '6867189/L2HPPLN7',
      type: 'webpage',
      title: 'The Architecture of Open Source Applications - Elegance, Evolution and a Few Fearless Hacks',
      abstract:
        "Architects look at thousands of buildings during their training, and study critiques of those buildings written by masters. In contrast, most software developers only ever get to know a handful of large programs well — usually programs they wrote themselves — and never study the great programs of history. As a result, they repeat one another’s mistakes rather than building on one another’s successes.\n\nThis book’s goal is to change that. In it, the authors of twenty-five open source applications explain how their software is structured, and why. What are each program's major components? How do they interact? And what did their builders learn during their development? In answering these questions, the contributors to this book provide unique insights into how they think.",
      URL: 'https://www.aosabook.org/en/index.html',
      language: 'en',
      author: [
        {
          family: 'Brown',
          given: 'Amy',
        },
        {
          family: 'Wilson',
          given: 'Greg',
        },
      ],
      issued: {
        'date-parts': [[2014, 4, 21]],
      },
    },
    {
      id: '6867189/36G9SCWF',
      type: 'article',
      title: 'IEEE Reference Guide',
      publisher: 'IEEE Periodicals',
      URL: 'https://ieeeauthorcenter.ieee.org/wp-content/uploads/IEEE-Reference-Guide.pdf',
      language: 'en',
      issued: {
        'date-parts': [[2018]],
      },
    },
    {
      id: '6867189/YD7G37U3',
      type: 'webpage',
      title:
        'The Architecture of Open Source Applications - Volume II: Structure, Scale, and a Few More Fearless Hacks',
      abstract:
        "Architects look at thousands of buildings during their training, and study critiques of those buildings written by masters. In contrast, most software developers only ever get to know a handful of large programs well — usually programs they wrote themselves — and never study the great programs of history. As a result, they repeat one another’s mistakes rather than building on one another’s successes. \n\nThis second volume of The Architecture of Open Source Applications aims to change that. In it, the authors of twenty-four open source applications explain how their software is structured, and why. What are each program's major components? How do they interact? And what did their builders learn during their development? In answering these questions, the contributors to this book provide unique insights into how they think.",
      URL: 'https://www.aosabook.org/en/index.html',
      language: 'en',
      author: [
        {
          family: 'Brown',
          given: 'Amy',
        },
        {
          family: 'Wilson',
          given: 'Greg',
        },
      ],
      issued: {
        'date-parts': [[2012]],
      },
    },
    {
      id: '6867189/WWDFSZKG',
      type: 'article',
      title: 'ICT Research Methods - Non-functional test',
      publisher: 'Stichting HBO-i',
      URL: 'http://ictresearchmethods.nl/Non-functional_test',
      shortTitle: 'Non-functional test',
      language: 'en',
    },
  ],
};
