export const citationData = {
  items: [
    {
      id: '6867189/MRHU8VEY',
      type: 'article',
      title: 'NEN-ISO/IEC 25010',
      publisher: 'Nederlands Normalisatie-instituut',
      abstract:
        'Systems and software engineering - Systems and software Quality Requirements and Evaluation (SQuaRE) - System and software quality models (ISO/IEC 25010:2011,IDT)',
      URL: 'https://connect-1nen-1nl-1000147gi0017.stcproxy.han.nl/Standard/Detail/157265?compId=11254&collectionId=0',
      note: 'Normcommissie 381007 "Software and systems engineering"',
      language: 'en',
      issued: {
        'date-parts': [[2011, 3, 30]],
      },
    },
    {
      id: '6867189/KFBTV6FX',
      type: 'article-journal',
      title: 'The 4+1 View Model of architecture',
      'container-title': 'IEEE Software',
      page: '42-50',
      volume: '12',
      issue: '6',
      abstract:
        "The 4+1 View Model organizes a description of a software architecture using five concurrent views, each of which addresses a specific set of concerns. Architects capture their design decisions in four views and use the fifth view to illustrate and validate them. The logical view describes the design's object model when an object-oriented design method is used. To design an application that is very data driven, you can use an alternative approach to develop some other form of logical view, such as an entity-relationship diagram. The process view describes the design's concurrency and synchronization aspects. The physical view describes the mapping of the software onto the hardware and reflects its distributed aspect. The development view describes the software's static organization in its development environment.",
      DOI: '10.1109/52.469759',
      note: 'Conference Name: IEEE Software',
      author: [
        {
          family: 'Kruchten',
          given: 'P.B.',
        },
      ],
      issued: {
        'date-parts': [['1995', 11]],
      },
    },
    {
      id: '6867189/YD7G37U3',
      type: 'webpage',
      title:
        'The Architecture of Open Source Applications - Volume II: Structure, Scale, and a Few More Fearless Hacks',
      abstract:
        "Architects look at thousands of buildings during their training, and study critiques of those buildings written by masters. In contrast, most software developers only ever get to know a handful of large programs well — usually programs they wrote themselves — and never study the great programs of history. As a result, they repeat one another’s mistakes rather than building on one another’s successes. \n\nThis second volume of The Architecture of Open Source Applications aims to change that. In it, the authors of twenty-four open source applications explain how their software is structured, and why. What are each program's major components? How do they interact? And what did their builders learn during their development? In answering these questions, the contributors to this book provide unique insights into how they think.",
      URL: 'https://www.aosabook.org/en/index.html',
      language: 'en',
      author: [
        {
          family: 'Brown',
          given: 'Amy',
        },
        {
          family: 'Wilson',
          given: 'Greg',
        },
      ],
      issued: {
        'date-parts': [[2012]],
      },
    },
    {
      id: '6867189/L2HPPLN7',
      type: 'webpage',
      title: 'The Architecture of Open Source Applications - Elegance, Evolution and a Few Fearless Hacks',
      abstract:
        "Architects look at thousands of buildings during their training, and study critiques of those buildings written by masters. In contrast, most software developers only ever get to know a handful of large programs well — usually programs they wrote themselves — and never study the great programs of history. As a result, they repeat one another’s mistakes rather than building on one another’s successes.\n\nThis book’s goal is to change that. In it, the authors of twenty-five open source applications explain how their software is structured, and why. What are each program's major components? How do they interact? And what did their builders learn during their development? In answering these questions, the contributors to this book provide unique insights into how they think.",
      URL: 'https://www.aosabook.org/en/index.html',
      language: 'en',
      author: [
        {
          family: 'Brown',
          given: 'Amy',
        },
        {
          family: 'Wilson',
          given: 'Greg',
        },
      ],
      issued: {
        'date-parts': [[2014, 4, 21]],
      },
    },
    {
      id: '6867189/CSN8H2FB',
      type: 'article-journal',
      title: 'Weaving Together Requirements and Architectures',
      'container-title': 'IEEE Computer',
      page: '115–117',
      volume: '34',
      issue: '3',
      DOI: '10.1109/2.910904',
      language: 'en',
      author: [
        {
          family: 'Nuseibeh',
          given: 'Bashar',
        },
      ],
      issued: {
        'date-parts': [[2001]],
      },
    },
    {
      id: '6867189/KRJVJXEU',
      type: 'book',
      title: 'Software Architecture in Practice',
      publisher: 'Addison-Wesley Professional',
      'publisher-place': 'Upper Saddle River, NJ',
      'number-of-pages': '624',
      edition: '3',
      'event-place': 'Upper Saddle River, NJ',
      abstract:
        'The award-winning and highly influential   Software Architecture in Practice, Third Edition,   has been substantially revised to reflect the latest developments in the field. In a real-world setting, the book once again introduces the concepts and best practices of software architecture—how a software system is structured and how that system’s elements are meant to interact. Distinct from the details of implementation, algorithm, and data representation, an architecture holds the key to achieving system quality, is a reusable asset that can be applied to subsequent systems, and is crucial to a software organization’s business strategy.   The authors have structured this edition around the concept of architecture influence cycles. Each cycle shows how architecture influences, and is influenced by, a particular context in which architecture plays a critical role. Contexts include technical environment, the life cycle of a project, an organization’s business profile, and the architect’s professional practices. The authors also have greatly expanded their treatment of quality attributes, which remain central to their architecture philosophy—with an entire chapter devoted to each attribute—and broadened their treatment of architectural patterns.   If you design, develop, or manage large software systems (or plan to do so), you will find this book to be a valuable resource for getting up to speed on the state of the art.   Totally new material covers     Contexts of software architecture: technical, project, business, and professional    Architecture competence: what this means both for individuals and organizations    The origins of business goals and how this affects architecture    Architecturally significant requirements, and how to determine them    Architecture in the life cycle, including generate-and-test as a design philosophy; architecture conformance during implementation; architecture and testing; and architecture and agile development    Architecture and current technologies, such as the cloud, social networks, and end-user devices',
      ISBN: '978-0-321-81573-6',
      language: 'en',
      author: [
        {
          family: 'Bass',
          given: 'Len',
        },
        {
          family: 'Clements',
          given: 'Paul',
        },
        {
          family: 'Kazman',
          given: 'Rick',
        },
      ],
      issued: {
        'date-parts': [['2012', 10, 5]],
      },
    },
    {
      id: '6867189/7WZQK3F8',
      type: 'article',
      title: 'OMG Unified Modeling Language (OMG UML) Version 2.5.1',
      publisher: 'Object Modeling Group',
      URL: 'https://www.omg.org/spec/UML/2.5.1/PDF/',
      language: 'en',
      accessed: {
        'date-parts': [[2020, 1, 23]],
      },
    },
    {
      id: '6867189/S6V9BLQB',
      type: 'article-journal',
      title: 'Architectural Patterns Revisited – A Pattern Language',
      page: '39',
      URL:
        'https://www.semanticscholar.org/paper/Architectural-Patterns-Revisited-A-Pattern-Language-Avgeriou-Zdun/5acf5fcd4e5c18abe269341dfa14cc6356d1e3cf',
      language: 'en',
      author: [
        {
          family: 'Avgeriou',
          given: 'Paris',
        },
        {
          family: 'Zdun',
          given: 'Uwe',
        },
      ],
    },
    {
      id: '6867189/UY9Q86MZ',
      type: 'article',
      title: 'NEN-ISO/IEC/IEEE 42010:2011 en',
      publisher: 'Nederlands Normalisatie-instituut',
      abstract: 'Systems and software engineering -\nArchitecture description (ISO/IEC/IEEE\n42010:2011,IDT)',
      URL: 'https://connect-1nen-1nl-1000147gi0017.stcproxy.han.nl/Standard/Detail/166180?compId=11254&collectionId=0',
      language: 'en',
      issued: {
        'date-parts': [[2011, 12]],
      },
    },
    {
      id: '6867189/EQ64YP6T',
      type: 'paper-conference',
      title: 'Mature Architecting - A Survey about the Reasoning Process of Professional Architects',
      'container-title': '2011 Ninth Working IEEE/IFIP Conference on Software Architecture',
      publisher: 'IEEE',
      'publisher-place': 'Boulder, CO, USA',
      page: '260-269',
      event: '2011 9th Working IEEE/IFIP Conference on Software Architecture (WICSA)',
      'event-place': 'Boulder, CO, USA',
      URL: 'http://ieeexplore.ieee.org/document/5959780/',
      DOI: '10.1109/WICSA.2011.42',
      ISBN: '978-1-61284-399-5',
      language: 'en',
      author: [
        {
          family: 'Heesch',
          given: 'Uwe van',
        },
        {
          family: 'Avgeriou',
          given: 'Paris',
        },
      ],
      issued: {
        'date-parts': [['2011']],
      },
      accessed: {
        'date-parts': [[2020, 7, 29]],
      },
    },
    {
      id: '6867189/B9TSISWM',
      type: 'article-journal',
      title: 'A documentation framework for architecture decisions',
      'container-title': 'Journal of Systems and Software',
      page: '795-820',
      volume: '85',
      issue: '4',
      abstract:
        'In this paper, we introduce a documentation framework for architecture decisions. This framework consists of four viewpoint deﬁnitions using the conventions of ISO/IEC/IEEE 42010, the new international standard for the description of system and software architectures. The four viewpoints, a Decision Detail viewpoint, a Decision Relationship viewpoint, a Decision Chronology viewpoint, and a Decision Stakeholder Involvement viewpoint satisfy several stakeholder concerns related to architecture decision management.',
      URL: 'https://linkinghub.elsevier.com/retrieve/pii/S0164121211002755',
      DOI: '10.1016/j.jss.2011.10.017',
      journalAbbreviation: 'Journal of Systems and Software',
      language: 'en',
      author: [
        {
          family: 'van Heesch',
          given: 'U.',
        },
        {
          family: 'Avgeriou',
          given: 'P.',
        },
        {
          family: 'Hilliard',
          given: 'R.',
        },
      ],
      issued: {
        'date-parts': [['2012']],
      },
      accessed: {
        'date-parts': [[2020, 7, 29]],
      },
    },
    {
      id: '6867189/KSEUCHYI',
      type: 'article-journal',
      title: 'Decision-Centric Architecture Reviews',
      'container-title': 'IEEE Software',
      page: '69-76',
      volume: '31',
      issue: '1',
      URL: 'http://ieeexplore.ieee.org/document/6449237/',
      DOI: '10.1109/MS.2013.22',
      journalAbbreviation: 'IEEE Softw.',
      language: 'en',
      author: [
        {
          family: 'van Heesch',
          given: 'Uwe',
        },
        {
          family: 'Eloranta',
          given: 'Veli-Pekka',
        },
        {
          family: 'Avgeriou',
          given: 'Paris',
        },
        {
          family: 'Koskimies',
          given: 'Kai',
        },
        {
          family: 'Harrison',
          given: 'Neil',
        },
      ],
      issued: {
        'date-parts': [['2014']],
      },
      accessed: {
        'date-parts': [[2020, 7, 29]],
      },
    },
    {
      id: '6867189/LIMQKXRF',
      type: 'article-journal',
      title: 'Architecture Decisions: Demystifying Architecture',
      'container-title': 'IEEE Software',
      page: '19-27',
      volume: '22',
      issue: '2',
      URL: 'http://ieeexplore.ieee.org/document/1407822/',
      DOI: '10.1109/MS.2005.27',
      shortTitle: 'Architecture Decisions',
      journalAbbreviation: 'IEEE Softw.',
      language: 'en',
      author: [
        {
          family: 'Tyree',
          given: 'J.',
        },
        {
          family: 'Akerman',
          given: 'A.',
        },
      ],
      issued: {
        'date-parts': [['2005']],
      },
      accessed: {
        'date-parts': [[2020, 7, 29]],
      },
    },
    {
      id: '6867189/JB42UD4J',
      type: 'paper-conference',
      title: 'A Comparative Analysis of Architecture Frameworks',
      'container-title': '11th Asia-Pacific Software Engineering Conference',
      publisher: 'IEEE',
      'publisher-place': 'Busan, Korea',
      page: '640-647',
      event: '11th Asia-Pacific Software Engineering Conference',
      'event-place': 'Busan, Korea',
      abstract:
        'Architecture frameworks are methods used in architecture modeling. They provide a structured and systematic approach to designing systems. To date there has been little analysis on their roles in system and software engineering and if they are satisfactory. This study provides a model of understanding through analyzing the goals, inputs and outcomes of six Architecture Frameworks. It characterizes two classes of architecture frameworks and identifies some of their deficiencies. To overcome these deficiencies, we propose to use costs, benefits and risks for architecture analysis. We also propose a method to delineate architecture activities from detailed design activities.',
      URL: 'http://ieeexplore.ieee.org/document/1371981/',
      DOI: '10.1109/APSEC.2004.2',
      ISBN: '978-0-7695-2245-6',
      language: 'en',
      author: [
        {
          family: 'Tang',
          given: 'A.',
        },
        {
          family: 'Jun Han',
          given: '',
        },
        {
          family: 'Pin Chen',
          given: '',
        },
      ],
      issued: {
        'date-parts': [[2004]],
      },
      accessed: {
        'date-parts': [[2020, 7, 29]],
      },
    },
    {
      id: '6867189/47FIAVVC',
      type: 'article-journal',
      title: 'What do software architects really do?',
      'container-title': 'Journal of Systems and Software',
      page: '2413-2416',
      volume: '81',
      issue: '12',
      abstract:
        'To be successful, a software architect—or a software architecture team, collectively—must strike a delicate balance between an external focus—both outwards: Listening to customers, users, watching technology, developing a long-term vision, and inwards: driving the development teams—and an internal, reﬂective focus: spending time to make the right design choices, validating them, and documenting them. Teams that stray too far away from this metastable equilibrium fall into some traps that we describe as antipatterns of software architecture teams.',
      URL: 'https://linkinghub.elsevier.com/retrieve/pii/S0164121208002057',
      DOI: '10.1016/j.jss.2008.08.025',
      journalAbbreviation: 'Journal of Systems and Software',
      language: 'en',
      author: [
        {
          family: 'Kruchten',
          given: 'Philippe',
        },
      ],
      issued: {
        'date-parts': [['2008']],
      },
      accessed: {
        'date-parts': [[2020, 7, 29]],
      },
    },
    {
      id: '6867189/B7CWPDNZ',
      type: 'article-journal',
      title: 'A general model of software architecture design derived from five industrial approaches',
      'container-title': 'Journal of Systems and Software',
      page: '106-126',
      volume: '80',
      issue: '1',
      abstract:
        'We compare ﬁve industrial software architecture design methods and we extract from their commonalities a general software architecture design approach. Using this general approach, we compare across the ﬁve methods the artifacts and activities they use or recommend, and we pinpoint similarities and diﬀerences. Once we get beyond the great variance in terminology and description, we ﬁnd that the ﬁve approaches have a lot in common and match more or less the ‘‘ideal’’ pattern we introduced. From the ideal pattern we derive an evaluation grid that can be used for further method comparisons.',
      URL: 'https://linkinghub.elsevier.com/retrieve/pii/S0164121206001634',
      DOI: '10.1016/j.jss.2006.05.024',
      journalAbbreviation: 'Journal of Systems and Software',
      language: 'en',
      author: [
        {
          family: 'Hofmeister',
          given: 'Christine',
        },
        {
          family: 'Kruchten',
          given: 'Philippe',
        },
        {
          family: 'Nord',
          given: 'Robert L.',
        },
        {
          family: 'Obbink',
          given: 'Henk',
        },
        {
          family: 'Ran',
          given: 'Alexander',
        },
        {
          family: 'America',
          given: 'Pierre',
        },
      ],
      issued: {
        'date-parts': [['2007']],
      },
      accessed: {
        'date-parts': [[2020, 7, 29]],
      },
    },
    {
      id: '6867189/ZFEMEPAK',
      type: 'article-journal',
      title: 'Using Patterns to Capture Architectural Decisions',
      'container-title': 'IEEE Software',
      page: '38-45',
      volume: '24',
      issue: '4',
      URL: 'https://ieeexplore.ieee.org/document/4267601/',
      DOI: '10.1109/MS.2007.124',
      journalAbbreviation: 'IEEE Softw.',
      language: 'en',
      author: [
        {
          family: 'Harrison',
          given: 'Neil B.',
        },
        {
          family: 'Avgeriou',
          given: 'Paris',
        },
        {
          family: 'Zdun',
          given: 'Uwe',
        },
      ],
      issued: {
        'date-parts': [['2007']],
      },
      accessed: {
        'date-parts': [[2020, 7, 29]],
      },
    },
    {
      id: '6867189/E6ISX9PI',
      type: 'paper-conference',
      title: 'Forces on Architecture Decisions - A Viewpoint',
      'container-title':
        '2012 Joint Working IEEE/IFIP Conference on Software Architecture and European Conference on Software Architecture',
      publisher: 'IEEE',
      'publisher-place': 'Helsinki, Finland',
      page: '101-110',
      event:
        '2012 Joint Working IEEE/IFIP Conference on Software Architecture (WICSA) & European Conference on Software Architecture (ECSA)',
      'event-place': 'Helsinki, Finland',
      abstract:
        'In this paper, the notion of forces as inﬂuences upon architecture decisions is introduced. To facilitate the documentation of forces as a part of architecture descriptions, we specify a decision forces viewpoint, which extends our existing framework for architecture decisions, following the conventions of the international architecture description standard ISO/IEC/IEEE 42010. The applicability of the viewpoint was validated in three case studies, in which senior software engineering students used it to document decisions in software projects; two of which conducted for industrial customers. The results show that the forces viewpoint is a well-received documentation approach, satisfying stakeholder concerns related to traceability between decision forces and architecture decisions.',
      URL: 'http://ieeexplore.ieee.org/document/6337708/',
      DOI: '10.1109/WICSA-ECSA.212.18',
      ISBN: '978-1-4673-2809-8 978-0-7695-4827-2',
      language: 'en',
      author: [
        {
          family: 'van Heesch',
          given: 'Uwe',
        },
        {
          family: 'Avgeriou',
          given: 'Paris',
        },
        {
          family: 'Hilliard',
          given: 'Rich',
        },
      ],
      issued: {
        'date-parts': [['2012']],
      },
      accessed: {
        'date-parts': [[2020, 7, 29]],
      },
    },
    {
      id: '6867189/NEDCRJJH',
      type: 'report',
      title: 'ATAM: Method for Architecture Evaluation:',
      publisher: 'Defense Technical Information Center',
      'publisher-place': 'Fort Belvoir, VA',
      'event-place': 'Fort Belvoir, VA',
      URL: 'http://www.dtic.mil/docs/citations/ADA382629',
      note: 'DOI: 10.21236/ADA382629',
      shortTitle: 'ATAM',
      language: 'en',
      author: [
        {
          family: 'Kazman',
          given: 'Rick',
        },
        {
          family: 'Klein',
          given: 'Mark',
        },
        {
          family: 'Clements',
          given: 'Paul',
        },
      ],
      issued: {
        'date-parts': [[2000, 8, 1]],
      },
      accessed: {
        'date-parts': [[2020, 7, 29]],
      },
    },
    {
      id: '6867189/5T3IGG5H',
      type: 'book',
      title: 'Pattern-Oriented Software Architecture - Volume 1: A System of Patterns',
      publisher: 'Wiley Publishing',
      abstract:
        "Pattern-Oriented Software Architecture: A System of Patterns looks at how patterns occur on three different levels--in software architecture, in everyday design, and in idioms (which describe how a particular design pattern is implemented in a programming language like C++). This synthetic approach is a little theoretical at times, but the authors also present over a dozen patterns and provide real-world examples wherever possible. For architectural patterns, the authors look at the Layers pattern, used in operating systems such as Windows NT and virtual machines. They also consider Pipes and Filters, which process streams of data. (This pattern, the authors point out, is a lynchpin of Unix.) Their Blackboard pattern shows how a complex problem, such as image or speech recognition can be broken up into smaller, specialized subsystems that work together to solve a problem. (For recognizing words from a raw waveform input, a Blackboard approach might have separate processes to find phonemes, then words, then sentences.) This book also looks at today's distributed systems in considering the Broker pattern, which is used on the Internet and in Microsoft's OLE technology. This section also presents several powerful patterns for building effective graphical user interfaces, such as Model-View-Controller. The authors define several well-known design patterns, such as the Proxy and Command patterns, and also basic, far-reaching patterns, such as Whole-Part and Master-Slave, which are widely used throughout computing. Their survey ends with a discussion on the way objects can communicate (using such patterns as Forwarder-Receiver, Client-Dispatcher-Server, and Publisher-Subscriber), which many developers will recognize as familiar patterns, but are codified here as \"official\" patterns. The book then discusses some idioms in C++ and a more far-reaching role for patterns in software design and architecture. By fitting patterns into traditional software engineering practices, the authors of Pattern-Oriented Software Architecture successfully argue that the role for patterns will only continue to diversify and enrich tomorrow's software engineering tools and methodologies. --Richard Dragan",
      ISBN: '978-0-471-95869-7',
      shortTitle: 'Pattern-Oriented Software Architecture - Volume 1',
      author: [
        {
          family: 'Buschmann',
          given: 'Frank',
        },
        {
          family: 'Meunier',
          given: 'Regine',
        },
        {
          family: 'Rohnert',
          given: 'Hans',
        },
        {
          family: 'Sommerlad',
          given: 'Peter',
        },
        {
          family: 'Stal',
          given: 'Michael',
        },
      ],
      issued: {
        'date-parts': [[1996]],
      },
    },
  ],
};
