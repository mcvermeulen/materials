# Slidedecks

Vanwege een verschillend tijdschema van DT lessen t.o.v. de VT lessen worden de slidedecks in een andere volgorde gebruikt.

## Informatie

- In de directory "Voltijd" staan de slidedecks die worden gebruikt voor de voltijdopleiding. Deze dienen als informatiebron voor zelfstudie en vormen de basis voor de slides gebruikt tijdens de lessen.
- In de directory "Deeltijd" staan de slidedecks die gebruikt worden in de lessen. Deze dienen als gespreksonderwerpen tijdens de les en de bijbehorende demo applicatie. 

## Demo applicatie

Tijdens de les wordt een demo applicatie gebruikt. Dit is een webapplicatie die enkele jaren in een productieomgeving heeft gedraaid.
Voor deze applicatie bestaat nog geen software architectuur en gaan we zoveel als mogelijk reconstrueren tijdens de lessen.

## Schema

In onderstaand schema is te zien in welke lesweek de voltijd slidedecks worden gebruikt en welke hoofdstukken uit **Software Architecture in Practice** erbij horen.

|        |  | Topics                                          | Slide deck |
|--------|--|-------------------------------------------------|------------|
| Week 1 |  | Introduction Software Architecture Course       | swa_intro  |
|        |  | Introduction Software Architecture              | swa_1      |
|        |  |           VISION, SYSTEM OVERVIEW               |            |
|        |  | Definition of Software Architecture             | swa_2      |
| Week 3 |  | Architecturally significant requirements 1      | swa_3      |
|        |  |           QUALITY ATTRIBUTES		              |            |
|        |  |           HIGH LEVEL USE CASES		          |            |
|        |  | Architecturally significant requirements 2      | swa_4      |
|        |  |           QUALITY ATTRIBUTE SCENARIOS		      |            |
|        |  |           USE CASE SCENARIOS                    |            |
| Week 2 |  | General scenarios and Hofmeisters Model         | swa_5      |
|        |  |           QUALITY ATTRIBUTE SCENARIOS           |            |
|        |  | Architectural Analysis and Synthesis            | swa_6      |
|        |  |           STAKEHOLDERS                          |            |
|        |  |           CONCERNS                              |            |
|        |  |           HIGH LEVEL USE CASES                  |            |
|        |  |           INTERFACE REQUIREMENTS                |            |
|        |  |           TECHNICAL CONSTRAINTS                 |            |
|        |  | Architectural Evaluation/ SWE research          | swa_7      |
| Week 5 |  | Architectural Patterns                          | swa_8      |
|        |  | Architectural Patterns and Tactics              | swa_9      |
| Week 4 |  | Architectural Patterns/ Architecture Frameworks | swa_10     |
|        |  | The 4+1 View Model                              | swa_11     |
|        |  |           Logical View                          |            |
|        |  |           Implementation View                   |            |
|        |  |           Process View                          |            |
|        |  |           Deployment View                       |            |
|        |  | C4 Model                                        | swa_11     |
| Week 6 |  | Architecture Decision Documentation             | swa_12     |
|        |  |           Decision-Forces View                  |            |
|        |  |           Decision-Relationship View            |            |
|        |  |           Decision Detail View                  |            |
|        |  | Architecture Evaluation: ATAM & DCAR            | swa_13     |
