import { componentExercise } from './component_exercise.js';
import { componentHeading } from './component_heading.js';
import { componentIntendedlearningoutcome } from './component_intendedlearningoutcome.js';
import { componentGradingcriteria } from './component_gradingcriteria.js';
import { componentGradingcriterion } from './component_gradingcriterion.js';
import { componentIntendedlearningoutcomes } from './component_intendedlearningoutcomes.js';
import { componentMiroboard } from './component_miroboard.js';
import { componentSection } from './component_section.js';
import { componentTableofcontents } from './component_tableofcontents.js';
import { componentVideo } from './component_video.js';
import { renderAll } from './render_all.js';
import { locale } from './citationlocale.js';
import { citationstyleIeee } from './citationstyle.js';
import { processCitations } from './process_citations.js';
import '../css/style.css';

export const renderDocument = (citationData, urlBase) => {
  window.addEventListener(
    'DOMContentLoaded',
    () => {
      componentSection(); // TODO: order dependence
      componentTableofcontents();
      componentIntendedlearningoutcome();
      componentIntendedlearningoutcomes();
      componentGradingcriterion();
      componentGradingcriteria();
      componentVideo();
      componentMiroboard();
      componentExercise();
      componentHeading();
      if (citationData.items.length > 0) {
        processCitations(citationData, locale, citationstyleIeee);
      }
      const elementBody = document.body;
      renderAll(elementBody, urlBase);
    },
    false,
  );
};
