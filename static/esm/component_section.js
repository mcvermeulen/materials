import { LitElement, html, css } from 'lit-element';

class Section extends LitElement {
  constructor() {
    super();
    this.level = 0;
    this.index = 1;
    this.title = 'Untitled';
    this.extra = false;
    this._invoked = 0;
  }

  static get properties() {
    return { title: { type: String }, level: { type: Number }, index: { type: String }, extra: { type: Boolean } };
  }

  static get styles() {
    // TODO: inherit styles
    return [
      css`
        *,
        ::before {
          box-sizing: border-box;
        }

        /* TODO: fix */
        :host(:target),
        :target {
          animation-duration: 2s;
          animation-iteration-count: 1;
          animation-name: fadeInOpacity;
          animation-timing-function: ease-in;
          background-color: lightgreen;
          opacity: 1;
        }

        .extra {
          background-color: var(--color-extra-bg);
          margin: 10px;
          max-width: fit-content;
          min-width: 100%;
          padding: 5px;
        }

        .extra::after {
          bottom: 0;
          content: '🧐';
          font-size: 2rem;
          position: relative;
          right: 10px;
        }

        @keyframes fadeInOpacity {
          0% {
            opacity: 0;
          }
          100% {
            opacity: 1;
          }
        }
      `,
    ];
  }

  render() {
    // TODO: elementHeading.innerText.replace(regexpId, '_');
    return html`<section class="${this.extra ? 'extra' : ''}">
      <component-heading exportparts="a" level="${this.level}" index=${this.index}>
        <slot name="title"></slot>
      </component-heading>
      <slot></slot>
    </section>`;
  }
}

export const componentSection = () => {
  customElements.define('component-section', Section);
};
