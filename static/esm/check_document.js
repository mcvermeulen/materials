export const checkDocument = elementBody => {
  // Lessons can't be within lessons, nor can units be within units or lessons.
  const elementInvalidnesting = elementBody.querySelector(
    ':scope [data-les] [data-les], :scope [data-eenheid] [data-eenheid], :scope [data-les] [data-eenheid]',
  );
  if (elementInvalidnesting) {
    throw new Error('Invalid nesting detected');
  }
};
