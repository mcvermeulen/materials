import { LitElement, html, css } from 'lit-element';
import { classMap } from 'lit-html/directives/class-map';

class Tableofcontents extends LitElement {
  constructor() {
    super();
    this.sectionRoot = document.querySelector('body main > component-section');
    this.nElementDetails = document.querySelectorAll('body details');
    this.active = false;
    this.classes = { active: this.active };
  }

  connectedCallback() {
    super.connectedCallback();
    this._locationHashChanged = this._locationHashChanged.bind(this);
    window.addEventListener('hashchange', this._locationHashChanged);
  }

  disconnectedCallback() {
    window.removeEventListener('hashchange', this._locationHashChanged);
    super.disconnectedCallback();
  }

  static templateEntryLeaf(section, index, level) {
    let indexParent = '';
    if (section.parentElement) {
      if (section.parentElement.index) {
        indexParent = `${section.parentElement.index}.`;
      }
    }
    // TODO: clean up
    if (index && level > 2) {
      // eslint-disable-next-line no-param-reassign
      section.index = `${indexParent}${index}`;
      // eslint-disable-next-line no-param-reassign
      section.id = `sec_${section.index}`;
    } else if (index && level > 1) {
      // eslint-disable-next-line no-param-reassign
      section.index = `${index}`;
      // eslint-disable-next-line no-param-reassign
      section.id = `sec_${section.index}`;
    }
    // eslint-disable-next-line no-param-reassign
    section.level = level;
    let title = '';
    if (section.firstElementChild && section.firstElementChild.getAttribute('slot')) {
      title = section.firstElementChild.cloneNode(true);
    }
    return html`<p>
      <a part="a" href="#${section.id}">
        <span class="locator_toc">${section.index}</span>
        ${title}</a
      >
    </p>`;
  }

  templateEntry(section, index, level) {
    const nElementSubentry = Array.from(section.querySelectorAll(':scope > component-section'));
    // TODO: Use functional style.
    let elementEntry = html``;
    if (section === this.sectionRoot) {
      // ToC root is not a level
      let indexChild = 0;
      elementEntry = html`<li>
        <ol>
          ${nElementSubentry.map(elementSubentry => {
            indexChild += 1;
            return html`${this.templateEntry(elementSubentry, indexChild, level)}`;
          })}
        </ol>
      </li>`;
    } else if (nElementSubentry.length > 0) {
      let indexChild = 0;
      const levelNew = level + 1;
      elementEntry = html`<li>
        <details>
          <summary>${Tableofcontents.templateEntryLeaf(section, index, level)}</summary>
          <ol>
            ${nElementSubentry.map(elementSubentry => {
              indexChild += 1;
              return html`${this.templateEntry(elementSubentry, indexChild, levelNew)}`;
            })}
          </ol>
        </details>
      </li>`;
    } else if (index) {
      elementEntry = html`<li>${Tableofcontents.templateEntryLeaf(section, index, level)}</li>`;
    }
    return html`<ol>
      ${elementEntry}
    </ol>`;
  }

  static get properties() {
    return { active: { type: Boolean } };
  }

  static get styles() {
    return [
      css`
        #buttons {
          align-items: space-between;
          margin: 0.2em;
        }

        nav:not(.active) #buttons {
          display: flex;
          flex-direction: column;
        }

        button {
          background-color: transparent;
          border: 1px solid WindowFrame;
          color: inherit;
          font: inherit;
          font-size: 1.5em;
          letter-spacing: inherit;
          margin: 0.1em;
          padding: 0.25em 0.375em;
        }

        nav {
          align-items: center;
          backdrop-filter: blur(15px);
          background-color: rgba(255, 255, 255, 0.3);
          box-shadow: 2px 0 5px 1px #aaa;
          display: flex;
          flex-direction: column;
          height: 100%;
          left: 0;
          max-width: max-content;
          min-width: min-content;
          overflow: hidden scroll;
          position: fixed;
          scrollbar-width: thin;
          top: 0;
          z-index: 1;
        }

        .locator_toc {
          font-weight: bold;
        }

        nav > #tableofcontents {
          display: none;
          margin: 1em;
        }

        nav > #tableofcontents > p {
          margin-left: 1em;
        }

        nav.active > #tableofcontents {
          display: revert;
        }

        ol {
          list-style: none;
          margin-left: 0;
          padding-left: 0;
        }

        ol ol {
          margin-left: 0.5em;
          padding-left: 0;
        }

        summary::-webkit-details-marker {
          color: gray;
        }

        details > summary {
          cursor: pointer;
          line-height: 1.5;
          width: 100%;
        }

        details[open] > summary ~ * {
          animation: sweep 0.2s ease-in;
        }

        details > summary:focus {
          outline: unset;
        }

        @keyframes sweep {
          0% {
            margin-left: -100%;
          }

          100% {
            margin-left: 0;
          }
        }
      `,
    ];
  }

  _handleClick() {
    this.classes.active = !this.active;
    this.active = !this.active;
  }

  _handleClickUnfold() {
    for (const elementDetails of this.nElementDetails) {
      elementDetails.setAttribute('open', '');
    }
  }

  _handleClickFold() {
    for (const elementDetails of this.nElementDetails) {
      elementDetails.removeAttribute('open');
    }
  }

  _locationHashChanged() {
    this.classes.active = false;
    this.active = false;
  }

  render() {
    // TODO: stylelint whitelisting vs. all custom components
    // TODO: Handle case where `sectionRoot` is `null` or >1.
    return html`<nav class="${classMap(this.classes)}">
      <div id="buttons">
        <button type="button" @click="${this._handleClick}" title="Menu">☰</button>
        <button type="button" @click="${this._handleClickUnfold}" title="Alles uitklappen">📖</button>
        <button type="button" @click="${this._handleClickFold}" title="Alles inklappen">📕</button>
      </div>
      <div id="tableofcontents">
        <p>
          <a href="..">HOME</a>
        </p>
        ${this.templateEntry(this.sectionRoot, 0, 2)}
      </div>
    </nav>`;
  }
}

export const componentTableofcontents = () => {
  customElements.define('component-tableofcontents', Tableofcontents);
};
