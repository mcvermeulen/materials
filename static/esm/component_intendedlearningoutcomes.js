import { LitElement, html, css } from 'lit-element';

class Intendedlearningoutcomes extends LitElement {
  static get styles() {
    return [
      css`
        *,
        ::before {
          box-sizing: border-box;
        }
      `,
    ];
  }

  render() {
    return html`<p>Je …</p>
      <ol>
        <slot></slot>
      </ol>`;
  }
}

export const componentIntendedlearningoutcomes = () => {
  customElements.define('component-intendedlearningoutcomes', Intendedlearningoutcomes);
};
