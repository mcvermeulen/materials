export const renderLesson = elementBody => {
  const nElementLesSummary = elementBody.querySelectorAll(':scope [data-les]');
  let counterLes = 1;
  for (const elementLesSummary of nElementLesSummary) {
    const elementLes = document.createElement('p');
    elementLes.innerText = `📖 Les ${counterLes}`;
    elementLes.classList.add('textalign_center');
    elementLesSummary.prepend(elementLes);
    counterLes += 1;
  }
};
