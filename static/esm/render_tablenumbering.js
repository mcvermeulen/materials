export const renderTablenumbering = elementBody => {
  const nElementTablecaption = elementBody.querySelectorAll(':scope table caption');
  let countTable = 1;
  for (const elementTablecaption of nElementTablecaption) {
    elementTablecaption.prepend(`Tabel ${countTable}: `);
    countTable += 1;
  }
};
