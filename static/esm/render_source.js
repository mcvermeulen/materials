export const renderSource = elementBody => {
  const nElementBron = elementBody.querySelectorAll(':scope .bron');
  for (const elementBron of nElementBron) {
    elementBron.prepend('ℹ️ ');
  }
};
