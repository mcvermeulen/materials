import 'sanitize.css/evergreen.css';
import 'sanitize.css/forms.evergreen.css';
import 'sanitize.css/assets.css';
import 'sanitize.css/reduce-motion.css';
import 'sanitize.css/typography.css';

import Prism from 'prismjs';
import { checkDocument } from './check_document.js';
// TODO:
// import { renderLesson } from './render_lesson.js';
import { renderSource } from './render_source.js';
import { renderPractice } from './render_practice.js';
import { renderTablenumbering } from './render_tablenumbering.js';
import { renderFigurenumbering } from './render_figurenumbering.js';
import { locationHashChanged } from './fragment_scroller.js';
import 'prismjs/themes/prism-dark.css';
import 'prismjs/components/prism-java';
import 'prismjs/components/prism-antlr4';
import 'prismjs/plugins/line-highlight/prism-line-highlight.css';
import 'prismjs/plugins/line-highlight/prism-line-highlight.js';
import 'prismjs/plugins/toolbar/prism-toolbar.js';
import 'prismjs/plugins/show-language/prism-show-language.js';
import 'prismjs/plugins/toolbar/prism-toolbar.css';
import { renderMath } from './render_math.js';

export const renderAll = (elementBody, urlBase) => {
  checkDocument(elementBody);
  // TODO:
  // renderLesson(elementBody);
  renderPractice(elementBody);
  renderSource(elementBody);
  renderTablenumbering(elementBody);
  renderFigurenumbering(elementBody);
  Prism.highlightAll();
  renderMath(urlBase);
  const elementMain = elementBody.querySelector('main');
  elementMain.hidden = false;
  locationHashChanged();
  window.addEventListener('hashchange', locationHashChanged);
};
