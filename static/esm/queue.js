export class Queue {
  constructor() {
    this.nItem = [];
  }

  enqueue(item) {
    this.nItem.push(item);
  }

  dequeue() {
    return this.nItem.shift();
  }

  peek() {
    return this.nItem[0];
  }

  get size() {
    return this.nItem.length;
  }

  [Symbol.iterator]() {
    return this;
  }

  next() {
    const item = this.dequeue();
    if (item) {
      return { done: false, value: item };
    }
    return { done: true };
  }
}
