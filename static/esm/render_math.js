import 'mathjax-full/ts/output/chtml/fonts/tex-woff-v2/MathJax_Fraktur-Regular.woff';
import 'mathjax-full/ts/output/chtml/fonts/tex-woff-v2/MathJax_Main-Bold.woff';
import 'mathjax-full/ts/output/chtml/fonts/tex-woff-v2/MathJax_Math-Regular.woff';
import 'mathjax-full/ts/output/chtml/fonts/tex-woff-v2/MathJax_SansSerif-Regular.woff';
import 'mathjax-full/ts/output/chtml/fonts/tex-woff-v2/MathJax_Size1-Regular.woff';
import 'mathjax-full/ts/output/chtml/fonts/tex-woff-v2/MathJax_Zero.woff';
import 'mathjax-full/ts/output/chtml/fonts/tex-woff-v2/MathJax_Calligraphic-Regular.woff';
import 'mathjax-full/ts/output/chtml/fonts/tex-woff-v2/MathJax_Calligraphic-Bold.woff';
import 'mathjax-full/ts/output/chtml/fonts/tex-woff-v2/MathJax_Fraktur-Bold.woff';
import 'mathjax-full/ts/output/chtml/fonts/tex-woff-v2/MathJax_Vector-Bold.woff';
import 'mathjax-full/ts/output/chtml/fonts/tex-woff-v2/MathJax_Vector-Regular.woff';
import 'mathjax-full/ts/output/chtml/fonts/tex-woff-v2/MathJax_Math-Italic.woff';
import 'mathjax-full/ts/output/chtml/fonts/tex-woff-v2/MathJax_AMS-Regular.woff';
import 'mathjax-full/ts/output/chtml/fonts/tex-woff-v2/MathJax_Main-Regular.woff';
import 'mathjax-full/ts/output/chtml/fonts/tex-woff-v2/MathJax_Main-Italic.woff';
import 'mathjax-full/ts/output/chtml/fonts/tex-woff-v2/MathJax_SansSerif-Italic.woff';
import 'mathjax-full/ts/output/chtml/fonts/tex-woff-v2/MathJax_Size4-Regular.woff';
import 'mathjax-full/ts/output/chtml/fonts/tex-woff-v2/MathJax_Size3-Regular.woff';
import 'mathjax-full/ts/output/chtml/fonts/tex-woff-v2/MathJax_Script-Regular.woff';
import 'mathjax-full/ts/output/chtml/fonts/tex-woff-v2/MathJax_SansSerif-Bold.woff';
import 'mathjax-full/ts/output/chtml/fonts/tex-woff-v2/MathJax_Math-BoldItalic.woff';
import 'mathjax-full/ts/output/chtml/fonts/tex-woff-v2/MathJax_Size2-Regular.woff';
import 'mathjax-full/ts/output/chtml/fonts/tex-woff-v2/MathJax_Typewriter-Regular.woff';
import mathjax from 'mathjax-full/js/mathjax';
import TeX from 'mathjax-full/js/input/tex';
import CHTML from 'mathjax-full/js/output/chtml';
import AllPackages from 'mathjax-full/js/input/tex/AllPackages';
import browserAdaptor from 'mathjax-full/js/adaptors/browserAdaptor';
import RegisterHTMLHandler from 'mathjax-full/js/handlers/html';

export const renderMath = urlBase => {
  RegisterHTMLHandler.RegisterHTMLHandler(browserAdaptor.browserAdaptor());
  const html = mathjax.mathjax.document(document, {
    InputJax: new TeX.TeX({
      packages: AllPackages.AllPackages,
      macros: { require: ['', 1] },
    }),
    OutputJax: new CHTML.CHTML({
      fontURL: `${urlBase}/web_modules/mathjax-full/ts/output/chtml/fonts/tex-woff-v2/`,
    }),
  });
  // TODO: these settings appear not to work. E.g. the menu does not work.
  window.MathJax = {
    version: mathjax.mathjax.version,
    html,
    options: {
      enableMenu: true,
      menuOptions: {
        settings: {
          texHints: true,
          semantics: false,
          zoom: 'NoZoom',
          zscale: '200%',
          renderer: 'CHTML',
          alt: false,
          cmd: false,
          ctrl: false,
          shift: false,
          scale: 1,
          collapsible: false,
          inTabOrder: true,
        },
        annotationTypes: {
          TeX: ['TeX', 'LaTeX', 'application/x-tex'],
          StarMath: ['StarMath 5.0'],
          Maple: ['Maple'],
          ContentMathML: ['MathML-Content', 'application/mathml-content+xml'],
          OpenMath: ['OpenMath'],
        },
      },
    },
  };
  html.findMath().compile().getMetrics().typeset().updateDocument();
};
