import { LitElement, html, css } from 'lit-element';

class Exercise extends LitElement {
  static get properties() {
    return { title: { type: String }, together: { type: Boolean }, extra: { type: Boolean } };
  }

  static get styles() {
    // TODO: inherit styles
    return [
      css`
        *,
        ::before {
          box-sizing: border-box;
        }

        :host > div {
          background-color: var(--color-exercise-bg);
          margin: 10px;
          max-width: fit-content;
          min-width: 100%;
          padding: 5px;
        }

        :host > div::after {
          bottom: 0;
          content: '😅';
          font-size: 2rem;
          position: relative;
          right: 10px;
        }

        :host > div.together::after {
          content: '😅👥';
        }
      `,
    ];
  }

  render() {
    return html`<div class="${this.together ? 'together' : ''}"><slot></slot></div>`;
  }
}

export const componentExercise = () => {
  customElements.define('component-exercise', Exercise);
};
