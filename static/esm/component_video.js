import { LitElement, html, css } from 'lit-element';

class Video extends LitElement {
  constructor() {
    super();
    this.options = {
      root: null,
      rootMargin: '0px',
      threshold: 0.2,
    };
  }

  static get properties() {
    return {
      isplaylist: { type: Boolean },
      videoid: { type: String },
      title: { type: String },
    };
  }

  static get styles() {
    return [
      css`
        :host > div {
          height: 0;
          margin-block-end: 10px;
          margin-block-start: 10px;
          padding-top: 56.25%;
          position: relative;
          width: 100%;
        }

        iframe {
          height: 100%;
          left: 0;
          position: absolute;
          top: 0;
          width: 100%;
        }
      `,
    ];
  }

  _callbackObserver(entries, observerLocal) {
    entries.forEach(entry => {
      if (entry.isIntersecting) {
        if (entry.isIntersecting && entry.target.classList.contains('video-youtube')) {
          entry.target.setAttribute('allow', 'accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture');
          entry.target.setAttribute('allowfullscreen', '');
          if (this.videoid) {
            const url = `https://www.youtube-nocookie.com/embed/${this.isplaylist ? 'videoseries?list=' : ''}${
              this.videoid
            }${this.isplaylist ? '' : '?modestbranding=1'}`;
            entry.target.setAttribute('src', url);
            if (window.DOMParser) {
              const urlSubtitles = `https://video.google.com/timedtext?lang=en&v=${this.videoid}`;
              fetch(urlSubtitles).then(response => {
                if (response.ok) {
                  const parser = new DOMParser();
                  response.text().then(xml => {
                    const xmlDoc = parser.parseFromString(xml, 'text/xml');
                    if (xmlDoc.firstElementChild.tagName === 'transcript') {
                      // TODO: Use html``.
                      const elementTranscript = document.createElement('details');
                      const elementTranscriptSummary = document.createElement('summary');
                      elementTranscriptSummary.innerText = 'Letterlijke transcriptie van video';
                      elementTranscript.appendChild(elementTranscriptSummary);
                      const elementUtterance = document.createElement('blockquote');
                      for (const child of xmlDoc.firstElementChild.childNodes) {
                        elementUtterance.innerHTML += ` ${child.textContent}`;
                      }
                      elementTranscript.appendChild(elementUtterance);
                      entry.target.parentElement.insertAdjacentElement('afterEnd', elementTranscript);
                    }
                  });
                }
              });
            }
          }
          observerLocal.unobserve(entry.target);
        }
      }
    });
  }

  firstUpdated() {
    if (!this.elementVisible) {
      this.intersectionObserver = new IntersectionObserver(this._callbackObserver.bind(this), this._options);
      const elementDefer = this.shadowRoot.querySelector('iframe');
      this.intersectionObserver.observe(elementDefer);
    }
  }

  render() {
    return html`<div>
      <iframe class="video-youtube" videoid="${this.videoid}" title="${this.title}"></iframe>
    </div>`;
  }
}

export const componentVideo = () => {
  customElements.define('component-video', Video);
};
