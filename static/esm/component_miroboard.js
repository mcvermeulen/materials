import { LitElement, html, css } from 'lit-element';

class Miroboard extends LitElement {
  static get properties() {
    return {
      viewport: { type: String },
      idBoard: { type: String },
      title: { type: String },
    };
  }

  static get styles() {
    return [
      css`
        :host > iframe {
          height: 100%;
          overflow: auto;
          resize: vertical;
          width: 100%;
        }
      `,
    ];
  }

  render() {
    return html`<iframe
      src="https://miro.com/app/live-embed/${this.idBoard}&equals;/?moveToViewport&equals;${this.viewport}"
      allowfullscreen
      title="${this.title}"
    ></iframe>`;
  }
}

export const componentMiroboard = () => {
  customElements.define('component-miroboard', Miroboard);
};
