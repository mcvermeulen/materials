export const renderFigurenumbering = elementBody => {
  const nElementFigcaption = elementBody.querySelectorAll(':scope figure figcaption');
  let countFigure = 1;
  for (const elementFigcaption of nElementFigcaption) {
    elementFigcaption.prepend(`Figuur ${countFigure}: `);
    countFigure += 1;
  }
};
