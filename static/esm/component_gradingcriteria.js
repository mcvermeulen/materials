import { LitElement, html, css } from 'lit-element';

class Gradingcriteria extends LitElement {
  static get styles() {
    return [
      css`
        *,
        ::before {
          box-sizing: border-box;
        }

        :host > div {
          background-color: var(--color-extra-bg);
          margin-block-end: 10px;
          margin-block-start: 10px;
          max-width: fit-content;
          min-width: 100%;
          padding: 5px;
        }

        :host > div::after {
          bottom: 0;
          content: '🧭';
          font-size: 2rem;
          position: relative;
          right: 10px;
        }
      `,
    ];
  }

  render() {
    return html`<div>
      <p>Je …</p>
      <ol>
        <slot></slot>
      </ol>
    </div>`;
  }
}

export const componentGradingcriteria = () => {
  customElements.define('component-gradingcriteria', Gradingcriteria);
};
