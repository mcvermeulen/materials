import { LitElement, html, css } from 'lit-element';

class Intendedlearningoutcome extends LitElement {
  static get properties() {
    return { codes: { type: Array } };
  }

  static get styles() {
    // TODO: inherit styles
    return [
      css`
        *,
        ::before {
          box-sizing: border-box;
        }

        li > code::before {
          content: '🎯  ';
        }

        li > code {
          font-family:
            /* macOS 10.10+ */ 'Menlo', /* Windows 6+ */ 'Consolas',
            /* Android 4+ */ 'Roboto Mono', /* Ubuntu 10.10+ */ 'Ubuntu Monospace', /* KDE Plasma 5+ */ 'Noto Mono',
            /* KDE Plasma 4+ */ 'Oxygen Mono', /* Linux/OpenOffice fallback */ 'Liberation Mono',
            /* fallback */ monospace, /* macOS emoji */ 'Apple Color Emoji', /* Windows emoji */ 'Segoe UI Emoji',
            /* Windows emoji */ 'Segoe UI Symbol', /* Linux emoji */ 'Noto Color Emoji';
          font-size: 0.8rem;
        }
      `,
    ];
  }

  render() {
    return html`<li>
      <p><slot></slot></p>
      ${this.codes.map(code => html`<code>${code}</code>`)}
    </li>`;
  }
}

export const componentIntendedlearningoutcome = () => {
  customElements.define('component-intendedlearningoutcome', Intendedlearningoutcome);
};
