import { html, render } from 'lit-html';

export const renderLessonTrees = () =>
  render(
    html`<div slot="title">Bomen</div>
      <component-section>
        <div slot="title">Wat je leert</div>
        <component-intendedlearningoutcomes>
          <component-intendedlearningoutcome codes='["AD-begrip-standaardalgoritmes-datastructuren"]'
            >legt het wiskundige <strong>boom</strong>-object uit.
          </component-intendedlearningoutcome>
          <component-intendedlearningoutcome codes='["AD-productie-computationele-taak"]'
            >programmeert een standaard boom-datastructuur uit.
          </component-intendedlearningoutcome>
          <component-intendedlearningoutcome codes='["AD-begrip-standaardalgoritmes-datastructuren"]'
            >legt bomen <strong>aflopen</strong> in <strong>pre-</strong>, <strong>in-</strong> en
            <strong>post</strong>-volgorde uit.
          </component-intendedlearningoutcome>
          <component-intendedlearningoutcome codes='["AD-begrip-standaardalgoritmes-datastructuren"]'
            >legt de standaard <strong>binaire zoekboom</strong>-datastructuur uit.
          </component-intendedlearningoutcome>
          <component-intendedlearningoutcome codes='["AD-productie-computationele-taak"]'
            >programmeert de standaard binaire zoekboom uit
          </component-intendedlearningoutcome>
          <component-intendedlearningoutcome codes='["AD-productie-computationele-taak"]'>
            legt de
            <strong>AVL-boom</strong>-datastructuur uit.</component-intendedlearningoutcome
          >
          <component-intendedlearningoutcome codes='["AD-productie-computationele-taak"]'>
            categoriseert de tijdcomplexiteit van AVL-boom-operaties.</component-intendedlearningoutcome
          >
          <component-intendedlearningoutcome codes='["AD-productie-computationele-taak"]'>
            legt <strong>rotaties</strong> uit die nodig zijn om de <strong>AVL-conditie</strong> te
            handhaven.</component-intendedlearningoutcome
          >
        </component-intendedlearningoutcomes>
        <component-miroboard idBoard="o9J_lflxQv0" viewport="-887,-702,1003,782" title="Mind map"></component-miroboard>
      </component-section>
      <component-section>
        <div slot="title">Waarom je over bomen leert</div>
        <p class="praktijk">
          Bomen worden vrijwel in alle software gebruikt. Iedere webpagina bestaat uit een boomstructuur van HTML. Ieder
          gangbaar bestandssysteem organiseert bestanden als een boom, waarin bestanden de bladeren zijn en mappen de
          knopen. Ook de processen die multitask besturingssysteem bijhoudt (zoals ook het besturingssysteem dat je nu
          gebruikt), worden bijgehouden in een boomstructuur. Die boomstructuur is opgebouwd volgens welk proces (ouder)
          een ander proces opstart (kind). Als een ouderproces stopt, stopt het kindproces in principe automatisch. Last
          but not least: bomen zijn cruciaal in de komende les over de implementatie van programmeertalen.
        </p>
      </component-section>
      <component-section>
        <div slot="title">Bomen</div>
        <p>
          Een boom is een samenhangende graaf uit \\(N\\) knopen (punten,
          <em class="term">nodes</em>) met \\(N-1\\) takken (lijnen). Een boom heeft altijd exact één
          <em class="term">wortelknoop</em> (<em class="term">root node</em>). Vanuit de wortelknoop zijn verbindingen
          naar iedere andere knoop. Wiskundig is een boom gedefinieerd als een ongerichte graaf, maar in de IT worden
          gerichte grafen die verder wel een boom zijn ook weleens informeel ‘boom’ genoemd. Formeel zijn zulke grafen
          <em class="term">gerichte acyclische grafen</em> (<em class="term">Directed Acyclic Graphs (DAGs)</em>).
        </p>
        <p class="praktijk">
          Knopen binnen bomen hebben verscheidene belangrijke relaties tot elkaar. De namen van die relaties komen vaak
          terug in algoritmes (informatica-achtig werk dus), maar ook als je bijvoorbeeld gewoon veel met HTML werkt als
          alledaagse ontwikkelaar.
        </p>
        <p>
          OpenDSA geeft een illustratie van deze relaties:
          <a class="bron" href="https://opendsa-server.cs.vt.edu/ODSA/Books/Everything/html/GenTreeIntro.html"
            >18.1. General Trees</a
          >. In het Nederlands laat hun overzicht zich als volgt samenvatten. Een
          <em class="term">afstammeling</em> (<em class="term">ancestor</em>) van een knoop \\(n_\\text{dominator}\\) is
          een knoop \\(n_\\text{afstammeling}\\) die op hetzelfde simpele pad ligt naar de wortel. Een
          <em class="term">kindknoop</em> van een knoop \\(n_\\text{ouder}\\) heeft \\(n_\\text{ouder}\\) als ouder, en
          is m.a.w. zijn directe afstammeling. <em class="term">Zusterknopen</em> van elkaar (<em class="term"
            >siblings</em
          >) hebben dezelfde <em class="term">ouderknoop</em> (<em class="term">parent</em>). Iedere set siblings vormt
          een eigen <em class="term">laag</em> (<em class="term">level</em>) in de boom. Een
          <em class="term">bladknoop</em> (<em class="term">leaf node</em>) is een knoop die geen kinderen heeft. Een
          <em class="term">deelboom</em> (<em class="term">subtree</em>) is een deelgraaf van de boom, die zelf ook een
          boom is (dus voldoet aan dezelfde eigenschappen, in de praktijk veelal kleiner en met een andere wortel).
          OpenDSA beschouwt een boom ook als een adt. Je kan immers een interface voor een boom schrijven, met o.a. het
          aflopen ervan en zoeken erin als operaties, en de onderliggende datastructuren zijn vrij in te vullen. De
          <em class="term">diepte</em> (<em class="term">depth</em>) van een een knoop is de lengte van het pad van die
          knoop naar de wortel. De <em class="term">hoogte</em> (<em class="term">height</em>) van een een boom is de
          lengte van het langste simpele pad, m.a.w. de diepte van de diepste bladknoop. De
          <em class="term">ariteit</em> (<em class="term">arity</em>) van een boom is de maximale hoeveelheid kinderen
          die een knoop mag hebben. Een <em class="term">binaire boom</em> heeft maximaal twee kinderen, en een ternaire
          drie, enzovoort. Als je de ariteit van de boom strak in de hand houdt met je datastructuur is het mogelijk om
          gunstige complexiteitscategorieën voor de operaties erop te bereiken. Hierover leer je later meer.
        </p>
      </component-section>
      <component-section>
        <div slot="title">Binaire bomen en binaire zoekbomen</div>
        <component-section>
          <div slot="title">Waarom je over binaire bomen en binaire zoekbomen leert</div>
          <p class="praktijk">
            Een zoekboom wordt gebruikt voor efficiënt zoeken (dwz. het implementeren van de dictionary-adt), in plaats
            van bijvoorbeeld hashtabel, als de ordening van de data belangrijk is voor de toepassing. In tegenstelling
            tot bij hashtabellen, is de load factor van een eventuele onderliggende rij niet zo belangrijk. Als je meer
            kinderen wilt toevoegen onder een knoop, kan je de kleine rij met zijn kinderen verdubbelen i.p.v. een hele
            tabel met alle data. Verder hoeven waarden niet gehasht te worden voor ze ingevoegd worden, en hoef je dus
            ook niet te onderzoeken of je hashfunctie wel optimaal is. Meer in het algemeen: een hashtabel is veel
            complexer om optimaal te implementeren dan een zoekboom. Dan het nadeel van zoekbomen: hun
            complexiteitscategorie van de looptijd in het slechtste geval voor <code>insert(item)</code> en
            <code>search(item)</code> is niet constant maar logaritmisch.
          </p>
          <p>
            Naast voor zoeken worden binaire bomen gebruikt voor het ontleden van broncode, en daarbij met name voor het
            bewaren van binaire operaties, zoals plus, min, maal en gedeeld door.
          </p>
        </component-section>
        <p class="bron">
          <a href="https://opendsa-server.cs.vt.edu/ODSA/Books/Everything/html/BinaryTreeIntro.html"
            >12.1. Binary Trees Chapter Introduction</a
          >
        </p>
        <p class="bron">
          <a href="https://opendsa-server.cs.vt.edu/ODSA/Books/Everything/html/BinaryTree.html">12.2. Binary Trees</a>
        </p>
        <p class="bron">
          <a href="https://opendsa-server.cs.vt.edu/ODSA/Books/Everything/html/RecursiveDS.html"
            >12.3. Binary Tree as a Recursive Data Structure</a
          >
        </p>
        <p class="bron">
          <a href="https://opendsa-server.cs.vt.edu/ODSA/Books/Everything/html/BinaryTreeImpl.html"
            >12.8. Binary Tree Node Implementations</a
          >
        </p>
        <component-section>
          <div slot="title">(Binaire) bomen aflopen</div>
          <p>
            OpenDSA legt ook het <em class="term">aflopen</em> (<em class="term">traversal</em>) van bomen uit, en biedt
            een simulatie:
            <a
              class="bron"
              href="https://opendsa-server.cs.vt.edu/ODSA/Books/Everything/html/GenTreeIntro.html#general-tree-traversals"
              >18.1.1.3. General Tree Traversals</a
            >. Aflopen houdt in het ‘openen’ van iedere knoop om hem te lezen, of misschien wel te beschrijven. Het komt
            erop neer dat er drie vaste volgordes zijn waarin je een boom kunt aflopen vanuit de wortel, vernoemd naar
            het moment waarop je de ouderknoop opent. Bij binaire bomen:
          </p>
          <ol>
            <li>
              <p>
                <em class="term">vooraf-volgorde</em> (<em class="term">preorder</em>): eerst de ouder, dan het
                linkerkind, dan het rechterkind;
              </p>
            </li>
            <li>
              <p>
                <em class="term">op-volgorde</em> (<em class="term">inorder</em>): eerst het linkerkind, dan de ouder,
                dan het rechterkind;
              </p>
            </li>
            <li>
              <p>
                <em class="term">achteraf-volgorde</em> (<em class="term">postorder</em>): eerst het rechterkind, dan
                dan het linkerkind, dan de ouder.
              </p>
            </li>
          </ol>
          <p>
            Heel belangrijk om te begrijpen is dat het voorgaande algoritme
            <em>telkens bij het openen van een knoop opnieuw toegepast wordt, van voren af aan</em>. Als je op-volgorde
            afloopt, ga je dus eerst depth-first de linkertak af, tot je bij een blad komt, en daarna krimpt de recursie
            weer. Het op-volgorde aflopen geeft de waardes (boomknopen) in oplopend gesorteerde volgorde terug. Bij
            algemene bomen loop je de kinderen op volgorde waarin ze bewaard zijn af, en verschilt alleen het moment
            waarop je de ouderknoop opent. In een implementatie van een binaire boom heeft ieder knoop-object aparte
            velden <code>kind_links</code> en <code>kind_rechts</code>, waardoor je precies bijhoudt welk kind links dan
            wel rechts is. Bij datastructuren voor algemene bomen is zo'n volgorde lastiger te handhaven in een
            onderliggende lijst met kinderen.
          </p>
          <p>Het aflopen van een boom heeft een looptijd van \\(Θ(n)\\) in alle gevallen.</p>
          <p class="bron">
            Binary Tree 3. Traverse (algorithms, pseudocode and VB.NET code)
            <component-video
              videoid="eiBS_Avn3Co"
              title="Binary Tree 3. Traverse (algorithms, pseudocode and VB.NET code)"
            ></component-video>
          </p>
          <p class="bron">
            <a href="https://opendsa-server.cs.vt.edu/ODSA/Books/Everything/html/BinaryTreeTraversal.html"
              >12.5. Binary Tree Traversals</a
            >
          </p>
          <p class="bron">
            <a href="https://opendsa-server.cs.vt.edu/ODSA/Books/Everything/html/WritingTraversals.html"
              >12.6. Implementing Tree Traversals</a
            >
          </p>
          <component-section>
            <div slot="title">Complexiteitscategorie van het aflopen van bomen</div>
            <component-exercise>
              <p>Waarom heeft het aflopen van bomen een lineaire looptijd?</p>
            </component-exercise>
          </component-section>
          <component-section>
            <div slot="title">Volgorde van kinderen in een algemene boom bijhouden</div>
            <component-exercise>
              <p>
                Waarom is het bijhouden van de volgorde van kinderen in een algemene boom-datastructuur lastiger dan in
                een binaire boom?
              </p>
            </component-exercise>
          </component-section>
        </component-section>
        <component-section>
          <div slot="title">Zoekbomen</div>
          <p>
            Een toepassing van bomen waar je voor deze course waarschijnlijk minder mee in aanraking bent gekomen, is
            die van zoekbomen. Een
            <em class="term">zoekboom</em> is een datastructuur waarin iedere knoop één waarde bewaart van een vast data
            type, dat een vaste volgorde kent (als in: <code class="lang-java">Comparable</code>). Door deze waardes op
            een consequente manier in de boom te bewaren, kan je snel zoeken naar één (bijv. iets gelijk aan (x)) of
            meer waardes (bijv.: alles hoger dan (y)). Denk hierbij aan binary search, waarin telkens een heel stuk van
            een gesorteerde rij kon worden overgeslagen vanwege de volgorde waarin de data was opgeslagen. Een
            standaarddatastructuur is de <em class="term">binaire zoekboom</em> (<em class="term">binary search tree</em
            >, <em class="term">BST</em>). Die is opgebouwd volgens <code>insert(i)</code>-operaties. In principe kan je
            ook een ternaire zoekboom etc. ontwikkelen, maar standaardzoekbomen zijn binair. <code>search(i)</code> in
            een binaire zoekboom heeft een looptijd van \\(Θ(\\log n)\\) in het gemiddelde geval. Dat is zo omdat iedere
            binaire zoekboom een
            <em class="term"
              >vaste eigenschap heeft: alle waardes kleiner dan (of gelijk aan) een knoop \\(n\\) zijn
              linker-afstammeling van die knoop, alle waardes groter dan (of gelijk aan) die knoop zijn
              rechter-afstammeling van die knoop</em
            >. Hierdoor is de verwachte hoogte van een binaire zoekboom heel klein: logaritmisch in het aantal knopen.
            Immers, telkens splits je de waardes op in links en rechts, zoals bij binary search. De vaste eigenschap is
            alleen te handhaven door het invoegen en verwijderen van knopen volgens een doordacht algoritme te laten
            verlopen.
          </p>
          <component-section>
            <div slot="title">Invoegen van knopen in een binaire zoekboom</div>
            <p>Voor het invoegen geldt het volgende algoritme:</p>
            <ul>
              <li>
                <p>
                  Als \\(i &lt; n\\): maak een knoop voor \\(i\\) als er nog geen linkerkind is, ga anders door naar het
                  bestaande linkerkind.
                </p>
              </li>
              <li>
                <p>Als \\(i &gt; n\\), idem bij het rechterkind.</p>
              </li>
              <li>
                <p>Als \\(i = n\\), doe dan niets.</p>
              </li>
            </ul>
            <p>
              Dit algoritme is ook weer recursief, het gaat net zo lang door tot duidelijk is dan \\(i\\) nog niet in de
              boom zit en ingevoegd kan worden als kind ergens (diep?) in een tak.
            </p>
            <p>
              Zoals je kan zien zijn in een binaire zoekboom de knopen als unieke sleutels. In afwijkende implementaties
              worden dubbel ingevoegde waardes alsnog in een rij bewaard binnen de knoop \\(n\\) of telkens in het
              linkerkind (OpenDSA). Vandaar dat de vaste eigenschap van binaire zoekbomen was geformuleerd als
              kleiner/groter dan <em>of gelijk aan</em>. In ADP gaan we uit van het eenvoudigste geval: binaire
              zoekbomen zonder dubbele waardes.
            </p>
            <p class="bron">Binary Tree 1. Constructing a tree (algorithm and pseudocode)</p>
            <component-video
              videoid="m4T__2QnoyQ"
              title="Binary Tree 1. Constructing a tree (algorithm and pseudocode)"
            ></component-video>
            <p class="bron">
              <a href="https://opendsa-server.cs.vt.edu/ODSA/Books/Everything/html/BST.html"
                >12.11. Binary Search Trees</a
              >
            </p>
          </component-section>
          <component-section>
            <div slot="title">Verwijderen van knopen in een binaire zoekboom</div>
            <p>Als je knoop \\(n\\) verwijdert, heb je te maken met drie mogelijke gevallen:</p>
            <ul>
              <li>
                <p>Als \\(n\\) een blad is, verwijder dan \\(n\\).</p>
              </li>
              <li>
                <p>
                  Als \\(n\\) een ouder is met één kind, verwijder dan (n) zelf, maakt het kind van \\(n\\) het kind van
                  \\(n\\)’s ouder.
                </p>
              </li>
              <li>
                <p>
                  Als \\(n\\) een ouder is met twee kinderen, zoek \\(n'\\), (A) de kleinste knoop in de boom groter dan
                  \\(n\\) zelf, óf (B) de grootste knoop kleiner dan of gelijk aan \\(n\\). Vervang de waarde in knoop
                  \\(n\\) door de waarde in knoop \\(n'\\). Verwijder knoop \\(n'\\) (en volg daarbij weer het
                  algoritme).
                </p>
              </li>
            </ul>
          </component-section>
          <p>
            We moeten extra moeite doen om die looptijd van \\(Θ(\\log n)\\) ook in het slechtste geval te handhaven.
            Dat kan alleen als de boom
            <em class="strong">gebalanceerd</em> is. Een binaire boom is (perfect) gebalanceerd als
            <em>ieder</em> linker- en rechterkind gelijke hoogte heeft (waarbij iedere knoop in de boom dus een keer als
            wortel fungeert van een deelboom). Dan benadert die boom, met \\(N\\) knopen, een hoogte van \\(\\log N\\).
            Maar is de boom niet gebalanceerd, dan loopt de looptijd in het slechtste geval op tot \\(Θ(n)\\). Dan is de
            boom net een linked list (met maar één diepste pad). Dit kan in de praktijk al gebeuren als je twee
            verschillende gesorteerde waardes achtereenvolgens invoegt.
          </p>
          <component-section>
            <div slot="title">AVL-zoekbomen</div>
            <p>
              Een binaire zoekboom kan ongebalanceerd raken bij ongunstige invoer, en bij het verwijderen van knopen.
              Daarom zijn er geavanceerdere binaire zoekbomen:
              <em class="term">zelf-balancerende</em> binaire zoekbomen. De <em class="term">AVL-zoekboom</em> is een
              standaarddatastructuur daarvoor. Bij ADP behandelen we deze, maar er zijn ook andere zoals de Red-Black
              Tree. Het idee is dat je leert hoe je zoiets als herbalanceren zou kunnen aanpakken en waarom, niet het
              doorgronden van de allerbeste datastructuur. Dat laatste ligt trouwens erg aan de beoogde toepassing.
            </p>
            <p class="bron">
              <a href="https://opendsa-server.cs.vt.edu/ODSA/Books/Everything/html/BalancedTree.html"
                >26.1. Balanced Trees</a
              >
            </p>
            <p>
              Naast de vaste eigenschap van binaire zoekbomen heeft een AVL-zoekboom de vaste eigenschap dat hij
              ‘redelijk’ gebalanceerd is (<em class="term">AVL-gebalanceerd</em> of voldoet aan de
              <em class="term">AVL-conditie</em>).
            </p>
            <p>
              Een binaire boom is (perfect) gebalanceerd als
              <em>ieder</em> linker- en rechterkind een hoogte heeft die maximaal met \\(1\\) verschilt. Het
              hoogteverschil links minus rechts wordt ook wel de
              <em class="term">balance factor of kortweg <code>bf</code></em> genoemd. Van de hoogte van de
              linkerdeelboom trek je die van de rechterdeelboom af, en dan krijg je een getal \\(-1\\), \\(0\\) of
              \\(1\\). (Let op: sommige bronnen draaien dit om. Wij en VisuAlgo hanteren: \\(\\text{linkerhoogte} -
              \\text{rechterhoogte}\\).)
            </p>
            <p>
              Hierdoor heeft <code>search(item)</code> op een AVL-zoekboom wel een looptijd van \\(O(\\log n)\\) in het
              slechtste geval, terwijl een binaire zoekboom dan lineaire looptijd vereist. Het algoritme van toevoegen aan een AVL-boom is als volgt:
            </p>
            <ol>
              <li>
                <p>Voeg knoop \\(n\\) toe zoals bij een binaire zoekboom.</p>
              </li>
              <li>
                <p>
                  Controleer of de boom nog AVL-gebalanceerd is door vanuit de net toegevoegde knoop telkens naar zijn
                  ouderknoop te gaan (krimpen van de recursie). Stop bij de eerste, dus
                  <em>diepste</em> knoop die AVL-ongebalanceerd is. Met andere woorden, die knoop is wortel van een
                  <em>AVL-ongebalanceerde deelboom</em> \\(S\\). Noem de wortelknoop \\(s\\) (net als in OpenDSA). Je
                  vindt deze \\(s\\) dus op het pad vanaf \\(n\\) naar de wortel van de (hele) boom.
                </p>
              </li>
              <li>
                <p>Stel, het pad van \\(s\\) naar \\(n\\) begint met</p>
                <ul>
                  <li>
                    <p>…linkerkind → linkerkind (<em>LL</em>, geval 1); of</p>
                  </li>
                  <li>
                    <p>…rechterkind → rechterkind (<em>RR</em>, geval 4 in OpenDSA, 3 in VisuAlgo),</p>
                  </li>
                </ul>
              </li>
              <li>
                <p>
                  … pas dan een
                  <em class="term">enkele rotatie</em> toe, namelijk
                </p>
                <ul>
                  <li>
                    <p>LL? <strong>Roteer dan \\(S\\) naar rechts</strong>;</p>
                  </li>
                  <li>
                    <p>RR? <strong>Roteer dan \\(S\\) naar links</strong>.</p>
                  </li>
                </ul>
              </li>
              <li>
                <p>Of anders, stel het pad van \\(s\\) naar \\(n\\) begint met</p>
                <ul>
                  <li>
                    <p>…linkerkind → rechterkind: (<em>LR</em>, geval 2),</p>
                  </li>
                  <li>
                    <p>…rechterkind → linkerkind: (<em>RL</em>, geval 3 in OpenDSA, 4 in VisuAlgo),</p>
                  </li>
                </ul>
              </li>
              <li>
                <p>
                  … pas dan een
                  <em class="term">dubbele rotatie</em> toe, namelijk
                </p>
                <ul>
                  <li>
                    <p>
                      LR?
                      <strong
                        >Roteer dan de linkerdeelboom van \\(S\\) naar links, en daarna \\(S\\) zelf naar rechts</strong
                      >;
                    </p>
                  </li>
                  <li>
                    <p>
                      RL?
                      <strong
                        >Roteer dan de rechterdeelboom van \\(S\\) naar rechts, en daarna \\(S\\) zelf naar
                        links</strong
                      >.
                    </p>
                  </li>
                </ul>
              </li>
            </ol>
            <component-section>
              <div slot="title">Rotaties</div>
              <p>
                De rotaties heten zo omdat ze symmetrisch zijn. Naar rechts is het omgekeerde van naar links roteren. In
                woorden is het erg ondoorzichtig om te beschrijven <em>hoe</em> je roteert. Er zijn verschillende
                manieren om te beschrijven welke deelboom nu precies ‘roteert’. De knopen trekken elkaar als het ware
                ‘mee’ aan de lijnen tijdens het roteren, en deels worden de lijnen daarna anders verbonden. De knopen
                waaruit de deelbomen bestaan worden in iedere bron helaas weer anders gelabeld. Wij hanteren de manier
                van VisuAlgo. Helaas zijn ook de verschillende gevallen in bronnen vaak anders benoemd of geordend. Het is het handigst om een visuele uitleg zoals in de volgende illustratie te gebruiken. Dan snap je de rotaties, en kan je in omgekeerde volgorde het bijbehorende geval herleiden.
              </p>
              <figure>
                <figcaption>
                  Hoe een dubbele rotatie een ingewikkeld geval vereenvoudigt tot een makkelijker geval, dat je met een
                  enkele rotatie oplost (bron: VisuAlgo). Let op de balance factor, en leg dit algoritme naast de
                  stapsgewijze beschrijving in deze paragraaf. Als je ziet dat ze overeenkomen, heb je alles pas door.
                </figcaption>
                <img src="https://visualgo.net/img/four_cases.png" alt="De vier gevallen en de rotaties daarbij" />
              </figure>
              <p>Bekijk ook de uitleg van Back to Back SWE.</p>
              <p class="bron">
                <a href="https://backtobackswe.com/platform/content/avl-trees-rotations/video">AVL Trees & Rotations (Self-Balancing Binary Search Trees)</a>
              </p>
              <p>Dit wat ingewikkelde algoritme kan je het beste oefenen met VisuAlgo. Om een specifieke boom te maken, voeg je de knopen in volgens een breadth-first-ordening.</p>
              <p class="bron">
                <a href="https://visualgo.net/en/bst">VisuAlgo - Binary Search Tree, AVL Tree</a>
              </p>
              <p>Tot slot nog een bron die technisch correct is maar wat summier en impliciet: OpenDSA. Bestudeer deze alleen om de overeenkomsten te vinden tussen verschillende uitleggen, als het al snapt.</p>
              <p class="bron">
                <a href="https://opendsa-server.cs.vt.edu/ODSA/Books/Everything/html/AVL.html">26.2. The AVL Tree</a>
              </p>
              </component-section>
            </component-section>
          </component-section>
        </component-section>
        <component-section>
          <div slot="title">Bijeenkomst 2020-12-01</div>
          <component-section>
            <div slot="title">Welkom (20:00)</div>
            <ul>
              <li>
                <p>Aanwezigheidsregistratie.</p>
              </li>
              <li>
                <p>De structuur van deze les.</p>
              </li>
            </ul>
          </component-section>
          <component-section>
            <div slot="title">Afronding course, statusbespreking (20:05)</div>
            <ul>
              <li>
                <p>Digitaal tentamen.</p>
              </li>
              <li>
                <p>Lopende doorontwikkeling n.a.v. feedback.</p>
              </li>
              <li>
                <p>Status OP?</p>
              </li>
            </ul>
          </component-section>
          <component-section>
            <div slot="title">Korte bespreking van de leerinhoud (20:20)</div>
          </component-section>
          <component-section>
            <div slot="title">Zelf bestuderen en basisopgaven maken (20:30)</div>
            <p>
              <a href="https://secure.ans-delft.nl/universities/13/courses/43101/assignments/114276/quiz/start"
                >Basisopgaven les 7: Bomen</a
              >
            </p>
          </component-section>
        </component-section>
        <component-section>
          <div slot="title">Bijeenkomst 2020-12-03</div>
          <component-section>
            <div slot="title">Welkom (18:30)</div>
            <ul>
              <li>
                <p>Aanwezigheidsregistratie.</p>
              </li>
              <li>
                <p>
                  Hoe is het gegaan? Neem even de tijd om de
                  <a href="https://secure.ans-delft.nl/universities/13/courses/43101/assignments/95815/quiz/start"
                    >vragenlijst ‘Je huidige leerresultaten’</a
                  >
                  (weer) in te vullen.
                </p>
              </li>
              <li>
                <p>De structuur van deze les.</p>
              </li>
            </ul>
          </component-section>
          <component-section class="together">
            <div slot="title">Operaties op een AVL-boom uitvoeren met de hand (18:45)</div>
          </component-section>
          <component-section>
            <div slot="title">Samenopgaven</div>
            <component-section>
              <div slot="title">Operaties op een AVL-boom uitvoeren (19:00)</div>
              <component-exercise together>
                <p>
                  <a href="https://secure.ans-delft.nl/universities/13/courses/43101/assignments/118811/quiz/start"
                    >Samenopgave les 7: Bomen - Operaties op een AVL-boom uitvoeren</a
                  >
                </p>
              </component-exercise>
            </component-section>
            <component-section>
              <div slot="title">Ontwikkel een binaire zoekboom (19:15)</div>
              <component-exercise together>
                <p>
                  <a href="https://secure.ans-delft.nl/universities/13/courses/43101/assignments/116583/quiz/start"
                    >Samenopgave les 7: Bomen - Ontwikkel een binaire zoekboom</a
                  >
                </p>
                <component-section>
                  <div slot="title">Bespreking (20:15)</div>
                </component-section>
              </component-exercise>
            </component-section>
          </component-section>
        </component-section></component-section
      >`,
    document.getElementById('lessonTrees'),
  );
