import { html, render } from 'lit-html';

export const renderLessonAdt = () =>
  render(
    html` <div slot="title">Abstract data types en operaties</div>
      <component-section>
        <div slot="title">Wat je leert</div>
        <component-intendedlearningoutcomes>
          <component-intendedlearningoutcome codes='["AD-begrip-standaardalgoritmes-datastructuren"]'
            >legt
            <strong>abstract data type (<abbr title="abstract data type">adt</abbr>)</strong>
            uit.</component-intendedlearningoutcome
          >
          <component-intendedlearningoutcome codes='["AD-begrip-standaardalgoritmes-datastructuren"]'
            >legt
            <strong>datastructuur</strong>
            uit.</component-intendedlearningoutcome
          >
          <component-intendedlearningoutcome codes='["AD-begrip-standaardalgoritmes-datastructuren"]'>
            legt de <strong>list</strong>-adt uit.</component-intendedlearningoutcome
          >
          <component-intendedlearningoutcome codes='["AD-begrip-standaardalgoritmes-datastructuren"]'
            >legt de <strong>linked list</strong>-datastructuur uit.</component-intendedlearningoutcome
          >
          <component-intendedlearningoutcome codes='["AD-begrip-standaardalgoritmes-datastructuren"]'
            >categoriseert de tijdcomplexiteit van operaties op een linked
            list-datastructuur.</component-intendedlearningoutcome
          >
          <component-intendedlearningoutcome codes='["AD-begrip-standaardalgoritmes-datastructuren"]'
            >legt de <strong>stack</strong>-adt uit.</component-intendedlearningoutcome
          >
          <component-intendedlearningoutcome codes='["AD-begrip-standaardalgoritmes-datastructuren"]'>
            categoriseert de tijdcomplexiteit van operaties op een
            stack-datastructuur.</component-intendedlearningoutcome
          >
          <component-intendedlearningoutcome codes='["AD-begrip-standaardalgoritmes-datastructuren"]'>
            legt de <strong>queue</strong>-adt uit.</component-intendedlearningoutcome
          >
          <component-intendedlearningoutcome codes='["AD-begrip-standaardalgoritmes-datastructuren"]'>
            categoriseert de tijdcomplexiteit van operaties op een
            queue-datastructuur.</component-intendedlearningoutcome
          >
          <component-intendedlearningoutcome codes='["AD-begrip-standaardalgoritmes-datastructuren"]'>
            legt de <strong>priority queue</strong>-adt uit.</component-intendedlearningoutcome
          >
        </component-intendedlearningoutcomes>
        <component-miroboard idBoard="o9J_kg2eXvk" viewport="-568,-601,694,542" title="Mind map"></component-miroboard>
      </component-section>
      <component-section>
        <div slot="title">Adt’s</div>
        <p>Een <em class="term">adt</em> is een abstractie van een datastructuur. Het begrip adt is een afspraak om</p>
        <ul>
          <li>een bepaalde manier van data bewaren overal dezelfde naam te geven,</li>
          <li>
            waarbij er minstens een bepaalde set
            <em class="term">operaties</em> op mogelijk moet zijn.
          </li>
        </ul>
        <p>
          Een operatie is bijv. een element toevoegen (en die operatie heet dan bijv. <code>insert</code> of
          <code>add</code>), of zoeken naar een element binnen de datastructuur (<code>search</code>).
        </p>
        <p>
          Kortom, een adt is dus een soort interface voor data, en de operaties erop zijn methods. Maar <em>adt</em> is
          een programmeertaalonafhankelijk concept, in tegenstelling tot <em>interface</em>.
        </p>
        <p>
          Waar een adt abstract en formeel is, is een
          <em class="term">datastructuur</em> een concrete implementatie, uitgedrukt in broncode en gebonden aan een
          concrete geheugenindeling. Adt’s kunnen op verschillende manieren worden geïmplementeerd, zelfs binnen
          programmeertalen. Het resultaat van het implementeren is een datastructuur.
        </p>
        <component-section>
          <div slot="title">Waarom je adt’s leert</div>
          <p class="praktijk">
            De standaard-adt’s waar je in deze les over leert beschrijven de betekenis/functionaliteit van
            datastructuren in vele algoritme-implementaties. Zelfs als je algoritmes niet zelf implementeert, maar
            alleen aanroept, moet je alsnog werken met de invoer- en uitvoerdata (veelal in datastructuren). Het is dan
            belangrijk om te weten naar wat voor operaties je moet zoeken.
          </p>
          <p>
            De implementatiedetails in datastructuren zullen in de praktijk (grote) gevolgen hebben voor de
            prestatie-efficiëntie van een algoritme-implementatie of software-systeem als geheel. Daarom is het
            belangrijk om de begrippen adt en datastructuur te onderscheiden.
          </p>
        </component-section>
        <p>
          Operaties hebben elk hun eigen complexiteitscategorie. Welke complexiteitscategorie hangt weliswaar af van de
          implementatiedetails, maar het is al wel bekend wat de optimale complexiteitscategorieën zijn van de operaties
          van standaard-adt’s. Met deze kennis weet je voor wélke toepassing en context je ontwikkelteam het beste voor
          wélke datastructuur kan kiezen.
        </p>
        <p>Lists, stacks en queues zijn belangrijke adt’s. Hierover leer je meer in het volgende.</p>
      </component-section>
      <component-section>
        <div slot="title">Lists</div>
        <p>Een <em id="term_list" class="term">list</em> is een eindige, geordende rij van items/elementen.</p>
        <p>
          In de volgende video’s legt David Scot Taylor verscheidene adt’s en datastructuren uit op een erg
          toegankelijke manier.
        </p>
        <p class="bron">Lists, Iterators and Abstract Data Types</p>
        <component-video videoid="B4y8Sb2O4H0" title="Lists, Iterators and Abstract Data Types"></component-video>
        <p class="bron">
          <a href="https://opendsa-server.cs.vt.edu/ODSA/Books/CS3/html/ListADT.html">Lists</a>
        </p>
        <component-section>
          <div slot="title">Itereren</div>
          <component-exercise>
            <p>Leg uit wat itereren is.</p>
            <p>Leg ook uit wat het verschil is tussen itereren via een iterator vs. door indexeren.</p>
            <p class="instructor_only">
              <a href="https://youtu.be/MZOf3pOAM6A"
                >Iterators, <code class="lang-java">forEach</code> en indexing onder Java Android</a
              >
            </p>
          </component-exercise>
        </component-section>
        <component-section>
          <div slot="title">Waarom je lists leert</div>
          <p>
            Iedere programmeur heeft al wel eens met een list gewerkt. Zelfs een gewone array uit Java is in essentie
            een datastructuur voor een list. Die array-implementatie voldoet aan
            <a href="#term_list">de definitie van list</a>, maar sommige operaties (bijv. aan het begin of einde ervan
            een element toevoegen), zijn standaard niet in één functie (bijv. method) geïmplementeerd.
          </p>
        </component-section>
        <component-section>
          <div slot="title">De linked list-datastructuur</div>
          <p>
            Een <em class="term">linked list</em> is een datastructuur voor een list, bestaande uit nodes. Een node
            bestaat uit het element (de inhoud) en verwijzingen naar de volgende en soms ook vorige node. De elementen
            staan niet per se opeenvolgend aansluitend in het geheugen. De verwijzingen zijn <em>pointers</em> of
            <em>references</em> (programmeertaalafhankelijk) naar een geheugenobject.
          </p>
          <p class="bron">Linked Lists</p>
          <component-video videoid="14g1YBvx8Co" title="Linked Lists"></component-video>
          <p class="bron">
            <a href="https://opendsa-server.cs.vt.edu/ODSA/Books/CS3/html/ListLinked.html">Linked Lists</a>
          </p>
          <p class="bron">
            <a href="https://opendsa-server.cs.vt.edu/ODSA/Books/CS3/html/ListDouble.html">Doubly Linked Lists</a>
          </p>
          <component-section class="praktijk">
            <div slot="title">Waarom je linked lists leert</div>
            <p>
              Alle niet-embedded software-systemen gebruiken geheugenallocatoren voor de heap (<code class="lang-c"
                >malloc()</code
              >). De implementatie van een geheugenallocator
              <a href="http://staffwww.fullcoll.edu/aclifton/cs241/lecture-dynamic-mem-linked-lists.html"
                >gebruikt vaak linked lists</a
              >.
            </p>
            <p>Ook de processenlijst van het Linux-besturingssysteem is gebaseerd op een linked list.</p>
            <p>
              Linked lists zijn een mogelijke onderliggende datastructuur voor andere datastructuren, zoals stacks,
              queues, hashtabellen en grafen.
            </p>
            <p>
              Doubly-linked lists zouden gebruikt kunnen worden voor iets als een terug- en vooruitfunctie (bijv. in een
              web browser).
            </p>
            <p>
              Linked lists worden vaker gebruikt dan alternatieve lists als het aantal elementen niet bekend is of vast
              mag liggen, en willekeurige toegang tot elementen niet nodig is (bijv. voor sorteren).
            </p>
          </component-section>
          <component-section>
            <div slot="title">Linked lists en geheugenruimte</div>
            <component-exercise>
              <p>
                Leg uit wat meer geheugenruimte inneemt: een singly of doubly-linked list (van dezelfde rij elementen,
                langer dan 0).
              </p>
              <p>
                Leg uit of het type elementen invloed heeft op je vorige uitleg. Stel bijv. dat een doubly-linked list
                elementen van een primitief datatype bevat terwijl een singly-linked list elementen van een niet-primitief
                datatype heeft.
              </p>
            </component-exercise>
          </component-section>
          <component-section>
            <div slot="title">Sentinel node</div>
            <component-exercise>
            <p>Wat is het nut van een sentinel node?</p>
          </component-exercise>
        </component-section>
        <component-section>
          <div slot="title">De dynamic array-datastructuur</div>
          <p>
            <em class="term">Dynamische rijen</em> zijn ook wel bekend als <em class="term">vectors</em>. Dynamische
            rijen hebben geen vaste lengte, i.t.t. statische (gewone) arrays. Hierdoor zijn ze erg veelzijdig, en
            misschien wel de mééstgebruikte datastructuur ooit.
          </p>
          <p>
            In de volgende video’s legt David Scot Taylor dynamische rijen uit. Hij stelt de praktijk centraal, wat zijn
            uitleg voor hbo’ers erg goed maakt, vergeleken met de meer academische video’s die je her en der kan vinden.
            Zijn uitleg over geamortiseerde algoritme-analyse hoef je trouwens niet te leren voor ADP.
          </p>
          <p class="bron">Dynamic Arrays, aka ArrayLists</p>
          <component-video videoid="0XC9lFGGpME" title="Dynamic Arrays, aka ArrayLists"></component-video>
          <component-section>
            <div slot="title">Array access</div>
            <component-exercise>
              <p>
                Hoe lang duurt het om
                <code class="lang-java">array_1[33913452]</code> te evalueren vs.
                <code class="lang-java">array_1[3]</code>?
              </p>
              <p>Antwoord in termen van: korter, langer, of even lang.</p>
            </component-exercise>
          </component-section>
          <component-section>
            <div slot="title"><code class=lang-java>ArrayList</code> vs. statische array</div>
            <component-exercise>
              <p>
                Beschrijf één of meer verschillen tussen een
                <code class="lang-java">ArrayList</code> en een statische array.
              </p>
            </component-exercise>
          </component-section>
        </component-section>
      </component-section>
      <component-section>
        <div slot="title">Wanneer je arrays dan wel linked lists gebruikt</div>
        <p class="bron">Linked Lists and Dynamic Arrays: Misuse and Abuse</p>
        <component-video
          videoid="K8H4LGWB5vQ"
          title="Linked Lists and Dynamic Arrays: Misuse and Abuse"
        ></component-video>
        <p class="instructor_only">
          Een element verwijderen uit een dynamische rij heeft een looptijd van \\(Θ(n)\\) in het gemiddelde geval. Maar
          David Scot Taylot heeft het over verwijderen van ‘every other item’ (elementen om -en-om) van zeg
          \\(\\frac{n}{2}\\) elementen. Dus \\(Θ(n \\frac{n}{2}) = Θ(n^2)\\). De uitspraak van David is dus wat
          vergezocht, maar is bedoeld als een speciaal voorbeeld waarin een linked list een betere
          complexiteitscategorie geeft dan een
          <code class="lang-java">ArrayList</code>.
        </p>
        <component-section>
          <div slot="title"><code>List</code>-interface missers</div>
          <component-exercise>
            <p>
              Geef een voorbeeld uit jouw ervaring waarin je een
              <code class="lang-java">List</code>-interface gebruikte, waardoor operaties in een slechtere
              complexiteitscategorie zouden komen als je i.p.v. een dynamische rij een linked list gebruikt.
            </p>
            <p class="instructor_only">
              Geef een voorbeeld uit de broncode van een noemenswaardig software-systeem dat veel data zou kunnen
              verwerken (machine learning o.i.d.).
            </p>
          </component-exercise>
        </component-section>
      </component-section>
      <component-section>
        <div slot="title">Stack</div>
        <p>
          Een stack geeft het <em>laatste ingevoegde</em> element als eerste terug (<em class="term"
            >last in, first out (lifo)</em
          >).
        </p>
        <p class="bron">Stack Data Structure - Algorithm</p>
        <component-video videoid="niBsGw4h5yI" title="Stack Data Structure - Algorithm"></component-video>
        <p class="bron">
          <a href="https://opendsa-server.cs.vt.edu/ODSA/Books/CS3/html/StackArray.html">Stacks</a>
        </p>
      </component-section>
      <component-section>
        <div slot="title">Queue</div>
        <p>
          Een queue geeft het <em>vroegst ingevoegde</em> element als eerste terug (<em class="term"
            >first in, first out (fifo)</em
          >).
        </p>
        <p class="bron">Queue Data Structure - Algorithm</p>
        <component-video videoid="jXMqVpAVyMY" title="Queue Data Structure - Algorithm"></component-video>
        <p class="bron">
          <a href="https://opendsa-server.cs.vt.edu/ODSA/Books/CS3/html/Queue.html">Queues</a>
        </p>
        <component-section>
          <div slot="title">Priority queue</div>
          <p>
            Een <em class="term">priority queue</em> is qua interface identiek aan een queue, maar de operaties hebben
            een andere betekenis. In plaats van dat het <em>vroegst ingevoegde</em> element als eerste teruggegeven
            wordt, wordt telkens het <em>kleinste</em> element teruggegeven. Alternatief kan telkens het
            <em>grootste</em> element teruggegeven. Algemener gezegd, wordt telkens het element met de hoogste
            prioriteit teruggegeven.
          </p>
          <p>
            De datastructuur nodig voor implementatie van een priority queue met operaties met optimale
            complexiteitscategorieën, een
            <em>heap</em>, is erg ingewikkeld en behoort niet tot de leerinhoud van ADP. Later in ADP leer je wel over
            een relatief optimale implementatie en een erg belangrijke toepassing van de priority queue. Het is ook
            mogelijk om de adt te implementeren op een naïeve manier, door intern bijv. een lijst bij te houden en die
            telkens te sorteren op prioriteit.
          </p>
        </component-section>
      </component-section>
      <component-section>
        <div slot="title">Alle besproken standaard-adt’s nog eens</div>
        <p>In de volgende bron nog een samenvatting van David Scot Taylor. Deques hoef je voor ADP niet te kennen.</p>
        <p class="bron">Stacks, Queues and Double Ended Queues (Deques)</p>
        <component-video
          videoid="IITnvmnfi_Y"
          title="Stacks, Queues and Double Ended Queues (Deques)"
        ></component-video>
      </component-section>
      <component-section>
        <div slot="title">Generic programming</div>
        <p>
          Java is één van de programmeertalen die mogelijkheden biedt voor
          <em class="term">generic programming</em>. <em>Generic</em> betekent net als het Nederlandstalige
          <em>generiek</em> ‘algemeen’. Generiek is de tegenhanger van <em>specifiek</em>. Generic programming betekent
          dat je binnen een programmeertaal die dat ondersteunt,
          <em>regels kan programmeren over welke datatypes geldig zijn</em>
          op een plaats in de broncode waar types staan. Generiek, want die regels zijn flexibeler en algemener dan ‘de
          class is exact
          <code class="lang-java">DezeClass</code>’. Bij Java kan je generic programming toepassen in definities van een
          method, class of interface.
        </p>
        <p class="bron">
          <a href="https://docs.oracle.com/javase/tutorial/java/generics/index.html">Lesson: Generics</a>
        </p>
        <p class="praktijk">
          Generic programming is nuttig om broncode algemener te maken, dus broncode beter herbruikbaar te maken. Denk
          bijv. aan de volgende toepassingen:
        </p>
        <ul>
          <li>
            <p>
              Als je niet wilt casten naar een superklasse, zoals
              <code class="lang-java">Object</code>, maar ook niet een exact datatype kan gebruiken.
            </p>
          </li>
          <li>
            <p>
              Als een datastructuur alleen elementen van hetzelfde datatype bevat, kan je broncode eenvoudiger worden en
              kan je objectcode een hogere prestatie-efficiëntie leveren.
            </p>
          </li>
        </ul>
        <component-section>
          <div slot="title">Vergelijkbaarheid van classes met <code class="lang-java">Comparable</code></div>
          <p>
            <code class="lang-java">Comparable</code> is een generic interface in Java. Een class kan
            <code class="lang-java">Comparable</code> implementeren, om aan te geven hoe zijn objecten qua inhoud
            vergeleken worden met (andere) objecten.
          </p>
          <p class="bron">
            <a href="https://www.baeldung.com/java-comparator-comparable">Comparator and Comparable in Java</a>
            (negeer <code class="lang-java">Comparator</code> als je wilt).
          </p>
        </component-section>
      </component-section>
      <component-section>
        <div slot="title">Bijeenkomst 2020-09-15</div>
        <component-section>
          <div slot="title">Welkom (20:00)</div>
          <ul>
            <li><p>Aanwezigheidsregistratie.</p></li>
            <li>
              <p>Heeft iedereen uitnodiging voor Miro geaccepteerd?</p>
            </li>
            <li><p>De structuur van deze les.</p></li>
          </ul>
        </component-section>
        <component-section>
          <div slot="title">Onderzoek naar performance (OP) (20:06)</div>
          <p>Vorige week hebben jullie de taak genoteerd in je Planner-plan o.i.d. dat jullie</p>
          <ul>
            <li><p>de opdrachttekst van OP en</p></li>
            <li><p>het beoordelingsmodel</p></li>
          </ul>
          <p>door zouden nemen.</p>
          <p>Reacties?</p>
        </component-section>
        <component-section>
          <div slot="title">Zelf bestuderen en basisopgaven maken (20:15)</div>
          </li>
          <p>
            <a href="https://secure.ans-delft.nl/universities/13/courses/43101/assignments/79473/quiz/start"
              >Basisopgaven les 2: Abstract data types en operaties</a
            >
          </p>
          <ul>
            <li>
              <p>
                Een datastructuur is in broncode niet per se een class, en hoeft ook geen adt-interface te
                implementeren. Dat hangt helemaal ervan af of de programmeertaal bijv. object-georiënteerd is.
              </p>
            </li>
            <li>
              <p>Amortized algorithm analysis en deques hoef je niet te kennen.</p>
            </li>
          </ul>
        </component-section>
        <component-section

        >
          <div slot="title">Vrijwilligers: oefenen met Visual Studio Code Live Share? (21:00)</div>
      </component-section>
        <component-section>
          <div slot="title">Mindmap maken over adt’s, datastructuren en operaties (tweetallen) (21:15)</div>
          <component-exercise together>
            <component-section>
              <div slot="title">Bespreking (21:20)</div>
            </component-section>
          </component-exercise>
        </component-section>
        <component-section>
          <div slot="title">Volgende bijeenkomst (21:25)</div>
          <ul>
            <li>
              Voor de volgende bijeenkomst komt er een klein beetje leerinhoud bij in deze les, over
              <em>generic programming</em> in Java.
              <ul>
                <li>
                  Dit omdat
                  <ul>
                    <li>
                      <p>het ook in de voltijdsvariant zit, en</p>
                    </li>
                    <li>
                      <p>ik een samenopgave heb waar je deze kennis voor nodig hebt.</p>
                    </li>
                  </ul>
                </li>
              </ul>
            </li>
          </ul>
        </component-section>
      </component-section>
      <component-section>
        <div slot="title">Bijeenkomst 2020-09-17</div>
        <component-section>
          <div slot="title">Extra voorbereiding</div>
          <ul>
            <li>
              <p>Miro boards:</p>
              <ul>
                <li>
                  <p>Tweetallen gevormd via Ans-assignment voor samenopgaven.</p>
                </li>
                <li>
                  <p>
                    <a href="https://miro.com/app/board/o9J_klJ3MIw=/">Mind map les 2</a>
                  </p>
                </li>
              </ul>
            </li>
            <li class="instructor_only">
              <p>VS Code Live Share op GitLab-projecten:</p>
              <ul>
                <li>
                  <p>
                    <a href="https://gitlab.com/han-aim/dt-sd-asd/adp/generic-programming">Generic programming</a>
                  </p>
                </li>
                <li>
                  <p>
                    <a href="https://gitlab.com/han-aim/dt-sd-asd/adp/linked-lists">Linked lists</a>
                  </p>
                </li>
              </ul>
            </li>
          </ul>
        </component-section>
        <component-section>
          <div slot="title">Welkom (18:30)</div>
          <ul>
            <li><p>Aanwezigheidsregistratie.</p></li>
            <li>
              <p>
                Hoe is het gegaan? Neem even de tijd om de
                <a href="https://secure.ans-delft.nl/universities/13/courses/43101/assignments/95815/quiz/start"
                  >vragenlijst ‘Je huidige leerresultaten’</a
                >
                (weer) in te vullen.
              </p>
            </li>
            <li><p>De structuur van deze les.</p></li>
          </ul>
        </component-section>
        <component-section>
          <div slot="title">Bespreking ontbrekende (in Ans) basisopgave (18:35)</div>
          <component-exercise>
            <p>
              Beschrijf één of meer verschillen tussen een
              <code class="lang-java">ArrayList</code> en een statische array.
            </p>
            <ul class="instructor_only">
              <li>
                <p><code class="lang-java">ArrayList</code> heeft een variabele grootte, array een vaste grootte.</p>
              </li>
              <li>
                <p>
                  <code class="lang-java">ArrayList</code> verwijst naar de implementatie van de dynamische
                  rij-datastructuur in de Java standard library, terwijl een statische array verwijst naar de algemene
                  datastructuur.
                </p>
              </li>
              <li><p>… Meer verschillen?</p></li>
            </ul>
          </component-exercise>
        </component-section>
        <component-section>
          <div slot="title">Bespreking samenopgave les 1: ‘Meten van looptijden’ (18:40)</div>
          <component-exercise together>
            <ol>
              <li>
                <p>
                  Programmeer een lus met een lineaire, kwadratische en exponentiële groeiorde, die iets constants
                  uitvoert.
                </p>
              </li>
              <li>
                <p>Meet de looptijden met geschikte waardes voor \\(n\\).</p>
              </li>
              <li>
                <p>Exporteer deze looptijden naar een spreadsheet. Maak een werkblad per groeiorde.</p>
              </li>
              <li>
                <p>Bereken de groeiverhoudingen als extra kolommmen.</p>
                <p class="bron">
                  <a
                    href="https://www.ck12.org/algebra/identifying-function-models/lesson/Linear-Exponential-and-Quadratic-Models-BSC-ALG/"
                    >Identifying Function Models</a
                  >
                </p>
              </li>
              <li>
                <p>Maak grafieken van meetgegevens en kijk of je de punten kan matchen met de groeiorde.</p>
              </li>
            </ol>
          </component-exercise>
        </component-section>
        <component-section>
          <div slot="title">Generic programming</div>
          <p class="instructor_only">
            Uitleg aan de hand van
            <a href="https://gitlab.com/han-aim/dt-sd-asd/adp/generic-programming">broncode</a>.
          </p>
        </component-section>
        <component-section class="praktijk">
          <div slot="title">Broncodevoorbeeld stack en queue (19:00)</div>
          <p class="instructor_only">De website voor ASD past ook een stack en een queue toe. Leg uit.</p>
          <ol>
            <li>
              <p>
                Wat is de complexiteitscategorie van
                <code>push(item)</code> op deze stack en queue?
              </p>
            </li>
            <li>
              <p>Bevatten deze datastructuren voor prestatie-efficiëntie relevante implementatiedetails?</p>
            </li>
            <li>
              <p>Maakt het uit wat het antwoord op de eerste vraag is?</p>
            </li>
          </ol>
        </component-section>
        <component-section>
          <div slot="title">Samenopgaven</div>
          <p>
            <a href="https://secure.ans-delft.nl/universities/13/courses/43101/assignments/81313/quiz/start"
              >Samenopgaven les 2: Abstract data types en operaties</a
            >
          </p>
          <component-section>
            <div slot="title">Datastructuurtabel (tweetal) (19:20)</div>
            <component-exercise together>
              <p>
                Implementaties van adt’s zijn beschikbaar in (standaard)bibliotheken van vele programmeertalen. Maak in
                een tweetal een tabel over ten minste de volgende datastructuren:
              </p>
              <ol>
                <li><p>stack</p></li>
                <li><p>arraylist</p></li>
                <li><p>singly-linked list</p></li>
                <li><p>doubly-linked list</p></li>
                <li><p>queue</p></li>
              </ol>
              <p>
                Kopieer
                <a href="bron/Datastructuren.xlsx">deze spreadsheet</a>
                als beginnetje en deel het kopie binnen je tweetal. Werk online samen om de spreadsheet aan te vullen.
              </p>
              <p>
                Kies als <em>programmeertalen</em> ten minste Java, maar vul aan met andere voor jullie interessante
                programmeertalen.
              </p>
              <div class="instructor_only">
                <p>
                  <a href="https://devdocs.io/">DevDocs</a> biedt een snel overzicht van documentatie van
                  programmeertalen.
                </p>
                <p>
                  <a href="https://www.bigocheatsheet.com/">Big o cheatsheet</a>
                </p>
              </div>
              <component-section>
                <div slot="title">Bespreking (20:00)</div>
                <p>Drie tweetallen presenteren.</p>
              </component-section>
            </component-exercise>
          </component-section>
          <component-section>
            <div slot="title">A queue to the rescue (20:15) (tweetal)</div>
            <component-exercise together>
              <p class="praktijk">
                Aan het begin van de COVID-19-crisis werd gezegd dat niet iedereen getest kan worden, dus dat
                coronatestaanvragen geweigerd werden. Als het RIVM een queue had gebruikt, waren in ieder geval geen
                testaanvragers van de radar verdwenen. Wij programmeurs starten hierbij <em>The queuest against corona</em>!
              </p>
              <p>
                Ontwikkel een Proof of Concept (PoC) waarmee jullie aantonen dat een queue de beste manier is om
                testaanvragen te registreren.
              </p>
              <p>
                💡 <strong>Tip</strong>: gebruik
                <a href="https://docs.microsoft.com/en-us/visualstudio/liveshare/quickstart/share"
                  >Visual Studio Code Live Share</a
                >
                om deze <em>pair programming</em>-sessie efficiënt te maken.
              </p>
              <component-section>
                <div slot="title">Bespreking (20:45)</div>
                <ul class="instructor_only">
                  <li>
                    <p>Is de oplossing schaalbaar voor een realistisch aantal testaanvragen?</p>
                  </li>
                </ul>
              </component-section>
            </component-exercise>
          </component-section>
          <component-section>
            <div slot="title">De crisis dijt uit: extra requirements (20:55) (tweetal)</div>
            <component-exercise together>
              <p class="instructor_only">
                Hugo de Jonge belt ineens 😲. Er moet nu voortaan
                <a
                  href="https://www.volkskrant.nl/nieuws-achtergrond/voorrang-voor-onderwijs-en-zorg-in-de-teststraten-krijgt-tijdelijk-karakter~b1217577/"
                  >voorrang kunnen worden gegeven</a
                >
                aan bepaalde testaanvragen. Hij moet weer snel door naar de kapper en daarna naar de zonnebank 😆...
                Tijd voor jullie om snel resultaat te leveren!
              </p>
              <p class="instructor_only">
                Verbeter jullie PoC door relevante criteria mee te wegen bij het bepalen van een prioriteit van items in
                de queue.
              </p>
              <component-section>
                <div slot="title">Bespreking (21:15)</div>
                <p>Presentatie door twee tweetallen.</p>
              </component-section>
            </component-exercise>
          </component-section>
          <component-section extra>
            <div slot="title">Appending linked lists (mocht er tijd overblijven)</div>
            <component-exercise together>
              <p>
                Leg puntsgewijs alles uit wat relevant is voor de prestatie-efficiëntie voor de
                <code>append(list_from, list_to)</code>-operatie op linked lists in
                <a href="https://gitlab.com/han-aim/dt-sd-asd/adp/linked-lists">deze broncode</a>.
              </p>
              <p>
                Beantwoord bijv.: wordt de inhoud van
                <code class="lang-java">list2</code> door
                <code class="lang-java">listTo.addAll(listFrom)</code>
                gekopieerd, of alleen een verwijzing ernaar?
              </p>
              <p class="instructor_only">
                <a href="https://stackoverflow.com/a/52714457"
                  >Java <code class="lang-java">List.sort</code> vs. <code class="lang-java">Arrays.sort</code></a
                >.
              </p>
            </component-exercise>
          </component-section>
        </component-section>
      </component-section>`,
    document.getElementById('lessonAdt'),
  );
