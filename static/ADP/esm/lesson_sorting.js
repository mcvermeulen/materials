import { html, render } from 'lit-html';

export const renderLessonSorting = () =>
  render(
    html`<div slot="title">Sorteren</div>
      <component-section>
        <div slot="title">Wat je leert</div>
        <component-intendedlearningoutcomes>
          <component-intendedlearningoutcome codes='["AD-begrip-standaardalgoritmes-datastructuren"]'
            >legt
            <strong>binary search</strong>
            uit.</component-intendedlearningoutcome
          >
          <component-intendedlearningoutcome codes='["AD-begrip-standaardalgoritmes-datastructuren"]'
            >categoriseert de tijdcomplexiteit van binary search.</component-intendedlearningoutcome
          >
          <component-intendedlearningoutcome codes='["AD-begrip-standaardalgoritmes-datastructuren"]'>
            legt
            <strong>insertion sort</strong>
            uit.</component-intendedlearningoutcome
          >
          <component-intendedlearningoutcome codes='["AD-begrip-standaardalgoritmes-datastructuren"]'
            >categoriseert de tijdcomplexiteit van insertion sort.</component-intendedlearningoutcome
          >
          <component-intendedlearningoutcome codes='["AD-begrip-standaardalgoritmes-datastructuren"]'>
            legt
            <strong>selection sort</strong>
            uit.</component-intendedlearningoutcome
          >
          <component-intendedlearningoutcome codes='["AD-begrip-standaardalgoritmes-datastructuren"]'
            >categoriseert de tijdcomplexiteit van selection sort.</component-intendedlearningoutcome
          >
          <component-intendedlearningoutcome codes='["AD-begrip-standaardalgoritmes-datastructuren"]'
            >legt
            <strong>merge sort</strong>
            uit.</component-intendedlearningoutcome
          >
          <component-intendedlearningoutcome codes='["AD-begrip-standaardalgoritmes-datastructuren"]'
            >categoriseert de tijdcomplexiteit van merge sort.</component-intendedlearningoutcome
          >
          <component-intendedlearningoutcome codes='["AD-begrip-standaardalgoritmes-datastructuren"]'
            >legt
            <strong>Quicksort</strong>
            uit.</component-intendedlearningoutcome
          >
          <component-intendedlearningoutcome codes='["AD-begrip-standaardalgoritmes-datastructuren"]'
            >categoriseert de tijdcomplexiteit van Quicksort.</component-intendedlearningoutcome
          >
        </component-intendedlearningoutcomes>
        <component-miroboard idBoard="o9J_kg1ExyM" viewport="-490,-842,990,646" title="Mind map"></component-miroboard>
      </component-section>
      <component-section>
        <div slot="title">Waarom je over sorteren leert</div>
        <p>Sorteren/gesorteerdheid ligt aan de basis van de meeste praktische algoritmes.</p>
        <p class="praktijk">
          Databasesystemen zoals
          <a href="https://www.postgresql.org/docs/13/queries-order.html">PostgreSQL</a>
          kunnen data van hetzelfde type (bijv. een kolom) sorteren, zodat snel zoeken mogelijk is.
        </p>
        <p class="praktijk">
          In tegenstelling tot vele andere (standaard)algoritmes, zijn sorteeralgoritmes is niet alleen een bouwsteen
          voor complexere algoritmes, maar zeker ook rechtstreeks relevant voor functionele vereisten. Denk aan een
          spreadsheet-app, waarin je wilt kunnen sorteren.
        </p>
      </component-section>
      <component-section>
        <div slot="title">Binary search</div>
        <p>
          Om een rij te kunnen sorteren, is allereerst van belang dat de element van de rij <em>vergelijkbaar</em> zijn
          (een totale ordening hebben). Laten we beginnen met een voorbeeld van het nut van een gesorteerde rij.
        </p>
        <p>
          <em class="term">Binary search</em> is een eenvoudig algoritme voor efficiënt <em>zoeken</em> naar een element
          in een al gesorteerde rij. Binary search heeft in het gemiddelde en slechtste geval een betere looptijd (van
          \\(O(\\log(n))\\)) dan <em class="term">linear search</em> (van \\(O(n)\\)), zoals je leerde in
          <a href="#_1_1_Algoritmes_en_complexiteit">de les over algoritme-analyse</a>.
        </p>
        <p>Als de rij gesorteerd is kan je daardoor dus het schaalbaardere binary search gebruiken voor zoeken.</p>
        <p><em>Binary</em> verwijst overigens naar het opdelen van de rij in telkens twee deelrijen.</p>
        <p class="bron">Binary Search - Algorithm and Pseudo-code</p>
        <component-video videoid="2BhQxgIgXX4" title="Binary Search - Algorithm and Pseudo-code"></component-video>
        <component-section>
          <div slot="title">Algoritme-ontwerpparadigma binary search</div>
          <component-exercise>
            <p>Kevin Drumm beschouwt binary search als een divide-and-conquer-algoritme.</p>
            <p>
              Wat voor argument kan je geven voor de stelling dat binary search geen echt/standaard divide-and-conquer
              algoritme is?
            </p>
          </component-exercise>
        </component-section>
      </component-section>
      <component-section>
        <div slot="title">Selection sort</div>
        <p>
          Selection sort houdt in het telkens zoeken naar het grootste nog ongesorteerde element. Dat element zet
          selection sort telkens aan het einde van de rij.
        </p>
        <p>OpenDSA bevat een handige visualisatie en de broncode:</p>
        <p class="bron">
          <a href="https://opendsa-server.cs.vt.edu/ODSA/Books/Everything/html/SelectionSort.html">Selection Sort</a>
        </p>
        <p>
          Selection sort is weliswaar eenvoudig, maar heeft een looptijd van maar liefst \\(Θ(n^2)\\) in alle gevallen.
        </p>
      </component-section>
      <component-section>
        <div slot="title">Insertion sort</div>
        <p>
          <em class="term">Insertion sort</em> is een eenvoudig sorteeralgoritme, dat lijkt op hoe spelers van
          kaartspelen hun hand vaak sorteren.
        </p>
        <p class="bron">Insertion Sort Algorithm</p>
        <p>OpenDSA bevat een handige visualisatie en de broncode:</p>
        <p class="bron">
          <a href="https://opendsa-server.cs.vt.edu/ODSA/Books/Everything/html/InsertionSort.html">Insertion Sort</a>
        </p>
        <component-video videoid="TPyJ0k8y3Nc" title="Insertion Sort Algorithm"></component-video>
        <p>Rosetta Code bevat veel voorbeeldimplementaties in talloze programmeertalen:</p>
        <p class="bron">
          <a href="https://rosettacode.org/wiki/Sorting_algorithms/Insertion_sort">Sorting algorithms/Insertion sort</a>
        </p>
        <p>
          Insertion sort is nog steeds eenvoudig, maar heeft weer een looptijd in het gemiddelde en slechtste geval van
          maar liefst \\(Θ(n^2)\\). Insertion sort heeft wel een goede looptijd van \\(Θ(n)\\) in het beste geval, omdat
          deels al gesorteerde deelrijen de benodigde looptijd omlaag brengen. Het beste geval is echter lastig
          voorspelbaar, dus maakt insertion sort niet veel aantrekkelijker.
        </p>
      </component-section>
      <component-section>
        <div slot="title">Merge sort</div>
        <p>
          <em class="term">Merge sort</em> is een sorteeralgoritme met in alle gevallen een looptijd van \\(O(n
          \\log(n)\\), wat beter is dan de eerder vermelde gemiddelde en slechtste gevallen van insertion sort.
        </p>
        <p>
          Merge sort is gebaseerd op divide-and-conquer. Je kan merge sort recursief of iteratief formuleren en
          implementeren, maar veel basisuitleg gaat uit van een recursieve formulering.
        </p>
        <p>Jeff Erickson legt het algoritme en zijn geschiedenis uit:</p>
        <p class="bron">
          <a href="http://jeffe.cs.illinois.edu/teaching/algorithms/book/01-recursion.pdf">Mergesort</a>
          <cite data-id="6867189/PLHZEH2X" data-label="section" data-locator="1.4"></cite>
          (bewijzen behoren niet tot ADP)
        </p>
        <p>OpenDSA bevat een handige visualisatie en voorbeeldbroncode:</p>
        <p class="bron">
          <a href="https://opendsa-server.cs.vt.edu/ODSA/Books/Everything/html/Mergesort.html">Mergesort Concepts</a>
        </p>
        <p class="bron">Merge Sort 1 - The Algorithm</p>
        <component-video videoid="M5kMzPswo20" title="Merge Sort 1 - The Algorithm"></component-video>
        <p>Rosetta Code bevat veel voorbeeldimplementaties in talloze programmeertalen:</p>
        <p class="bron">
          <a href="https://rosettacode.org/wiki/Sorting_algorithms/Merge_sort">Sorting algorithms/Merge sort</a>
        </p>
        <component-section>
          <div slot="title">Waarom je merge sort leert</div>
          <p>
            In Java SE 14 wordt een variant van merge sort als standaardimplementatie van
            <code class="hljs lang-java">List.sort()</code>
            gebruikt. Het is dus zeker een standaardalgoritme.
          </p>
          <p class="bron">
            <a
              href="https://docs.oracle.com/en/java/javase/14/docs/api/java.base/java/util/List.html#sort(java.util.Comparator)"
              ><code class="hljs lang-java">List.sort()</code> API docs</a
            >.
          </p>
        </component-section>
      </component-section>
      <component-section>
        <div slot="title">Quicksort</div>
        <p>
          Quicksort is een algoritme dat in de praktijk vaak gebruikt wordt, omdat het in het gemiddelde geval een goede
          looptijd heeft van \\(O(n \\log(n))\\).
        </p>
        <p>
          Quicksort is ook het meest complexe sorteeralgoritme dat we behandelen, omdat er veel details in zitten die
          tussen implementaties kunnen verschillen. Bovendien wordt in de praktijk in Quicksortimplementaties ook een
          ander sorteeralgoritme als insertion sort aangeroepen zodra een deelrij erg klein is, want dan weegt de lage
          constante overhead van insertion sort zwaarder dan zijn in het gemiddelde geval slechte
          complexiteitscategorie.
        </p>
        <p>
          Net als merge sort is Quicksort gebaseerd op divide-and-conquer. Je kan Quicksort eveneens recursief of
          iteratief formuleren en implementeren, maar veel basisuitleg gaat uit van een recursieve formulering.
        </p>
        <p>Jeff Erickson legt het algoritme en zijn geschiedenis uit:</p>
        <p class="bron">
          <a href="http://jeffe.cs.illinois.edu/teaching/algorithms/book/01-recursion.pdf">Quicksort</a>
          <cite data-id="6867189/PLHZEH2X" data-label="section" data-locator="1.5-1.6"></cite>
          (bewijzen behoren niet tot ADP)
        </p>
        <p>OpenDSA bevat een handige visualisatie en voorbeeldbroncode:</p>
        <p class="bron">
          <a href="https://opendsa-server.cs.vt.edu/ODSA/Books/CS3/html/Quicksort.html">Quicksort</a>
        </p>
        <p class="bron">Quicksort 1 - The Algorithm</p>
        <component-video videoid="h_9kAXFKJwY" title="Quicksort 1 - The Algorithm"></component-video>
        <p>
          Net als bij merge sort kan je van Quicksort een goed leesbare recursieve implementatie uitprogrammeren, of een
          slechter leesbare maar in de praktijk efficiëntere iteratieve.
        </p>
        <p class="bron">Quicksort 2 - Alternative Algorithm</p>
        <component-video videoid="O5V5JTa3O20" title="Quicksort 2 - Alternative Algorithm"></component-video>
        <p>Rosetta Code bevat veel voorbeeldimplementaties in talloze programmeertalen:</p>
        <p class="bron">
          <a href="https://rosettacode.org/wiki/Sorting_algorithms/Quicksort">Quicksort</a>
        </p>
        <component-section>
          <div slot="title">Pivot</div>
          <p>
            Alleen de naam van het algoritme, ‘Quicksort’, zegt vergeleken met de meeste standaardalgoritmes erg weinig.
            De manier waarop de
            <em class="term">pivot</em> wordt gekozen is cruciaal, maar wordt in allerlei varianten van quicksort anders
            ingevuld. Dat met goede reden, want de invoerdata kan bijv. verschillen voor toepassen. Van alle manieren
            zijn er gelukkig maar enkele overgebleven die betrouwbaar bleken. Als het gaat om het aantal pivots:
          </p>
          <ul>
            <li>Enkele</li>
            <li>Dubbele</li>
          </ul>
          <p>En als het gaat om de keuze van de pivotwaarde:</p>
          <ul>
            <li>Random</li>
            <li>Middelste</li>
          </ul>
        </component-section>
        <p>In het slechtste geval heeft Quicksort een looptijd van \\(O(n^2)\\).</p>
        <component-section>
          <div slot="title">Waarom je Quicksort leert</div>
          <p>
            In Java SE 14 wordt een variant van Quicksort als standaardimplementatie van
            <code class="hljs lang-java">Arrays.sort()</code>
            gebruikt. Het is dus zeker een standaardalgoritme.
          </p>
          <p class="bron">
            <a href="https://docs.oracle.com/en/java/javase/14/docs/api/java.base/java/util/Arrays.html#sort(int%5B%5D)"
              ><code class="hljs lang-java">Arrays.sort()</code> API docs</a
            >
          </p>
        </component-section>
      </component-section>
      <component-section extra>
        <div slot="title">Complexiteitscategorie bij lijsten in het algemeen</div>
        <p>
          Tot nu toe hebben we het over het sorteren van rijen gehad, niet over lijsten in het algemeen. Insertion sort
          en Quicksort hebben een slechtere complexiteitscategorie op linked lists dan op rijen, omdat elementen niet in
          constante looptijd benaderd kunnen worden (zoals met indexeren in een rij wel kan). Een extreme oplossing zou
          kunnen zijn het omzetten van de linked list naar een rij, sorteren met bijv. Quicksort, en de rij weer
          omzetten naar een linked list. Dat heeft een looptijd van \\(O(n) + O(n \\log(n)) + O(n)\\), wat nog steeds
          beter is dan \\(O(n^2)\\). Een eenvoudige oplossing is het besef dat als je een linked list moet gaan
          sorteren, je misschien niet de beste datastructuur voor je algoritme hebt gekozen.
        </p>
        <p>
          Mocht je toch benieuwd zijn hoe verscheidene sorteeralgoritmes het doen op linked lists, lees dan
          <a href="https://citeseerx.ist.psu.edu/viewdoc/download?doi=10.1.1.83.3116&rep=rep1&type=pdf"
            >A comparative study of linked list sorting algorithms</a
          >.
        </p>
      </component-section>
      <component-section>
        <div slot="title">Keuzeproces Java SE 14 voor sorteeralgoritmes</div>
        <component-exercise>
          <p>
            Je hebt nu bij zowel merge sort als Quicksort gezien dat ze gebruikt worden in belangrijke
            datastructuur-implementaties binnen Java SE 14.
          </p>
          <p>
            Onderzoek zelf: hoe komt het dat de ontwikkelaars van Java SE 14 in deze gevallen
            <em>verschillende</em> sorteeralgoritmes kozen?
          </p>
        </component-exercise>
      </component-section>
      <component-section>
        <div slot="title">Bijeenkomst 2020-10-13</div>
        <component-section>
          <div slot="title">Welkom (20:00)</div>
          <ul>
            <li>
              <p>Aanwezigheidsregistratie.</p>
            </li>
            <li>
              <p>De structuur van deze les.</p>
            </li>
          </ul>
        </component-section>
        <component-section>
          <div slot="title">Zelf bestuderen en basisopgaven maken (20:15)</div>
          <p>
            <a href="https://secure.ans-delft.nl/universities/13/courses/43101/assignments/88292/quiz/start"
              >Basisopgaven les 4: Sorteren</a
            >
          </p>
        </component-section>
        <component-section>
          <div slot="title">Mindmap maken over sorteren (in tweetallen) (21:10)</div>
          <component-exercise together>
            <component-section>
              <div slot="title">Bespreking (21:20)</div>
            </component-section>
          </component-exercise>
        </component-section>
      </component-section>
      <component-section>
        <div slot="title">Bijeenkomst 2020-10-08</div>
        <component-section>
          <div slot="title">Welkom (18:30)</div>
          <ul>
            <li>
              <p>Aanwezigheidsregistratie.</p>
            </li>
            <li>
              <p>
                Hoe is het gegaan? Neem even de tijd om de
                <a href="https://secure.ans-delft.nl/universities/13/courses/43101/assignments/95815/quiz/start"
                  >vragenlijst ‘Je huidige leerresultaten’</a
                >
                (weer) in te vullen.
              </p>
            </li>
            <li>
              <p>De structuur van deze les.</p>
            </li>
          </ul>
        </component-section>
        <component-section>
          <div slot="title">Samenopgaven</div>
          <p>
            <a href="https://secure.ans-delft.nl/universities/13/courses/43101/assignments/89786/quiz/start"
              >Samenopgaven les 4: Sorteren</a
            >
          </p>
          <component-section>
            <div slot="title">Sorteren van speelkaarten met insertion sort (18:40)</div>
            <component-exercise together></component-exercise>
          </component-section>
          <component-section>
            <div slot="title">Sorteren van speelkaarten met merge sort (19:00)</div>
            <component-exercise together></component-exercise>
          </component-section>
          <component-section>
            <div slot="title">Sorteren van speelkaarten met Quicksort (19:30)</div>
            <component-exercise together></component-exercise>
          </component-section>
          <component-section>
            <div slot="title">Ontwikkel een sorteerder voor de 3D Basisvoorziening van Nederland (tweetal) (20:00)</div>
            <component-exercise together>
              <p>
                Het Kadaster heeft de
                <a
                  href="https://tweakers.net/nieuws/169816/het-kadaster-stelt-3d-bestand-van-nederland-beschikbaar-als-open-data.html"
                  >3D Basisvoorziening van Nederland</a
                >
                uitgebracht. Dat is een soort 3D kaart van allerlei
                <em>tegels</em> van Nederland.
                <a href="https://brt.kadaster.nl/basisvoorziening-3d/">Download</a>
                een willekeurige tegel via het kaartje. Ontwikkel een sorteerder voor dit
                <a href="https://www.cityjson.org/">CityJSON</a>-bestand in een programmeertaal naar keuze, en naar
                eigen inzicht.
              </p>
            </component-exercise>
          </component-section>
        </component-section>
      </component-section>`,
    document.getElementById('lessonSorting'),
  );
