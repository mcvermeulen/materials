import { html, render } from 'lit-html';

export const renderLessonProgramminglanguages = () =>
  render(
    html`<div slot="title">Programmeertalen</div>
    <component-section>
          <div slot="title">Formele talen en programmeertaaltheorie</div>
          <component-section >
            <div slot="title">Wat je leert</div>
            <component-intendedlearningoutcomes>
              <component-intendedlearningoutcome codes='["P-begrip-programmeertaal"]'
                >legt
                <strong>programmeertaal</strong>
                (<strong>syntaxis</strong>, <strong>semantiek</strong>, <strong>paradigma’s</strong>) uit.
              </component-intendedlearningoutcome>
              <component-intendedlearningoutcome codes='["P-begrip-programmeertaal"]'>legt <strong>grammatica</strong> uit. </component-intendedlearningoutcome>
              <component-intendedlearningoutcome codes='["P-begrip-programmeertaal"]'
                >categoriseert
                <strong>standaardprogrammeertalen</strong>
                in paradigma’s.
              </component-intendedlearningoutcome>
            </component-intendedlearningoutcomes>
          </component-section>
          <component-section >
            <div slot="title">Waarom je over formele talen en programmeertaaltheorie leert</div>
            <p>
              Met o.a. Java maak je van broncode een uitvoerbaar programma. Hoe werkt dit? Globaal hetzelfde als bij iedere programmeertaal. Door de grondslagen van programmeertalen beter te begrijpen, leer je sneller willekeurige nieuwe programmeertalen. En dat is in het werk van een hbo-niveau software-ontwikkelaar geen overbodige luxe
              <span class="praktijk"></span>.
            </p>
          </component-section>
          <component-section >
            <div slot="title">Programma’s optimaliseren</div>
            <p>Als broncode omzetten naar een programma een simpele vertaalslag was, zouden programma’s ofwel erg traag zijn, of zou broncode heel erg ingewikkeld zijn.</p>
            <p>Li Haoyi legt uit hoe een compiler broncode als het ware herschrijft naar optimalere broncode. Bestudeer alles tot ‘Intermediate Representations’.</p>
            <p class="bron">
              <a href="https://www.lihaoyi.com/post/HowanOptimizingCompilerWorks.html">How an Optimizing Compiler Works</a>
            </p>
          </component-section>
          <component-section >
            <div slot="title">Syntaxis</div>
            <p>
              Om van broncode een programma te maken, moet een
              <em class="term">compiler</em> broncode exact kunnen ontleden. De regels daarvoor heten de <em class="term">syntaxis</em> (Engels: <em class="term">syntax</em>).
            </p>
            <p>Binnen syntactische analyse (met andere woorden, ontleding op basis van de vorm van de tekst) door een compiler zou je twee stappen kunnen onderscheiden:</p>
            <ul>
              <li>
                <p>
                  <em class="term">Lexing</em>, ook bekend als
                  <em class="term">tokenization</em>
                </p>
              </li>
              <li>
                <p><em class="term">Parsing</em></p>
              </li>
            </ul>
            <component-section >
              <div slot="title">Lexing</div>
              <p>De broncode kan je eerst voorbewerken zodat er minder variatie in zit. Stel, je hebt de volgende Java-broncode:</p>
              <figure>
                <figcaption>Een functiedefinitie in Java. Bron: ‘How an Optimizing Compiler Works’.</figcaption>
                <pre><code class="lang-java">static int ackermann(int m, int n) {
  if (m == 0) return n + 1;
  else if (n == 0) return ackermann(m - 1, 1);
  else return ackermann(m - 1, ackermann(m, n - 1));
}</code></pre>
              </figure>
              <p>Voor de compiler is de voorgaande broncode gelijk aan:</p>
              <figure>
                <figcaption>Een functiedefinitie in Java. Bron: ‘How an Optimizing Compiler Works’.</figcaption>
                <pre><code class="lang-java">static int ackermann(int m,int n) {
                    // Dit is de Ackermann-functie.
  if(m==0 )
                                           return n +1;
  else if ( n ==0) return ackermann(m -
    1,1); else return ackermann(m-1   , ackermann               (m,n-1));
    /*
    Zag je niet wat er teruggegeven is?*/
}</code></pre>
              </figure>
              <p>
                En gelijk ook talloze andere variaties qua o.a. witruimte en regeleindes. Tijdens lexing wordt de broncode opgehakt in
                <em class="term">tokens</em>. Naast van witruime en regeleindes hangt dat ook af van de regels over geldige karakters (bijvoorbeeld alleen ASCII en printbare karakters). In de vorige twee broncodefragmenten blijven alleen de helder gekleurde karakters over (oftewel, alles op de witruimte en commentaren na). Lexing werkt volgens <em class="strong">lexicale regels</em>. Die regels verschillen tussen talen. Zo is de lengte van witruimte in Python soms betekenisvol, en mag die dus niet opgeslokt worden door de lexer. In Java is de lengte van witruimte niet betekenisvol.
              </p>
            </component-section>
            <component-section >
              <div slot="title">Parsing</div>
              <p>De Java-broncode <code class="lang-java">getal++;</code> is bijvoorbeeld syntactisch geldig. Dat geldt niet voor <code>getal;++;</code>. Jij weet dit inmiddels in een oogopslag. Maar de regels hiervoor verschillen natuurlijk per taal. Dat ligt vast in een <em class="term">grammatica</em>. Zoals je ziet, lijken deze concepten allemaal aardig op wat je op school leert over menselijke talen (<em class="term">natuurlijke talen</em>). De wetenschappelijke term voor ‘computertalen’ is <em class="term">formele talen</em>.</p>
              <p>Grammatica’s kunnen voor de compiler worden vastgelegd in weer een <em>eigen</em> formele taal. Want hoe weet de compiler anders de grammatica van een grammatica? Er zijn een aantal grammaticatalen, bijvoorbeeld BNF, EBNF en ANTLR 4. In ADP leer je over ANTLR 4, want dat is niet alleen een grammaticataal, maar ook een heel praktisch framework om parsers te bouwen.</p>
            </component-section>
          </component-section>
          <component-section >
            <div slot="title">Semantiek</div>
            <p>
              De vraag is nu nog: hoe weet de compiler de betekenis van de geanalyseerde broncode? Zonder die betekenis kan hij natuurlijk geen specifiek programma maken op basis van de broncode. De betekenis van een programmeertaal wordt zijn
              <em class="term">semantiek</em> genoemd. ‘Semantiek’ is gewoon een synoniem voor ‘betekenis’, maar is de wetenschappelijke term. Als de term semantiek wordt gebruikt, wordt vaak bedoeld dat naar een exacte betekenis gezocht wordt. Precies het soort betekenis waar een compiler wat mee kan. ‘Dit is de Ackermann-functie’ is in principe óók een semantische bewering (intepretatie), maar alleen een mens kan die begrijpen en controleren.
            </p>
            <p>Een programmeertaal kan een formele semantiek hebben. In dat geval kan je wiskundig verifiëren wat een gegeven programma uitgedrukt in broncode precies doet. Dit is echter een zeldzaamheid, en niet het soort programmeertalen en toepassingen waar een hbo’er mee te maken krijgt. Een programmeertaal kan ook een informele specificatie hebben, waarin de semantiek wordt beschreven (soms heel compleet en grondig), maar niet exact en bewezen. Nog eenvoudiger is om geen specificatie (een vorm van ontwerpdocumentatie) te schrijven, maar alleen een implementatie. In zo’n geval is de semantiek van de taal simpelweg de semantiek van de referentie-implementatie. Met andere woorden, wat de door de compiler gemaakte programma’s feitelijk doen, is kennelijk wat de bedoeling is. Natuurlijk klopt dat niet altijd. Dan ontstaat er discussie, en wordt de compiler en daarmee de semantiek van de taal zelf bijgeschaafd.</p>
            <p>
              Java is gespecificeerd in
              <a href="https://docs.oracle.com/javase/specs/jls/se15/html/index.html">de <em>Java Language Specification</em> (JLS)</a>. De JLS behandelt de <a href="https://docs.oracle.com/javase/specs/jls/se15/html/jls-2.html">grammatica</a>, de
              <a href="https://docs.oracle.com/javase/specs/jls/se15/html/jls-3.html">lexicale regels</a>
              en de rest ervan is gewijd aan de semantiek.
            </p>
          </component-section>
          <component-section >
            <div slot="title">Paradigma’s</div>
            <p>Stukjes syntaxis met bijbehorende semantiek komen vaak overeen tussen meerdere programmeertalenn. Zo is de bekende <code>for</code>-lus bijna universeel in de wereld van programmeertalen. Zelfs de syntaxis van zo’n soort lus komt vaak nauw overeen (bijv. tussen Java, C, C++ en PHP). De (mogelijke) overeenkomsten tussen programmeertalen kan je categoriseren. Als er een bepaalde gemene deler is, zoals object-georiënteerd programmeren, dan noem je zoiets een <em class="strong">(programmeertaal)paradigma</em>. Paradigma is een wetenschappelijk woord voor een manier van dingen doen die belangrijk is in de geschiedenis (in dit geval: die van het programmeren). Het gaat dus niet om een syntactisch detail zoals de <a href="https://towardsdatascience.com/the-walrus-operator-7971cd339d7d">Walrus-operator</a> binnen die ene taal Python. Nee, het gaat om terugkerende patronen. Het verschil met een <em>design pattern</em> is dat een pattern gericht is op het oplossen van bepaalde soorten problemen, terwijl een paradigma een algemene stijl is die (eventueel) in (allerlei) patterns terug kan komen. Verder kan je een pattern in allerlei syntactische vormen uitprogrammeren, terwijl een paradigma sterker herkenbaar is aan een bepaalde syntaxis. Als de programmeertaal de boormachine is, dan is het pattern de specifieke boorkop, en het paradigma een eigenschap van meerdere modellen boormachines (zoals een hamerfunctie).</p>
            <p>Peter Van Roy geeft in het volgende hoofdstuk een inleiding tot, en geschiedenis van, programmeertaalparadigma’s. Zijn hele hoofdstuk is uiteindelijk relevant, dus lees het helemaal. Filter de de minder relevante (academische) passages er zelf uit. Je hoeft niet alle paradigmatische concepten te begrijpen nu, zolang je ze maar leert herkennen en enigszins van elkaar kan onderscheiden. In dit hoofdstuk komt veel kennis samen die je ook eerder in je studie hebt opgedaan.</p>
            <p class="bron">
              <a href="https://info.ucl.ac.be/~pvr/VanRoyChapter.pdf">Programming Paradigms for Dummies: What Every Programmer Should Know</a>
            </p>
          </component-section>
          <component-section >
            <div slot="title">Rekenkundige uitdrukkingen</div>
            <p>Rekenkundige uitdrukkingen (<em class="term">arithmetic expressions</em>) zijn sommen bestaande uit operanden, operatoren en haakjes. Bijvoorbeeld: \\((1 + 2) * 3 * 0\\). In dit voorbeeld zijn \\(+\\) \\(*\\) de <em class="term">operatoren</em>. \\(1\\), \\(2\\), \\((1+2)\\), \\(3\\) en \\(0\\) zijn de <em class="term">operanden</em>.</p>
            <p>Rekenkundige uitdrukkingen komen voor in heel veel formele talen, ook in Java en ICSS (van het <a href="Beroepsproduct_compiler/">beroepsproduct Compiler</a>). Een compiler zet de hele broncode om naar een boomstructuur, en ook de rekenkundige expressies erin.</p>
            <p>Uitdrukkingen met binaire operatoren (dwz. met twee operanden) kan je ook als een binaire boom opschrijven (representeren). Er is een korte Wikipedia-pagina over <em class="term">binaire expressiebomen</em>:</p>
            <p class="bron"><a href="https://en.wikipedia.org/wiki/Binary_expression_tree">Binary expression tree</a></p>
            <p>Je kan een rekenkundige uitdrukking in drie volgordes opschrijven. Deze volgordes lijken op de volgordes van het aflopen van binaire bomen:</p>
            <ol>
              <li>
                <p>Prefix: \\(+\\:1\\:2\\)</p>
              </li>
              <li>
                <p>Infix: \\(1+2\\) (deze manier zijn mensen gewend)</p>
              </li>
              <li>
                <p>Postfix: \\(1\\:2\\:+\\)</p>
              </li>
            </ol>
            <p>
              De volgorde waarop je alle sommen correct berekent heet de <em class="term"><a href="https://nl.wikipedia.org/wiki/Bewerkingsvolgorde">bewerkingsvolgorde</a></em> (<em class="term">operator precedence</em>). Sommige operatoren moet je eerst toepassen, en haakjes dwingen ook voorrang af. Ongeacht hoe je een som uitdrukt, moet je deze regels hanteren om dezelfde resultaten te krijgen als je hebt geleerd tijdens rekenlessen.
            </p>
            <p>Het shunting-yardalgoritme kan infix-uitdrukkingen omzetten naar postfix. Door de operatoren en operanden op één stack op te slaan en daarna de som stapsgewijs uit te rekenen, terwijl het items van de stack popt.</p>
            <p class="bron">
              <a href="https://nl.wikipedia.org/wiki/Shunting%2dyardalgoritme">Shunting-yardalgoritme</a>
            </p>
            <p>Een postfix-uitdrukking is relatief makkelijk (handmatig) te berekenen met het volgende algoritme. Loop van links naar rechts de operanden en operatoren (tokens) in de uitdrukking af. Push ieder token op één aanvankelijk lege stack, op de volgende manier. Zodra het token een operator is, pop je twee operanden van de stack en bereken je die deelsom. De uitvoer daarvan push je weer naar de stack, en zo ga je verder tot het einde van de uitdrukking.</p>
          </component-section>
      </component-section>
      <component-section>
        <div slot="title">Implementatie van programmeertalen</div>
          <component-section >
            <div slot="title">Wat je leert</div>
            <component-intendedlearningoutcomes>
              <component-intendedlearningoutcome codes='["P-begrip-compiler"]'>
                legt standaard
                <strong>architectuurpatronen</strong>
                in <strong>compilers</strong> en <strong>interpreters</strong> uit.
              </component-intendedlearningoutcome>
              <component-intendedlearningoutcome codes='["P-begrip-compiler"]'>
                legt
                <strong>abstracte syntactische boom</strong>
                uit.
              </component-intendedlearningoutcome>
              <component-intendedlearningoutcome codes='["P-begrip-compiler"]'> legt <strong>lexer</strong> uit. </component-intendedlearningoutcome>
              <component-intendedlearningoutcome codes='["P-begrip-compiler"]'>legt <strong>parser</strong> uit. </component-intendedlearningoutcome>
              <component-intendedlearningoutcome codes='["P-begrip-compiler"]'>legt <strong>codegeneratie</strong> uit. </component-intendedlearningoutcome>
              <component-intendedlearningoutcome codes='["P-begrip-compiler"]'>
                legt
                <strong>programmatransformatie</strong>
                uit.
              </component-intendedlearningoutcome>
            </component-intendedlearningoutcomes>
          </component-section>
          <component-section >
            <div slot="title">Compilers en interpreters</div>
            <p>Bekijk eerst de inleiding in compilers van Kevin Drumm:</p>
            <p class="bron">Compilation (videoserie)</p>
            <component-video videoid="PLTd6ceoshpreZuklA7RBMubSmhE0OHWh_" isplaylist title="Compilation"></component-video>
            <p>Na de voorgaande inleiding heb je een vrij compleet en redelijk gedetailleerd beeld van het ontwerp van compilers. In het volgende stip ik enkele hoofdpunten aan die je in ieder geval hieruit moet halen.</p>
            <component-section >
              <div slot="title">Parsing</div>
              <p>In een compiler is parseren geïmplementeerd als een pipeline van transformaties van iedere representatie naar de volgende representatie:</p>
              <ol>
                <li>
                  <p>van broncode naar boomdatastructuren (<em class="term">syntax trees</em>)</p>
                </li>
                <li>
                  <p>naar verscheidene tussenvormen van ruwe code (<em class="term">intermediate representations (IR)</em>)</p>
                </li>
                <li>
                  <p>en uiteindelijk naar <em class="term">objectcode</em> (een uitvoerbaar programma)</p>
                </li>
              </ol>
              <p>Lees Li Haoyi’s uitleg over deze representaties in de paragraaf ‘Intermediate Representations’.</p>
              <p class="bron">
                <a href="https://www.lihaoyi.com/post/HowanOptimizingCompilerWorks.html">How an Optimizing Compiler Works</a>
              </p>
              <p>Belangrijk is om te beseffen dat de compiler van een <em class="term">concrete syntactische boom</em> (<em class="term">CST</em>, <em class="term">parse tree</em>) een <em class="term">abstracte syntactische boom (AST)</em> maakt, waarin bijvoorbeeld variabelennamen niet meer precies zo hoeven te zijn als in de broncode, en waarop de compiler met allerlei boomtransformaties optimalisaties toepast.</p>
              <component-section>
                <div slot="title">Java semantisch analyseren</div>
                <component-exercise>
                  <p>
                    Geef voorbeelden van
                    <a href="https://youtu.be/8nBoVjEOCMI?t=725">alle in dit shot opgesomde semantische fouten</a>
                    aan de hand van een broncodefragment in Java.
                  </p>
                </component-exercise>
              </component-section>
            </component-section>
            <component-section >
              <div slot="title">Evaluatie en checking</div>
              <p><em class="term">Evaluatie</em> houdt in dat een AST afgelopen wordt en dat de waarde opgevraagd wordt van deelbomen. Stel, een deelboom is een binary expression tree. Dan kan je die deelboom in principe vervangen door de uitkomst van die som.</p>
              <p><em class="term">Checking</em> is een synoniem voor semantische analyse. <em class="term">Type checking</em> is een van de bekendste en misschien wel belangrijkste vormen van checking. Als variabelen een vast datatype hebben, is de correctheid van een programma veel makkelijker te garanderen. Type checking verzekert dat het datatype is wat het moet zijn bij alle knopen van de AST die een waarde voorstellen.</p>
            </component-section>
            <component-section >
              <div slot="title">Transformatie en codegeneratie</div>
             <p><em class="term">Transformatie</em> houdt bij compilers in dat een boomstructuur, zoals een CST of AST verbouwd wordt. Net als een kok zijn ingrediënten voorbewerkt, is het handig voor een compiler om de boomstructuren te versimpelen.</p>
             <p><em class="term">Generatie</em> lijkt op transformatie, maar hierbij wordt de vorm wezenlijk anders. Bijvoorbeeld AST → IR. Of IR → uitvoerbaar programma (objectcode), etc.</p>
            </component-section>
          </component-section>
          <component-section >
            <div slot="title">Soorten compilers</div>
            <p>Een <em class="term">interpreter</em> draait het programma als script (rechtstreeks) of bytecode (abstracte objectcode) en wijzigt zijn interne toestand, zonder de broncode concreet naar <em class="term">machine code (native code)</em> te vertalen. Een <em class="term">compiler</em> vertaalt de broncode naar een uitvoerbare vorm in native code.</p>
            <p>Er zijn twee veel voorkomende vormen van compilatie:</p>
            <ul>
              <li><p><em class="term">Ahead-of-time (AOT)</em>: builden van broncode naar een uitvoerbaar programma gecodeerd in native code.</p></li>
              <li><p><em class="term">Just-in-time (JIT)</em>: compiler gecombineerd met een interpreter. <em>At runtime</em> zelfstandig bytecode naar native code compileren. Het programma wordt in het geheugen gehouden van een soort compiler die voor de hele levensduur van het programma blijft lopen (dubbelzinnig een <em class="term">Virtual Machine</em> genoemd), zoals de Java Virtual Machine. De doelcode is geen native code, maar een machineonafhankelijke <em class="term">bytecode</em>. Voor optimalisatie wordt de bytecode die het vaakst nodig is alsnog naar native code gecompileerd door de VM.</p></li>
            </ul>
          </component-section>
          <component-section >
            <div slot="title">ANTLR 4</div>
            <p>ANTLR is een parsing framework dat werkt volgens het principe: ‘schrijf eerst een programmeertaalonafhankelijke grammatica, dan genereer ik broncode voor je met een lexer en parser’. In ons geval gebruiken we Maven en Java om de parser generator te compileren naar een uitvoerbare parser. Dit soort software heeft een <em class="term">parser generator</em>.</p>
            <p>De parser is maar een library, die wij vervolgens benutten als module voor de ICSS tool. We moeten zelf nog wel veel Java-broncode afprogrammeren om van de CST een AST te maken, en voor semantische checking, optimalisatie en evaluatie, en generatie van normale CSS.</p>
            <figure>
              <figcaption>De workflow van het toepassen van een parser generator zoals ANTLR.</figcaption>
              <img src="svg/Parser_generator.min.svg" alt="Parser generator" />
            </figure>
            <p>Het belangrijkste dat je moet leren van ANTLR is zijn grammaticataal. In ANTLR worden grammatica’s bewaard als <code>.g4</code>-bestanden. Aan de hand van enkele voorbeelden leer je meer hierover. Voor je in de grammatica’s duikt, moet je even de <a href="https://github.com/antlr/intellij-plugin-v4">‘ANTLR v4 grammar’ plugin</a> installeren in je IntelliJ IDEA IDE.</p>
            <component-section >
              <div slot="title">Een parser voor configuratiebestanden bouwen</div>
              <p>In het volgende leer je ANTLR aan de hand van een kleine voorbeeldtaal voor configuratiebestanden. Neem deze soort configuratiedata:</p>
              <figure>
                <figcaption>Voorbeeld van een simpele indeling voor configuratiebestanden (‘sleutel = waarde’).</figcaption>
                <pre><code>host = "server.example.com"
port = 8000
username = "tester"</code></pre>
              </figure>
              <p>Uit ervaring en intuïtief weet je al hoe je dit moet lezen, en wat de semantiek is. Hoe ziet een ANTLR-grammatica eruit voor dit soort invoerdata?</p>
              <figure>
                <figcaption>De ANTLR-grammatica <code>Configuration.g4</code> voor de simpele indeling voor configuratiebestanden.</figcaption>
                <pre><code class="lang-antlr4">grammar Configuration;

// Syntactische regels
// Eén of meer property.
properties: property+;
// Een ID-token, een letterlijk =-token, en een value.
property: ID '=' value;
// Een INT of een STRING.
value:  INT | STRING;

// Lexicale regels
// Eén of meer alfabetische karakters.
ID: [a-zA-Z]+;
// Een letterlijk dubbel aanhalingsteken, 0 of meer keer een willekeurig karakter, een letterlijk dubbel aanhalingsteken.
STRING: '"' .*? '"';
// Eén of meer cijfers. 09 is dus ook een geldig token.
INT: [0-9]+;
// Speciale regel: negeer witruimte.
WS: [ \\t\\r\\n]+ -> skip;
</code></pre>
              </figure>
              <p>Wil je meer details weten over grammatica’s ontwikkelen in ANTLR, raadpleeg dan de handleiding.</p>
              <p class="bron"><a href="https://github.com/antlr/antlr4/blob/master/doc/grammars.md">Grammar Structure</a></p>
              <p class="bron"><a href="https://github.com/antlr/antlr4/blob/master/doc/lexicon.md">Grammar Lexicon</a></p>
              <p>Je hoeft voor ADP niet meer te weten over ANTLR 4 dan:</p>
              <ul>
                <li>
                  <p>het sleutelwoord <code class="lang-antlr4">grammar</code>,</p>
                </li>
                <li>
                  <p>dat syntactische regels moeten beginnen met een <em>kleine letter</em>,</p>
                </li>
                <li>
                  <p>dat alternatieve gevallen gescheiden worden met een <code class="lang-antlr4">|</code>,</p>
                </li>
                <li>
                  <p>dat lexicale regels juist met <em>hoofdletters</em> moeten beginnen (maar schrijf ze volledig in hoofdletters),</p>
                </li>
                <li>
                  <p>en hoe de volgorde van regels belangrijk is,</p>
                </li>
                <li>
                  <p>dat strings/letters met enkele aanhalingstekens <code class="lang-antlr4">'…'</code> geschreven moeten worden,</p>
                </li>
                <li>
                  <p>dat je bepaalde <em class="term">sleutelwoorden</em> (<em class="term">keywords</em>) niet in namen mag gebruiken, net zoals in programmeertalen,</p>
                </li>
                <li>
                  <p>dat je meerregelig commentaar kan toevoegen met <code class="lang-antlr4">/* … */</code> en enkelregelig met <code class="lang-antlr4">// …</code>.</p>
                </li>
              </ul>
              <!-- TODO: Nu is het niet mogelijk om witruimte binnen strings te hebben. -->
              <component-section >
                <div slot="title">De grammatica debuggen</div>
                <p>Je zal niet in één keer de juiste grammatica ontwikkelen. Net als bij programmeren, doorloop je een cyclus waarin je stapsgewijs een grammatica ontwikkelt die alle mogelijke invoer aankan.</p>
                <p>Ga naar het grammaticabestand in je IntelliJ-IDE. Rechtsklik op de hoofdregel (<em class="term">start rule</em>). Dat is de regel die door géén andere regel aangeroepen wordt. Als het goed is is dat er maar één. Het is handig om deze regel ook bovenaan, direct onder <code class="lang-antlr4">grammar</code> te zetten.</p>
                <figure>
                  <figcaption>Selecteer de hoofdregel in je grammatica. Ga via het contextmenu naar ‘Test Rule <em>(regelnaam)</em>’.</figcaption>
                  <img src="raster/IntelliJ_ANTLR_Hoofdregel.png" alt="Selecteer de hoofdregel in je grammatica. Ga via het contextmenu naar ‘Test Rule regelnaam’." />
                </figure>
                <p>Nu zie je onderaan je scherm een paneel met links een tekstinvoerveld, en rechts een canvas voor een CST. Voer voorbeelddata in die de grammatica zou moeten herkennen en ontleden. Controleer daarna telkens of de CST passend is. Je moet straks in Java de CST verwerken. Het is belangrijk dat de structuur van de CST niet sterk afwijkt van geval tot geval. Het is minder erg om een hoge CST te hebben, dan om een CST te hebben waarin dezelfde syntactische regels op wisselende lagen in CSTs terecht komen afhankelijk van de toevallige invoer. Een andere grote bron van problemen is het feit dat lexicale regels altijd vóór syntactische regels worden toegepast. Lexicale regels zijn een botte bijl. Pas op met het uitbreiden van lexicale regels. Geef de voorkeur aan syntactische regels om invoer te verwerken.</p>
                <figure>
                  <figcaption>Het ANTLR Preview-paneel.</figcaption>
                  <img src="raster/IntelliJ_ANTLR_CST.png" alt="Het ANTLR Preview-paneel." />
                </figure>
              </component-section>
              <component-section >
                <div slot="title">De parser afprogrammeren</div>
                <p>Nadat je alle testinvoerdata succesvol hebt gedebugd in combinatie met je grammatica, wordt het tijd om je parser te bouwen.</p>
                <p>
                  In ADP en standaard-ANTLR 4 gebruik je bepaalde vaste opzet van je project. Zie voor de configuratieparser het <a href="https://gitlab.com/han-aim/vt_asd_app_codeexamples/-/tree/master/lexing_parsing">GitLab-voorbeeldproject <code>lexing_parsing</code></a
                  >. Allereerst werken we met Maven, waarin je in je <a href="https://gitlab.com/han-aim/vt_asd_app_codeexamples/-/blob/master/lexing_parsing/pom.xml"><code>pom.xml</code></a> <code>antlr4-runtime</code> als dependency en <code>antlr4-maven-plugin</code> als Maven-plugin moet opnemen. In <a href="https://gitlab.com/han-aim/vt_asd_app_codeexamples/-/tree/master/lexing_parsing/src/main/antlr4/nl/han/asd/app/lexing_parsing"><code>src/main/antlr4/</code>(package-pad)<code>/</code></a> zet je je grammaticabestand. In een gewone package zet je parser op. In dit voorbeeldproject gebeurt dat eventjes vanuit de <a href="https://gitlab.com/han-aim/vt_asd_app_codeexamples/-/blob/master/lexing_parsing/src/main/java/nl/han/asd/app/lexing_parsing/Main.java">main-class</a>. De broncode die je achter de voorgaande link ziet spreekt voor zich. Je krijgt hier uiteindelijk een <code class="lang-java">ParseTree</code>-object (<a href="https://www.javadoc.io/doc/org.antlr/antlr4-runtime/4.7.2/org/antlr/v4/runtime/tree/ParseTree.html">Javadoc</a>) met de CST. Daar kunnen we mee doen wat we willen. We hoeven in dit voorbeeld niets te compileren. In dit voorbeeld bouwen we dus ook geen AST meer op, want we hebben genoeg aan de CST. We kunnen de configuratiedeclaraties eventueel aflopen op de <code class="lang-java">ParseTree</code> en ze in een dictionary (property → value) zetten. Dan moet je wel even nadenken over het juiste datatype voor de value.
                </p>
                <p></p>
              </component-section>
            </component-section>
            <component-section >
              <div slot="title">Een compiler ontwikkelen</div>
              <p>In het vorige voorbeeld heb je ANTLR leren kennen. We zijn gestopt na een minimale parser te hebben voltooid. Als je compiler ontwikkelt, zal je echter veel meer werk moeten doen.</p>
              <component-section >
                <div slot="title">De parser</div>
                <p>Lees eerst uit de handleiding van ANTLR:</p>
                <p class="bron"><a href="https://github.com/antlr/antlr4/blob/master/doc/listeners.md">Parse Tree Listeners</a></p>
                <p>Lees ook tot en met ‘Rule Context Objects’:</p>
                <p class="bron"><a href="https://github.com/antlr/antlr4/blob/master/doc/parser-rules.md">Parser Rules</a></p>
                <p>In ADP werken we niet met een <em>visitor</em> voor de CST, maar met een <em>listener</em>. Voor het gemak beginnen we bij de startbroncode van het <a href="Beroepsproduct_compiler/"> beroepsproduct Compiler</a>.
                Begin bij <a href="https://github.com/michelportier/icss2021/blob/master/startcode/src/main/java/nl/han/ica/icss/Pipeline.java">de architectureel belangrijke class <code class="lang-java">Pipeline</code></a>. Bestudeer deze broncode grondig. Op regel 70 zie je een belangrijke verwijzing, namelijk naar de class <code class="lang-java">nl.han.ica.icss.parser.ASTListener</code></a>. Ga nu naar <code class="lang-java">nl.han.ica.icss.parser</code>. In <a href="https://github.com/michelportier/icss2021/blob/master/startcode/src/main/java/nl/han/ica/icss/parser/ASTListener.java"><code>ASTListener.java</code></a> moet het meeste werk gebeuren. Daarin is de class gedefinieerd die de AST moet gaan opbouwen. Hij overerft van een basisclass <code class="lang-java">ICSSBaseListener</code>. <code class="lang-java">ICSSBaseListener</code> wordt volledig gegenereerd door ANTLR. Dat gebeurt <a href="https://www.antlr.org/api/maven-plugin/latest/usage.html">als je de Maven-goal <code>antlr4</code> uitvoert</a> (deze goal wordt automatisch uitgevoerd tijdens de <code>generate-sources</code>-phase, vroeg binnen <a href="https://maven.apache.org/guides/introduction/introduction-to-the-lifecycle.html#lifecycle-reference">de default lifecycle van Maven</a>). Je kan dus ook de latere Maven-<code>compile</code>-phase telkens uitvoeren. Dan wordt telkens de broncode van de basisbouwstenen van de parser opnieuw gegenereerd door ANTLR, nog voor de rest van je parserproject gecompileerd wordt.</p>
                <p>Als je de methods van <code class="lang-java">ICSSBaseListener</code> bekijkt, zie je <code class="lang-java">enterStylesheet</code> en <code class="lang-java">exitStylesheet</code>. Deze methods zijn automatisch vernoemd naar de al bestaande syntactische regel <code class="lang-antlr4">stylesheet</code>. Iedere van die methods zal jij in <code class="lang-java">ASTListener</code> overriden (in principe). Verder zie je extra standaardmethods. Deze hebben vrij voor de hand liggende functies, maar heb je niet nodig voor je compiler.</p>
                <p>In het geval van ICSS is <code class="lang-antlr4">stylesheet</code> de hoofdregel. Dus <code class="lang-java">enterStylesheet()</code> wordt als eerste aangeroepen (en stylesheet is dus de wortelknoop). Verdere regels waaruit de stylesheet opgebouwd wordt, worden vervolgens steeds dieper aangeroepen. (Maar die extra regels zijn nu nog niet gedefinieerd in de grammatica.) Zodra je in <code class="lang-java">visitTerminal()</code> komt, weet je dat één pad vanuit de wortel helemaal geopend is, want je zit dan in een bladknoop. De recursie is klaar, want de diepste regel, dus het ‘kleinste brokje’ data, is geopend. Nu worden in omgekeerde volgorde de passende <code class="lang-java">exit…()</code>-methods aangeroepen terwijl de recursie krimpt.
                </p>
                <p>
                  Terug naar werking van de compiler pipeline. Terwijl de invoerdata verwerkt wordt, matchen stukjes invoerdata met de regels. Op basis daarvan heeft ANTLR alvast de complete CST opgebouwd. Iedere knoop in de CST bevat het stukje data en de toegepaste regel. Bij het openen van iedere knoop tijdens aflopen van de CST roept ANTLR telkens de naar die regel vernoemde <code class="lang-java">enter…()</code>-method aan. In de documentatie van de
                  <a href="https://www.antlr.org/api/Java/org/antlr/v4/runtime/tree/ParseTreeWalker.html#walk(org.antlr.v4.runtime.tree.ParseTreeListener,%20org.antlr.v4.runtime.tree.ParseTree)"><code class="lang-java">walk</code>-method</a> lees je dat de CST depth-first afgelopen wordt. Aan de broncode kan je vaststellen <a href="https://github.com/antlr/antlr4/blob/master/runtime/Java/src/org/antlr/v4/runtime/tree/ParseTreeWalker.java#L23-L39">dat de volgorde pre-order is</a>. Iedere keer dat een knoop wordt geopend, wordt dus een ‘event afgevuurd’ via de passende <code class="lang-java">enter…()</code>-method call op de listener.</p>
                <p>ANTLR geeft niet alleen de desbetreffende CST-knoop mee aan iedere listener method call, maar zelfs de CST-deelboom (met die knoop als wortel). Je wilt nu tijdens de method calls met de meegegeven CST-deelbomen de AST steeds verder opbouwen. CST-deelbomen zijn bij ANTLR geïmplementeerd in de class <a href="https://www.antlr.org/api/Java/org/antlr/v4/runtime/ParserRuleContext.html"><code class="lang-java">ParserRuleContext</code></a>. De method <code class="lang-java">getText</code> is alles wat je minimaal nodig hebt. Maak bij iedere method een passend AST-object aan met elk van de classes in <code class="lang-java">nl.han.ica.icss.ast</code>. Om je broncode begrijpelijk te houden wil je dat je syntactische regels qua naam zo veel mogelijk overeenstemmen met de namen van de AST-classes. ⚠️ Klopt je grammatica totaal niet met de AST-classes, verbeter/verander dan eerst je grammatica.</p>
                <p>In <code class="lang-java">ASTListener</code> is alvast een veld <code class="lang-java">currentContainer</code> voor je gedefinieerd. Dit moet een stack worden die je o.a. bij iedere <code class="lang-java">enter…()</code>-method gebruikt om nieuwe AST-knopen naar te pushen. Denk bijvoorbeeld eens terug aan de binary expression trees. Hierin bestaat een rekenkundige uitdrukking uit een boom. De operanden zijn losse knopen daarin. Ieder operand matcht apart met een syntactische regel. Je hebt in de opdrachttekst gelezen dat ICSS ook rekenkundige uitdrukkingen bevat. Je moet binnen <code class="lang-java">ASTListener</code> elk stukje van zo’n uitdrukking dus stap voor stap aan een nieuwe boom toevoegen, een AST-deelboom. De knopen van de AST-deelboom bewaar je op de stack. Door operaties op de stack uit te voeren zet je de knopen om naar een steeds grotere AST-deelboom, die op de stack blijft staan tot je bij <code class="lang-java">exitStylesheet</code> aankomt.</p>
              </component-section>
              <component-section >
                <div slot="title">De checker</div>
                <p>
                  <code class="lang-java">nl.han.ica.icss.checker.Checker.variableTypes</code>
                  is bedoeld om variabelen bij te houden (naam, type), dus is een <em class="term">symbol table</em>, zoals besproken in de videoserie van Kevin Drumm over compilers. Zijn datatype <code class="lang-java">HashMap&lt;String, ExpressionType&gt;</code> staat voor:</p>
                  <ul>
                    <li>
                      <p>de symbol table: een dictionary, geïmplementeerd als hashtabel (<code class="lang-java">HashMap</code>),</p>
                    </li>
                    <li>
                      <p>met variabelenaam als <em>sleutel</em> (<code class="lang-java">String</code>),</p>
                    </li>
                    <li>
                      <p>en het datatype van de waarde (bijvoorbeeld pixels) als <em>waarde</em> (<code class="lang-java">ExpressionType</code>)</p>
                    </li>
                  </ul>
                <p>
                  <code class="lang-java">variableTypes</code> is een linked list of eigenlijk stack, omdat je de <em>scope</em> van variabelen daarin moet vastleggen en gebruiken met <code>push</code>es en <code>pop</code>s. Je zal ook een method moeten maken die controleert of een variabele waarnaar verwezen wordt in de ICSS-invoer echt bestaat binnen de te doorzoeken scopes. En zo ja, of de waarde van het juiste datatype is binnen ICSS (volgens de vereisten uit de opdrachttekst). Het is bijvoorbeeld verkeerd om een variabele te hebben waar in een globale scope eerst een kleurwaarde aan is toegewezen, om er in een lokale scope een pixelwaarde aan toe te kennen. Scopes worden in iCSS gevormd door style rules, maar ook door conditionele statements. Sla de opdrachttekst erop na om de vereisten precies vast te stellen.
                </p>
                <p>de method <code class="lang-java">check(AST ast)</code> wordt aangeroepen vanuit de compiler pipeline. Vanuit deze method moet je de hele AST aflopen en semantisch analyseren. Met andere woorden: hier moet je je eigen type checking doen.</p>
                <p>Stel, je constateert een semantische fout. Dan is het natuurlijk wel zo handig als dat op een nette en uniforme manier teruggekoppeld wordt naar de pipeline en de ICSS tool. Roep in je checker de method <code class="lang-java">setError(String description)</code> van <code class="lang-java">nl.han.ica.icss.ast.ASTNode</code> aan op de AST-knoop/knopen waar de foutmelding van toepassing is. Als je zomaar een exception zou opwerpen zou de hele compiler crashen, of in ieder geval zou het parseren abrupt afgebroken worden. En als je de error alleen zou loggen, is nadat de pipeline doorlopen is lastig te traceren op welk punt in de invoer de fout optrad. Immers, dezelfde fout en invoer kan herhaald voorkomen, en alleen het regel-/kolomnummer waar de fout zit melden is ook niet per se gebruikersvriendelijk.</p>
              </component-section>
              <component-section >
                <div slot="title">De transformer</div>
                <p>In <code class="lang-java">nl.han.ica.icss.transforms</code> bouw je de volgende stap uit de pipeline. Deze stap wordt aangeroepen vanuit <code class="lang-java">nl.han.ica.icss.Pipeline</code> in de method <code class="lang-java">transform()</code>. Het doel van <em class="term">transformeren</em> is onder andere optimalisaties toepassen, waar je aan het begin van deze les over leerde. De basis van twee classes met de transformaties vereist in de opdracht vind je al in de package. Loop wederom in je <code class="lang-java">apply()</code>-implementaties de AST af. Deze keer verbouw (muteer) je de AST. Bedenk dat ICSS een vaste structuur heeft: een stylesheet bevat style rules, enzovoort. Je kan in de praktijk aardig wat aannames doen over de vorm van de AST. Zeker nu je de AST al hebt gecheckt. 🙂</p>
              </component-section>
              <component-section >
                <div slot="title">De generator</div>
                <p>In <code class="lang-java">nl.han.ica.icss.generator</code> ontwikkel je de laatste stap uit de pipeline. Het doel van een generator is een <em class="term">intermediate representation</em>, zoals de AST, omzetten naar doelcode. In ons geval willen we de AST omzetten naar CSS. De standaardklasse <code class="lang-java">java.lang.StringBuilder</code> kan hiervoor van pas komen, samen met onder meer de al gegeven <code class="lang-java">toString()</code>-implementatie van <code class="lang-java">ASTNode</code>. </p>
              </component-section>
              <component-section >
                <div slot="title">Tips</div>
                <component-section >
                  <div slot="title">Parser</div>
                  <ul>
                    <li>
                      <p>Bouw alleen tussentijdse versies van de AST op via de stack (<code class="lang-java">nl.han.ica.icss.parser.ASTListener.currentContainer</code>). Manipuleer in je parser alleen de stack, niet meerdere member variabelen, zoals ook <code class="lang-java">nl.han.ica.icss.parser.ASTListener.ast</code>.</p>
                    </li>
                    <li>
                      <p>
                        Wijs
                        <code class="lang-java">nl.han.ica.icss.parser.ASTListener.ast</code>
                        (een <code class="lang-java">AST</code>-object) pas toe zodra je klaar bent met het verwerken van alle invoer, dus in de juiste listener method daarvoor ... Roep dan ook <code class="lang-java">setRoot()</code> van de <code>AST</code> class aan.
                      </p>
                    </li>
                    <li>
                      <p>
                        Soms moet je niet alleen iets op de stack pushen of ervanaf poppen, maar het huidige object op de top van de stack manipuleren. Dan is dat object vaak een scope-achtige AST-knoop. Voeg bijv. losse
                        <em>declarations</em> en <em>variable assignments</em> uit je input toe aan die scope-achtige AST-knoop.
                      </p>
                    </li>
                  </ul>
                </component-section>
                <component-section >
                  <div slot="title">Je parser efficiënt debuggen</div>
                  <p>Als je de ICSS tool via Maven start (zie <code>README.md</code> bij de startbroncode), worden breakpoints die je instelt in bijv. je parser-methods genegeerd. Maven start je programma namelijk niet in een debugmodus. Een alternatief is om je parser te starten vanuit IntelliJ.</p>
                  <component-section >
                    <div slot="title">De ICSS tool draaien en debuggen</div>
                  </component-section>
                  <ol>
                    <li>
                      <p>
                        Ga naar de klasse
                        <code class="lang-java">nl.han.ica.icss.gui.Main</code>
                        in IntelliJ IDEA.
                      </p>
                    </li>
                    <li>
                      <p>Hover met je cursor over de class definition.</p>
                    </li>
                    <li>
                      <p>Klik op het groene ‘play’-knopje.</p>
                    </li>
                    <li>
                      <p>
                        Kies ‘Debug
                        <code class="lang-java">Main.main()</code>’.
                      </p>
                    </li>
                  </ol>
                  <component-section >
                    <div slot="title">De tests draaien en debuggen</div>
                    <ol>
                      <li>
                        <p>Ga naar een testklasse in IntelliJ IDEA.</p>
                      </li>
                      <li>
                        <p>Hover met je cursor over de class definition.</p>
                      </li>
                      <li>
                        <p>Klik op het groene ‘play’-knopje.</p>
                      </li>
                      <li>
                        <p>Kies ‘Debug <code class="lang-java">…</code>’.</p>
                      </li>
                    </ol>
                  </component-section>
                </component-section>
              </component-section>
            </component-section>
          <component-section >
            <div slot="title">Bijeenkomst 2020-12-15</div>
            <component-section >
              <div slot="title">Welkom (20:00)</div>
              <ul>
                <li>
                  <p>Aanwezigheidsregistratie.</p>
                </li>
                <li>
                  <p>De structuur van deze les.</p>
                </li>
              </ul>
            </component-section>
            <component-section >
              <div slot="title">Zelf bestuderen en basisopgaven maken (20:05)</div>
              <p>
                <a href="https://secure.ans-delft.nl/universities/13/courses/43101/assignments/126440/quiz/start">Basisopgaven les 8: Programmeertalen</a>
              </p>
            </component-section>
          </component-section>
          <component-section >
            <div slot="title">Bijeenkomst 2020-12-17</div>
            <component-section >
              <div slot="title">Welkom (18:30)</div>
              <ul>
                <li>
                  <p>Aanwezigheidsregistratie.</p>
                </li>
                <li>
                  <p>
                    Hoe is het gegaan? Neem even de tijd om de
                    <a href="https://secure.ans-delft.nl/universities/13/courses/43101/assignments/95815/quiz/start">vragenlijst ‘Je huidige leerresultaten’</a>
                    (weer) in te vullen.
                  </p>
                </li>
                <li>
                  <p>De structuur van deze les.</p>
                </li>
              </ul>
            </component-section>
            <component-section>
              <div slot="title">Samen beginnen aan <a href="Beroepsproduct_compiler/">beroepsproduct Compiler</a> (18:45)</div>
              <component-section >
                <div slot="title">Rekenkundige uitdrukkingen parseren</div>
              </component-section>
            </component-section>
            <component-section >
              <div slot="title">Zelfstandig verder werken aan Compiler (20:30)</div>
            </component-section>
          </component-section>
      </component-section>
    </component-section>`,
    document.getElementById('lessonProgramminglanguages'),
  );
