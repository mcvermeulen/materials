import { html, render } from 'lit-html';

export const renderLessonHashing = () =>
  render(
    html`<div slot="title">Hashing</div>
      <component-section>
        <div slot="title">Wat je leert</div>
        <component-intendedlearningoutcomes>
          <component-intendedlearningoutcome codes='["AD-begrip-standaardalgoritmes-datastructuren"]'
            >legt <strong>hashing</strong> uit (<strong>hashfunctie</strong>, <strong>load factor</strong>,
            <strong>hashtabel</strong>-datastuctuur, <strong>collision</strong>, algoritmes voor operaties zoals
            <strong>chaining/closed addressing: separate chaining</strong>, <strong>open addressing</strong> zoals
            <strong>linear probing</strong> en <strong>quadratic probing</strong>).</component-intendedlearningoutcome
          >
        </component-intendedlearningoutcomes>
        <component-miroboard idBoard="o9J_kgYC0lQ" viewport="-879,-420,806,913" title="Mind map"></component-miroboard>
      </component-section>
      <component-section>
        <div slot="title">Waarom je over hashing leert</div>
        <component-section>
          <div slot="title">Toepassingen</div>
          <p class="praktijk"></p>
        </component-section>
      </component-section>
      <component-section>
        <div slot="title">De dictionary-adt</div>
        <p>
          Een <em class="table">dictionary</em> of <em>mapping</em> is een adt waarin je met iedere unieke
          <em class="term">sleutel</em> (<em class="term">(search) key</em>) telkens één vaste waarde kan ophalen. Deze
          adt kan op allerlei manieren worden geïmplementeerd, waarvan de <em class="term">hashtabel</em> de
          belangrijkste is.
        </p>
        <p>OpenDSA maakt dit concept concreet in duidelijke Java-broncode.</p>
        <p class="bron">
          <a href="https://opendsa-server.cs.vt.edu/ODSA/Books/CS3/html/Dictionary.html">6.4. The Dictionary ADT</a>
        </p>
        <component-section>
          <div slot="title">Generic programming en dictionaries</div>
          <component-exercise>
            <p>Beschrijf de minstens twee type parameters die nodig zijn voor een generic dictionary class.</p>
          </component-exercise>
        </component-section>
      </component-section>
      <component-section>
        <div slot="title">Hashing</div>
        <p>
          In de volgende video geeft Kevin Drumm een eenvoudige inleiding in hashing. Hieruit haal je het overzicht over
          het thema hashing.
        </p>
        <p class="bron">Hash Tables and Hash Functions</p>
        <component-video videoid="KyUTuwz_b7Q" title="Hash Tables and Hash Functions"></component-video>
        <p>
          Hashing is dus een manier om een index te berekenen (met een
          <em class="term">hashfunctie</em>) waarop je een stukje data kan vinden dat je zoekt, met een looptijd van
          \\((O(1)\\) in het gemiddelde geval, als je hashing goed implementeert).
        </p>
        <component-section>
          <div slot="title">Slechte hashfunctie</div>
          <component-exercise>
            <p>
              Geef een voorbeeld van een hashfunctie die
              <code>get(key)</code>/<code>search(key)</code> op een hashtabel <em>slechter</em> dan \\(O(1)\\) in alle
              gevallen zou maken.
            </p>
          </component-exercise>
        </component-section>
        <p>
          Verder legde Kevin Drumm uit wat een hashtabel-datastructuur kan doen om om te gaan met
          <em class="term">collisions</em> (‘botsingen’: een sleutel blijkt niet uniek), onder de noemer
          <em class="term">collision resolution</em>
          (botsingsoplossing). Dat kan met
          <em class="term">open addressing</em> (bijv. <em class="term">linear</em> of
          <em class="term">quadratic probing</em>) of <em class="term">closed addressing</em>/<em class="term"
            >separate chaining</em
          >.
        </p>
        <component-section>
          <div slot="title">Belangrijkste eigenschappen hashfunctie</div>
          <component-exercise>
            <p>Geef drie belangrijke eigenschappen die de kwaliteit van een hashfunctie bepalen.</p>
          </component-exercise>
        </component-section>
        <p>OpenDSA legt hashing compleet uit. Hieruit haal je diepgaande kennis.</p>
        <p class="bron">
          <a href="https://opendsa-server.cs.vt.edu/ODSA/Books/CS3/html/HashIntro.html">10.1. Introduction</a>
        </p>
        <p class="bron">
          <a href="https://opendsa-server.cs.vt.edu/ODSA/Books/CS3/html/HashFunc.html"
            >10.2. Hash Function Principles</a
          >
        </p>
        <p class="bron">
          <a href="https://opendsa-server.cs.vt.edu/ODSA/Books/CS3/html/HashFuncExamp.html"
            >10.3. Sample Hash Functions</a
          >
        </p>
        <p>
          Om het ‘makkelijker te maken’ gebruiken informatici verwarrende synoniemen voor types collision resolution:
          <em class="term">closed hashing</em> voor <em>open addressing</em> en <em class="term">open hashing</em> is
          voor <em>separate chaining</em>.
        </p>
        <p class="bron">
          <a href="https://opendsa-server.cs.vt.edu/ODSA/Books/CS3/html/OpenHash.html">10.4. Open Hashing</a>
        </p>
        <p class="bron">
          <a href="https://opendsa-server.cs.vt.edu/ODSA/Books/CS3/html/BucketHash.html">10.5. Bucket Hashing</a>
        </p>
        <p class="bron">
          <a href="https://opendsa-server.cs.vt.edu/ODSA/Books/CS3/html/HashCSimple.html">10.6. Collision Resolution</a>
        </p>
        <p class="bron">
          <a href="https://opendsa-server.cs.vt.edu/ODSA/Books/CS3/html/HashCImproved.html"
            >10.7. Improved Collision Resolution</a
          >
        </p>
        <p class="bron">
          <a href="https://opendsa-server.cs.vt.edu/ODSA/Books/CS3/html/HashAnal.html"
            >10.8. Analysis of Closed Hashing</a
          >
        </p>
        <component-section>
          <div slot="title">Load factor</div>
          <component-exercise>
            <p>
              Leg in je eigen woorden uit wat de
              <em class="term">load factor</em> is en waarom dit concept belangrijk is.
            </p>
          </component-exercise>
        </component-section>
        <p class="bron">
          <a href="https://opendsa-server.cs.vt.edu/ODSA/Books/CS3/html/HashDel.html">10.9. Deletion</a>
        </p>
      </component-section>
      <component-section>
        <div slot="title">Bijeenkomst 2020-11-03</div>
        <component-section>
          <div slot="title">Welkom (20:00)</div>
          <ul>
            <li>
              <p>Aanwezigheidsregistratie.</p>
            </li>
            <li>
              <p>De structuur van deze les.</p>
            </li>
          </ul>
        </component-section>
        <component-section>
          <div slot="title">Zelf bestuderen en basisopgaven maken (20:15)</div>
          <p>
            <a href="https://secure.ans-delft.nl/universities/13/courses/43101/assignments/94519/quiz/start"
              >Basisopgaven les 5: Hashing</a
            >
          </p>
        </component-section>
        <component-section>
          <div slot="title">Mindmap maken over hashing (in tweetallen) (21:10)</div>
          <component-exercise together>
            <component-section>
              <div slot="title">Bespreking (21:20)</div>
            </component-section>
          </component-exercise>
        </component-section>
      </component-section>
      <component-section>
        <div slot="title">Bijeenkomst 2020-11-05</div>
        <component-section>
          <div slot="title">Welkom (18:30)</div>
          <ul>
            <li>
              <p>Aanwezigheidsregistratie.</p>
            </li>
            <li>
              <p>
                Hoe is het gegaan? Neem even de tijd om de
                <a href="https://secure.ans-delft.nl/universities/13/courses/43101/assignments/95815/quiz/start"
                  >vragenlijst ‘Je huidige leerresultaten’</a
                >
                (weer) in te vullen.
              </p>
            </li>
            <li>
              <p>De structuur van deze les.</p>
            </li>
          </ul>
        </component-section>
        <component-section>
          <div slot="title">Samenopgaven</div>
          <p>
            <a href="https://secure.ans-delft.nl/universities/13/courses/43101/assignments/102388/generator/quiz/start"
              >Samenopgaven les 5: Hashing (verbeterd)</a
            >
          </p>
          <component-section
            ><div slot="title">Hashen met singly-linked list buckets (19:00)</div>
            <component-exercise together></component-exercise>
          </component-section>
          <component-section
            ><div slot="title">Hashen met quadratic probing (19:30)</div>
            <component-exercise together></component-exercise>
          </component-section>
          <component-section>
            <div slot="title">Bespreking Sorteren van speelkaarten met Quicksort (20:00)</div>
            <component-exercise together>
              <p class="instructor_only">
                Mijn uitwerking staat op
                <a href="https://gitlab.com/han-aim/dt-sd-asd/adp/quicksort"
                  >https://gitlab.com/han-aim/dt-sd-asd/adp/quicksort</a
                >.
              </p>
            </component-exercise>
          </component-section>
          <component-section>
            <div slot="title">
              Bespreking
              <a
                href="#_1_4_10_2_4_Ontwikkel_een_sorteerder_voor_de_3D_Basisvoorziening_van_Nederland__tweetal___20_00_"
                >Ontwikkel een sorteerder voor de 3D Basisvoorziening van Nederland (tweetal)</a
              >
              (20:15)
            </div>
            <component-exercise together>
              <ul>
                <li>
                  <p>Sorteercriteria?</p>
                </li>
                <li><p>Hoe flexibel?</p></li>
                <li><p>Hoe goed is de performance?</p></li>
              </ul>
              <p class="instructor_only">
                Mijn uitwerking staat op
                <a href="https://gitlab.com/han-aim/dt-sd-asd/adp/cityjson_sorter"
                  >https://gitlab.com/han-aim/dt-sd-asd/adp/cityjson_sorter</a
                >.
              </p>
            </component-exercise>
          </component-section>
          <component-section>
            <div slot="title">
              Bespreking <a href="#_1_1_7_4_5_Meten_van_groeisnelheid__20_45_">Meten van groeisnelheid</a> (21:00)
            </div>
            <component-exercise together></component-exercise>
          </component-section>
        </component-section>
      </component-section>`,
    document.getElementById('lessonHashing'),
  );
