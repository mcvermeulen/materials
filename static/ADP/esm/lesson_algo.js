import { html, render } from 'lit-html';

export const renderLessonAlgo = () =>
  render(
    html`<div slot="title">Algoritmes en complexiteit</div>
      <component-section>
        <div slot="title">Wat je leert</div>
        <component-intendedlearningoutcomes>
          <component-intendedlearningoutcome codes='["AD-begrip-standaardalgoritmes-datastructuren"]'
            >legt
            <strong>algoritme</strong>
            uit.</component-intendedlearningoutcome
          >
          <component-intendedlearningoutcome codes='["AD-begrip-standaardalgoritmes-datastructuren"]'
            >legt
            <strong>datastructuur</strong>
            uit.</component-intendedlearningoutcome
          >
          <component-intendedlearningoutcome codes='["AD-begrip-standaardalgoritmes-datastructuren"]'
            >categoriseert de
            <strong>tijdcomplexiteit</strong>
            van standaardalgoritme-implementaties in een orde
            <strong>grote-o oftewel (O(·))</strong>.</component-intendedlearningoutcome
          >
          <component-intendedlearningoutcome
            codes='["AD-procedure-tijdcomplexiteit", "AD-begrip-afweging-tijdcomplexiteit"]'
            >redeneert over tijdcomplexiteit van algoritme-implementaties.</component-intendedlearningoutcome
          >
        </component-intendedlearningoutcomes>
        <component-miroboard idBoard="o9J_kg2KS6g" viewport="-648,-262,939,732" title="Mind map"></component-miroboard>
      </component-section>
      <component-section>
        <div slot="title">Algoritmes</div>
        <p>
          Een <em class="term">algoritme</em> is een compleet ‘stappenplan’ om een
          <em class="term">computationele taak</em> (<em class="term">probleem</em>) op te lossen. Het stappenplan is in
          principe algemeen, bijv. wiskundig gedefinieerd en niet in een concrete programmeertaal. De kleinst mogelijke
          stap in een algoritme heet een <em class="term">instructie</em>.
        </p>
        <p>
          Het begrip ‘algoritme’ heeft
          <a href="https://en.wikipedia.org/wiki/Algorithm_characterizations">geen exacte definitie</a>.
          <a href="https://www.quantamagazine.org/computer-scientist-donald-knuth-cant-stop-telling-stories-20200416/"
            >Donald Knuth</a
          >
          benoemt vijf criteria voor algoritmes
          <cite data-id="6867189/L2768YLY" data-label="page" data-locator="22-23"></cite>: een algoritme …
        </p>
        <ol>
          <li>
            <p>… stopt altijd na een telbaar aantal instructies (is eindig).</p>
            <p>
              <small><em class="term">Finiteness</em></small>
            </p>
          </li>
          <li>
            <p>… bevat alleen ondubbelzinnige instructies.</p>
            <p>
              <small><em class="term">Definiteness</em></small>
            </p>
          </li>
          <li>
            <p>… heeft nul of meer invoer-waarden (data, bijv. een getal).</p>
            <p>
              <small><em class="term">Input</em></small>
            </p>
          </li>
          <li>
            <p>… heeft één of meer uitvoer-waarden, met een duidelijke betekenis en verband met de invoer-waarden.</p>
            <p>
              <small><em class="term">Output</em></small>
            </p>
          </li>
          <li>
            <p>
              … is begrijpbaar, zodat je ook werkelijk het probleem ermee kan oplossen. Het bevat dus géén weliswaar
              ondubbelzinnige, maar onhaalbare stappen, zoals:
              <q>Vind nu een oplossing voor … (een wiskundig probleem dat nog onopgelost is).</q>
              😒
            </p>
            <p>
              <small><em class="term">Effectiveness</em></small>
            </p>
          </li>
        </ol>
        <p>
          Een <em class="term">standaardalgoritme</em> is in deze leereenheid een algoritme dat fundamenteel en basaal
          is binnen de informatica.
        </p>
        <p>
          <span class="praktijk"></span> Algoritmes <em class="term">implementeer</em> je in de vorm van broncode in een
          programmeertaal, om de algoritmes echt uit te kunnen voeren. Er kunnen verschillende licht afwijkende
          implementaties bestaan van hetzelfde algoritme, zelfs terwijl die in één programmeertaal zijn.
        </p>
      </component-section>
      <component-section>
        <div slot="title">Data</div>
        <p>
          Een <em class="term">datastructuur</em> is opgeslagen in computergeheugen (→ data) en opgebouwd uit
          <em class="term">datatypes</em> (→ structuur). Doordat een datastructuur is opgeslagen in het geheugen, hangt
          de concrete vorm van datastructuren af van de hardware en de programmeertaal-implementatie.
        </p>
        <p>Een <em class="term">datatype</em> is bijv. een class met data-velden, in Java.</p>
        <!-- TODO: Illustratie toevoegen over datastructuur, datatype. -->
        <p>
          Een <em class="term">primitief datatype</em> is simpeler dan zo’n class. De betekenis van primitief datatype
          hangt af van de programmeertaal, maar ‘getal’ (bijv. <code class="lang-java">int</code>) of ‘karakter’ (<code
            class="lang-java"
            >char</code
          >) zijn bijna altijd wel beschikbaar. Simpelere data types dan primitieve bestaan nooit.
        </p>
        <p class="bron">
          The Java® Language Specification - Java SE 14 Edition
          <cite data-id="6867189/LGMREFR6" data-label="section" data-locator="4.2"></cite>
          (<a href="https://docs.oracle.com/javase/specs/jls/se14/html/jls-4.html#jls-4.2">Primitive Types and Values</a
          >)
        </p>
        <p>
          Een standaarddatastructuur is in deze leereenheid een datastructuur die fundamenteel en basaal is binnen de
          informatica.
        </p>
      </component-section>
      <component-section>
        <div slot="title">Waarom je datastructuren en algoritmes leert</div>
        <p>
          Vrijwel alle software gebruikt standaarddatastructuren en algoritmes. Als je bijv. een route wilt plannen, je
          contactenlijst wilt bewaren, of je browser gebruiken, ben je al afhankelijk van datastructuren en algoritmes.
        </p>
        <p>
          Algoritmes en datastructuren gebruik je vanzelf, soms misschien indirect als je werkt met bepaalde libraries.
          De grootte van de data waar je mee werkt heeft vaak grote invloed op de performance efficiency van software
          <span class="praktijk"></span>.
        </p>
      </component-section>
      <component-section>
        <div slot="title">Algoritme-analyse</div>
        <p>
          Om software zo snel mogelijk te maken, wil je de efficiëntie van algoritmes voor dezelfde computationele taak
          kunnen vergelijken. Maar … looptijdmetingen zijn erg afhankelijk van toevallige en veranderbare technologische
          factoren, zoals het toevallige benuttingspercentage van de hardware tijdens de meting, de hardware zelf en de
          programmeertaal-implementatie. 🤯
        </p>
        <p>
          Analyse van
          <em class="term">computationele complexiteit</em> is een manier om de kern door te dringen van een algoritme,
          om zo zijn efficiëntie exact te kunnen aangeven. Deze analyse wordt ook
          <em class="term">asymptotische analyse</em>, <em class="term">algoritme-analyse</em> of
          <em class="term">theory of algorithms</em> genoemd. Algoritme-analyse richt zich op de voorspelbare
          <em class="term">groei</em> van een algoritme zijn <em class="term">hulpbron/resource</em>-gebruik, die
          helemaal afhangt van de <em class="term">invoer</em> (1. de <em>grootte</em> en 2. de <em>inhoud</em> ervan).
        </p>
        <p>
          De grootte van de invoer druk je voor algoritme-analyse uit met een positief natuurlijk getal \\(n\\). Wat je
          verstaat onder ‘grootte’ hangt af van de manier waarop je algoritme werkt en wat voor type invoerdata het
          gebruikt. Gewoonlijk staat het voor ‘het aantal elementen waaruit de invoerdata bestaat’, bijv. elementen van
          een lijst.
        </p>
        <component-section>
          <div slot="title">Waarom je algoritme-analyse leert</div>
          <p>
            We willen graag steeds grotere hoeveelheden data verwerken, want software is juist nuttig voor algoritmes
            die we als mensen zelf praktisch niet kunnen uitvoeren. Als je een software-systeem meeontwikkelt dat véél
            data moet verwerken, is de
            <em class="term">schaalbaarheid</em> van het ontwerp van het systeem erg belangrijk
            <span class="praktijk"></span>. Algoritme-analyse is bedoeld als hulpmiddel om juist schaalbaarheid van
            algoritmes en datastructuren te bepalen. Door bedreven te zijn in algoritme-analyse, speel je sneller een
            belangrijke rol binnen ontwikkelteams die zulke systemen ontwikkelen.
          </p>
          <p>
            Zelfs al doe je liever rechtstreekse metingen zonder verdere algoritme-analyse, dan nog moet je de
            complexiteit van de algoritme-implementatie begrijpen, wil je de implementatie kunnen verbeteren n.a.v. je
            meting. M.a.w., met algoritme-analyse kan je je metingen pas verklaren 💡.
          </p>
          <p>
            Soms gééf je niet om het verder optimaliseren van een software-systeem dat een computationele taak oplost.
            De algoritme-implementaties daarin hebben bijv. naar jouw zin (als het goed is ook die van de gebruikers en
            klanten) al een adequate looptijd. Toch beïnvloedt de efficiëntie van de algoritme-implementaties het
            stroomverbruik van de hardware. Bij populaire software-systemen, zoals WordPress, is het stroomverbruik
            zelfs aanzienlijk op wereldschaal. Wil je het milieu sparen, en de batterij van je gebruikers, dan heeft het
            alsnog zin om efficiëntie te analyseren 🌳.
          </p>
        </component-section>
        <component-section>
          <div slot="title">De hulpbronnen: looptijd en geheugenruimte</div>
          <p>
            Een hulpbron kan in theorie van alles zijn, maar in de praktijk is het vaak
            <em class="term">tijd</em> (looptijd) en <em class="term">ruimte</em> (geheugen). Net zoals een algoritme
            een <em class="term">tijdcomplexiteit</em> heeft, heeft een algoritme geheugenruimte nodig om data te
            bewaren (<em class="term">ruimtecomplexiteit</em>). In deze leereenheid sta je niet lang stil bij
            ruimtecomplexiteit. Belangrijk is om te beseffen dat veel algoritmes ontworpen zijn met een afweging tussen
            gebruik van tijd en ruimte.
          </p>
        </component-section>
        <p>
          Bij iedere algoritme-analyse moet je nadenken over minstens drie onderdelen 🧩: geval, groeiorde en
          begrenzing. In het volgende leer je over deze onderdelen, toegepast op tijdcomplexiteit.
        </p>
        <component-section>
          <div slot="title">🧩 A: Soort geval categoriseren m.b.t. de invoerdata</div>
          <p>
            Eerst kies je van welk <em class="term">geval</em> je uitgaat. Met geval bedoel je dan <em>niet</em> de
            grootte van de invoerdata, maar juist de <em>inhoud</em> van de invoerdata. Want sommige algoritmes gaan
            zich heel anders gedragen als ze bepaalde data tegenkomen (dat zie je onder andere terug bij de
            sorteeralgoritmes straks). Als de looptijd het allerhoogst is bij een bepaalde inhoud van de data, noemen we
            dat het <em class="term">slechtste geval</em>. Vice versa noemen het we <em class="term">beste geval</em>.
            Tot slot neem je idealiter het <em class="term">gemiddelde geval</em>, maar dat zou je met ingewikkelde
            kansrekening en statistiek moeten bewijzen.
          </p>
          <p>
            Het <em>slechtste</em> geval is voor een software-ontwikkelaar op hbo-niveau het belangrijkst om te leren.
            Door het slechtste geval te onderzoeken, weet je namelijk alvast zeker dat de bovengrens aan de
            looptijdgroei nooit slechter kan uitvallen op onvoorziene invoerdata. Als je bijv. data van buitenaf
            verwerkt weet je nooit zeker wat de inhoud daarvan is. Onder andere vanwege beveiliging wil je dan weten of
            je software-systeem niet zal vastlopen vanwege een ongelukkig gekozen algoritme
            <span class="praktijk"></span>.
          </p>
          <component-section>
            <div slot="title">De zin van beste-geval-algoritme-analyse</div>
            <component-exercise>
              <p>Noem één of meer voorbeelden van nuttige toepassingen van algoritme-analyse op het beste geval.</p>
            </component-exercise>
          </component-section>
        </component-section>
        <component-section>
          <div slot="title">🧩 B: Groeisnelheid vereenvoudigen tot een groeiorde</div>
          <p>
            Stel, je weet de functie \\(T(n)\\) die de exacte looptijd geeft van het algoritme. Aangenomen dat dit een
            stijgende functie is bij stijgende \\(n\\), ken je zo ook de groeisnelheid van de looptijd. Soms groeit de
            looptijd niet, tenminste, niet afhankelijk van \\(n\\). Dan noem je de looptijd van orde constant. Linksom
            of rechtsom vereenvoudig je \\(T(n)\\) altijd tot een functie die alleen de
            <em class="term">groeiorde</em> van de looptijd weergeeft. Door die vereenvoudiging hoeft voor
            algoritme-analyse iedereen uiteindelijk maar een handvol groeisnelheden te kennen. Weet je de groeiorde, dan
            weet je dus net zo goed of/hoe snel de looptijd zal groeien als wanneer je \\(T(n)\\) weet. Dus zelfs als je
            toch \\(T(n)\\) al weet, moet je die formule zo veel mogelijk vereenvoudigen tot een groeiorde.
          </p>
          <div id="columns_groeiordes" class="columns">
            <table>
              <caption>
                Voorbeelden van groeiordes, op volgorde van niet-stijgend (constant), langzaam naar snel stijgend, naar
                mate \\(n\\) hoger is.
              </caption>
              <thead>
                <tr>
                  <th id="element">Groeiorde</th>
                  <th id="betekenis">Naam</th>
                </tr>
              </thead>
              <tbody>
                <tr>
                  <td>\\(1\\)</td>
                  <td>Constant</td>
                </tr>
                <tr>
                  <td>\\(\\log (n)\\)</td>
                  <td>Logaritmisch</td>
                </tr>
                <tr>
                  <td>\\(n\\)</td>
                  <td>Lineair</td>
                </tr>
                <tr>
                  <td>\\(n \\log (n)\\)</td>
                  <td>Linearitmisch</td>
                </tr>
                <tr>
                  <td>\\(n^2\\)</td>
                  <td>Kwadratisch</td>
                </tr>
                <tr>
                  <td>\\(n^3\\)</td>
                  <td>Kubisch</td>
                </tr>
                <tr>
                  <td>\\(n^{c}\\)</td>
                  <td>Polynomiaal</td>
                </tr>
                <tr>
                  <td>\\(c^{n}\\)</td>
                  <td>Exponentieel</td>
                </tr>
                <tr>
                  <td>\\(n!\\)</td>
                  <td>Factoriaal</td>
                </tr>
              </tbody>
            </table>
          </div>
          <p>
            Maar hoe leid je \\(T(n)\\) dan af, of hoe kan ik misschien op een andere manier de orde van groei bepalen
            van een algoritme? Daarover later meer.
          </p>
          <component-section>
            <div slot="title">De lijnen van de groeiordes</div>
            <component-exercise>
              <p>
                Maak van de voorgaande groeiordes één lijngrafiek via
                <a href="https://www.geogebra.org/graphing">GeoGebra</a>. Leg uit wat de betekenis van de assen is in
                jouw grafiek.
              </p>
            </component-exercise>
          </component-section>
        </component-section>
        <component-section>
          <div slot="title">🧩 C: Begrenzing aan groei aangeven</div>
          <p>
            Tot slot moet je nog aangeven hoe de groeisnelheid van het algoritme precies begrensd is in vergelijking met
            de gekozen groeiorde.
          </p>
          <p></p>
          <div id="columns_begrenzingen" class="columns">
            <table id="table_begrenzingen">
              <caption>
                Begrenzingen. Op de ⋅ schrijf je de groeiorde.
              </caption>
              <thead>
                <tr>
                  <th scope="col">Begrenzing</th>
                  <th scope="col">Naam</th>
                  <th scope="col">Informele omschrijving</th>
                </tr>
              </thead>
              <tbody>
                <tr>
                  <td class="textalign_center">\\(o(⋅)\\)</td>
                  <td>Kleine o</td>
                  <td>
                    Groeit langzamer dan
                    <span class="nowrap">\\((&lt;)\\)</span>
                  </td>
                </tr>
                <tr>
                  <td class="textalign_center">\\(O(⋅)\\)</td>
                  <td>Grote o</td>
                  <td>
                    Groeit niet sneller dan
                    <span class="nowrap">\\((≤)\\)</span>
                  </td>
                </tr>
                <tr>
                  <td class="textalign_center">\\(Θ(⋅)\\)</td>
                  <td>Grote theta</td>
                  <td>
                    Groeit zo snel als
                    <span class="nowrap">\\((=)\\)</span>
                  </td>
                </tr>
                <tr>
                  <td class="textalign_center">\\(Ω(⋅)\\)</td>
                  <td>Grote omega</td>
                  <td>
                    Groeit niet langzamer dan
                    <span class="nowrap">\\((≥)\\)</span>
                  </td>
                </tr>

                <tr>
                  <td class="textalign_center">\\(ω(⋅)\\)</td>
                  <td>Kleine omega</td>
                  <td>
                    Groeit sneller dan
                    <span class="nowrap">\\((>)\\)</span>
                  </td>
                </tr>
              </tbody>
            </table>
          </div>
          <!-- TODO: Grafiek toevoegen. -->
          <p>
            \\(O(⋅)\\) wordt meestal voor het slechtste geval gebruikt. Deze begrenzing is makkelijker te bepalen dan
            \\(Θ(⋅)\\) en \\(o(⋅)\\). Ook is het zinniger om een bovengrens te weten dan een ondergrens (‘In het
            slechtste geval, groeit de looptijd
            <em>minstens</em> …’ 😑).
          </p>
          <component-section>
            <div slot="title">Meerdere begrenzingen</div>
            <component-exercise>
              <p>
                Stel een algoritme is \\(O(n)\\) in een zeker geval, is het theoretisch dan ook \\(O(n^2)\\) in dat
                geval?
              </p>
            </component-exercise>
          </component-section>
        </component-section>
        <component-section>
          <div slot="title">Het resultaat van algoritme-analyse rapporteren</div>
          <p>Nu heb je alle stukjes samen om het resultaat van je algoritme-analyse te rapporteren:</p>
          <figure>
            <figcaption>
              Een voorbeeldbeschrijving van de uitkomst van een algoritme-analyse. Omkaderd staan de stukjes die
              afhangen van je algoritme-analyse.
            </figcaption>
            <p>
              Algoritme-implementatie \\(\\bbox[3px, border: 2px solid currentColor]{a}\\) heeft
              <span class="bbox">in het slechtste geval</span> een <span class="bbox">looptijd</span> van \\(\\bbox[3px,
              border: 2px solid currentColor]{O}(\\bbox[3px, border: 2px solid currentColor]{n})\\)
              <small>(zeg: ‘orde grote-o van n’, of ‘grote-o lineair’)</small>.
            </p>
          </figure>
          <p>
            Met deze bewering heb je het algoritme in een
            <em class="term">complexiteitscategorie</em> geplaatst. De groeiorde en de begrenzing samen, dus de notatie
            \\(O(n)\\), staat voor de <em>verzameling</em> van allerlei mogelijke functies \\(T(n)\\).
          </p>
          <figure>
            <figcaption>
              Formelere definitie van de bewering \\(T(n) = O(⋅)\\), met een willekeurige groeiorde \\(f(n)\\) op de
              \\(⋅\\). Omkaderd staat een relatie \\(≤\\), die vind je terug in de informele omschrijving uit de
              <a href="#table_begrenzingen">Begrenzingentabel</a>.
            </figcaption>
            <p>
              \\(T(n) = O(f(n))\\) is gedefinieerd als: ‘Er is ten minste één constante \\(c > 0\\), en een andere
              constante \\(n_0 > 0\\), waarvoor \\(0 ≤ T(n_0) \\bbox[3px, border: 2px solid currentColor]{≤} c f(n_0)\\)
              geldt, zolang \\(n ≥ n_0\\).’.
            </p>
          </figure>
          <p>
            In de formele definitie betekent <q>\\(f(n)\\)</q> binnen <q>\\(O(f(n))\\)</q> <em>niet</em> de waarde van
            \\(f\\) met het argument \\(n\\), maar moet je <q>\\(f(n)\\)</q> opvatten als een <em>aanduiding</em> van de
            functie waarvan je de complexiteitscategorie uitdrukt. Je had er ook alleen \\(n\\) kunnen zetten, maar dat
            is minder abstract en directer. Dan zou \\(f(n)=n^2≠n\\) al niet meer passen in de definitie, bijvoorbeeld.
            Verder, de \\(=\\) betekent niet zoals normaal ‘is gelijk aan’, maar ‘is orde’.
          </p>
          <p>
            Je kan hetzelfde algoritme ook in meerdere complexiteitscategorieën plaatsen natuurlijk, bijv. een
            verschillende boven- en ondergrens of verschillende gevallen.
          </p>
          <p>
            In ADP word je alleen getoetst op je kennis van en vaardigheid met \\(O(⋅)\\), maar in de bronnen zie je de
            andere begrenzingen ook regelmatig terug. Verder focus je in ADP op het analyseren van algoritmes met
            broncode en metingen i.p.v. wiskundige analyse. In het volgende leer je hoe je broncode analyseert.
          </p>
        </component-section>
        <component-section>
          <div slot="title">De groeiorde bepalen</div>
          <p>
            Uit het voorgaande leerde je de belangrijkste theoretische basis van algoritme-analyse. Wil je nu een
            concreet algoritme analyseren, dan moet je een groeiorde kunnen bepalen.
          </p>
          <component-section>
            <div slot="title">Machinemodel</div>
            <p>
              Allereerst moet je weten hoe lang een computer doet over losse instructies. Een
              <em class="term">machinemodel</em> (<em class="term">model of computation</em>) is een vereenvoudigende
              aanname die je doet over de hardware, om een complexiteitsanalyse zo handig mogelijk uit te kunnen voeren.
              Een machinemodel is een soort prijslijst: welke soorten instructies bestaan er, en hoeveel tijd kost elke?
              In de praktijk hebben CPU’s veel grotere en ingewikkeldere instructiesets, maar deze zijn voor ons de
              belangrijkste:
            </p>
            <ol>
              <li>lezen van een waarde</li>
              <li>schrijven van een waarde</li>
              <li>berekenen met één operator \\(+,-,⨉,/\\)</li>
              <li>vergelijken van twee waardes</li>
            </ol>
            <p>
              Binnen algoritme-analyse wordt het
              <em class="term">Random Access Machine</em>-machinemodel aangenomen. Je hoeft in de praktijk echt niet te
              gaan tellen welke instructies er precies gebruikt worden tegen welke prijs. Het belangrijkste is de
              aanname dat ze allemaal grofweg dezelfde looptijd kosten en dus weggestreept mogen worden als de
              hoeveelheid instructies verder onafhankelijk is van \\(n\\).
            </p>
            <p>
              In het volgende leer je enkele regels waarmee je een \\(T(n)\\) moet vereenvoudigen tot een groeiorde.
            </p>
          </component-section>
          <component-section>
            <div slot="title">Verwaarloos constanten</div>
            <p>
              Verder, naarmate \\(n\\) groter is, worden veel details van \\(T(n)\\) steeds minder belangrijk. Er zijn
              simpele regels die je moet toepassen om de groei te vereenvoudigen tot een groeiorde.
            </p>
            <p>
              Zelfs als je heel veel instructies hebt in je algoritme-implementatie, moet je die
              <em>negeren voor zover ze niet afhangen van \\(n\\)</em>. Stel, de hardware moet bij bepaalde
              algoritme-implementatie áltijd maar liefst \\(182 323\\) keer een som maken, en daarna \\(n\\) keer één
              meer andere instructies. Dan mag je de grote constante \\(182 323\\) niet meetellen, want die valt in het
              niet bij \\(n\\), doordat je in algoritme-analyse ervan uitgaat dat \\(n\\) onbeperkt groot mag zijn.
            </p>
            <div id="columns_verwaarloosconstanten" class="columns">
              <table>
                <caption>
                  Voorbeelden van hoe constanten verwaarloosd moeten worden.
                </caption>
                <thead>
                  <tr>
                    <th id="constanten_T">\\(\\boldsymbol{T(n)}\\)</th>
                    <th id="constanten_groeiorde">Groeiorde</th>
                  </tr>
                </thead>
                <tbody>
                  <tr>
                    <td>\\(182 323 + n\\)</td>
                    <td>\\(n\\)</td>
                  </tr>
                  <tr>
                    <td>\\(50n\\)</td>
                    <td>\\(n\\)</td>
                  </tr>
                  <tr>
                    <td>\\(n^2+50^5\\)</td>
                    <td>\\(n^2\\)</td>
                  </tr>
                  <tr>
                    <td>\\(300n^2\\)</td>
                    <td>\\(n^2\\)</td>
                  </tr>
                  <tr>
                    <td>\\(n^3×100\\)</td>
                    <td>\\(n^3\\)</td>
                  </tr>
                  <tr>
                    <td>\\(\\frac{n^3}{100}\\)</td>
                    <td>\\(n^3\\)</td>
                  </tr>
                  <tr>
                    <td>\\(\\frac{100}{n^3}\\)</td>
                    <td>
                      \\(\\frac{1}{n^3}\\), om dalende groeiorde te behouden. (Dit is een zuiver theoretisch voorbeeld.)
                    </td>
                  </tr>
                  <tr>
                    <td>\\(42n^3+10000\\)</td>
                    <td>\\(n^3\\)</td>
                  </tr>
                  <tr>
                    <td>\\(\\log^{10}(n)\\)</td>
                    <td>\\(\\log(n)\\)</td>
                  </tr>
                  <tr>
                    <td>\\(\\log^2(n)\\)</td>
                    <td>\\(\\log (n)\\)</td>
                  </tr>
                  <tr>
                    <td>\\(2^n\\)</td>
                    <td>
                      \\(2^n\\), constanten in het grondtal van een macht met een variabele in de exponent mag je niet
                      verwaarlozen.
                    </td>
                  </tr>
                  <tr>
                    <td>\\(n^{2+1}\\)</td>
                    <td>
                      \\(n^3\\), constanten in de exponent van een macht met een variabele in het grondtal mag je niet
                      verwaarlozen.
                    </td>
                  </tr>
                  <tr>
                    <td>\\(n \\log (n^2)\\)</td>
                    <td>
                      <p>\\(\\log^a\\left(x^b\\right) = b⋅\\log^a\\left(x \\right)\\), als \\(x ≥ 0\\)</p>
                      <p>\\(2n \\log (n)\\) wordt \\(n \\log (n)\\). Dit is dus een uitzondering op de machtenregel.</p>
                    </td>
                  </tr>
                </tbody>
              </table>
            </div>
            <p>Zoals je in de voorgaande tabel ziet, zijn er wel beperkingen aan het verwaarlozen van constanten.</p>
          </component-section>
          <component-section>
            <div slot="title">Grote constanten en algoritme-analyse</div>
            <component-exercise>
              <p>
                Stel, je analyseert een algoritme dat erg grote constantes (bijvoorbeeld \\(c &gt; 10000)\\) heeft in
                zijn \\(T(n)\\). Wat betekent dat voor het nut van je algoritme-analyse?
              </p>
            </component-exercise>
          </component-section>
          <component-section>
            <div slot="title">Verwaarloos kleinere termen</div>
            <p>
              Oftewel, vereenvoudig tot de dominante
              <em class="term">term</em>. Een term is een deel van de formule gescheiden door optellen of aftrekken. Als
              er meerdere gelijke termen zijn, blijft er één over.
            </p>
            <div id="columns_verwaarlooskleineretermen" class="columns">
              <table>
                <caption>
                  Voorbeelden van hoe kleinere termen verwaarloosd en ingewikkelde formules vereenvoudigd moeten worden.
                </caption>
                <thead>
                  <tr>
                    <th id="kleinetermen_T">\\(\\boldsymbol{T(n)}\\)</th>
                    <th id="kleinetermen_groeiorde">Groeiorde</th>
                  </tr>
                </thead>
                <tbody>
                  <tr>
                    <td>\\(50n^3+2n^2+25\\)</td>
                    <td>\\(n^3\\)</td>
                  </tr>
                  <tr>
                    <td>\\(10n × 12n\\)</td>
                    <td>\\(n^2\\)</td>
                  </tr>
                  <tr>
                    <td>\\(\\frac{50n^3}{2n^2}\\)</td>
                    <td>\\(n\\), eerst de breuk vereenvoudigen.</td>
                  </tr>
                  <tr>
                    <td>\\(0.5 n \\log(n) - 2n + 7\\)</td>
                    <td>
                      <p>\\(n \\log(n)\\)</p>
                      <p><a href="https://www.geogebra.org/calculator/qxmvq3sx">De linkerterm groeit sneller</a>.</p>
                    </td>
                  </tr>
                </tbody>
              </table>
            </div>
          </component-section>
          <component-section>
            <div slot="title">Meerdere gelijke termen</div>
            <component-exercise>
              <p>
                Waarom blijft er sowieso één term over bij meerdere gelijke termen, bijv. waarom geldt \\(n + n + n =
                O(n)\\)?
              </p>
            </component-exercise>
          </component-section>
        </component-section>
        <component-section>
          <div slot="title">De groeiorde bepalen met broncode</div>
          <p>
            Nu je de basiskennis hebt van algoritme-analyse, leer je hoe je algoritme-analyse <em>doet</em> op een
            algoritme-implementatie (in broncode dus). Je ziet telkens een broncodefragment dat uit een algoritme zou
            kunnen komen, en vervolgens uitleg hoe je het fragment kan analyseren.
          </p>
          <figure>
            <figcaption>Een lus, afhankelijk van \\(m\\).</figcaption>
            <pre><code class="language-java">for (int i = 0; i &lt; m; i++) {
    iets += i;
}</code></pre>
          </figure>
          <p>
            De lus wordt een aantal keer doorlopen, afhankelijk van de invoergrootte, dus heeft een looptijd van
            \\(Θ(m)\\) (m.a.w. is van orde lineair), in alle gevallen.
          </p>
          <p>
            Gangbaar is om er gewoon \\(n\\) (dus hier \\(Θ(n)\\)) van te maken, want de enkele parameternaam
            <code>m</code> betekent bij dit algoritme verder niets interessants. In de broncode hoeft in ieder geval,
            net als in de groeiorde géén letterlijke <code>n</code> als (functie)parameter of variabele te bestaan met
            als waarde de invoergrootte, om algoritme-analyse te kunnen doen.
          </p>
          <figure>
            <figcaption>Een lus, afhankelijk van \\(m\\).</figcaption>
            <pre><code class="lang-java">for (int i = 0; i &lt; m; i++) {
    for (int j = 0; j &lt; m; j++) {
        iets += i;
    }
}</code></pre>
          </figure>
          <p>
            Een nesting van lussen afhankelijk van \\(n\\) is vaak een aanwijzing voor \\(T(n) = n^d …\\), waarbij
            \\(d\\) de diepte van de nesting is. In dit voorbeeld is \\(T(n) = m × m …\\). De voorgaande broncode heeft
            dan ook een looptijd van \\(Θ(n^2)\\), in alle gevallen.
          </p>
          <p>
            Maar blijf wel opletten, want het is mogelijk geneste lussen te hebben afhankelijk van \\(n\\), maar
            eigenlijk nog een lineair aantal iteraties.
          </p>
          <figure>
            <figcaption>Een lus, afhankelijk van \\(m\\).</figcaption>
            <pre><code class="lang-java">for (int i = 0; i &lt; m; i++) {
    for (; i &lt; m; i++) {
        iets += i;
    }
}</code></pre>
          </figure>
          <p>De voorgaande broncode heeft dan weer een looptijd van \\(Θ(n)\\), in alle gevallen.</p>
          <figure>
            <figcaption>Eén lus, maar kwadratisch aantal iteraties.</figcaption>
            <pre data-line="1"><code class="lang-java">for (int i = 0; i &lt; n * n; i++) {
    System.out.println(i);
}</code></pre>
          </figure>
          <p>
            En in de voorgaande broncode is er maar één lus. Toch is de looptijd niet van orde lineair. Deze broncode
            heeft een looptijd van \\(Θ(n^2)\\), in alle gevallen.
          </p>
          <figure>
            <figcaption>Lus met <code class="lang-java">if</code>.</figcaption>
            <pre class="lang-java"><code class="lang-java">public static boolean find(String[] array, String item) {
    boolean found = false;
    for (int i = 0; i &lt; array.length; i++){
        if (array[i].equals(item)){
            found = true;
        }
    }
    return found;
}</code></pre>
          </figure>
          <p>
            De voorgaande lus wordt ondanks de
            <code class="lang-java">if</code> nog steeds een aantal keer doorlopen afhankelijk van de invoergrootte. De
            instructies door de <code class="lang-java">if</code> hebben een looptijd onafhankelijk van \\(n\\).
          </p>
          <p>De voorgaande broncode heeft dus een looptijd van \\(Θ(n)\\), in alle gevallen.</p>
          <p>
            De invoergrootte mag in de broncode zelfs ongebonden aan een variabelenaam zijn, met als alternatief bijv.
            via de vaak beschikbare
            <code>length</code>-eigenschap.
          </p>
          <figure>
            <figcaption>Een lus die vroeg kan stoppen.</figcaption>
            <pre
              id="code_earlyreturn"
              data-line="4"
              class="language-java"
            ><code>public static boolean find(String[] array, String item) {
    for (int i = 0; i &lt; array.length; i++) {
        if (array[i].equals(item)) {
            return true;
        }
    }
    return false;
}</code></pre>
          </figure>
          <p>
            De lus wordt een aantal keer doorlopen, afhankelijk van de invoergrootte. Dat aantal is in het slechtste
            geval \\(n\\). De looptijd van de lus is dus \\(Θ(n)\\) in het slechtste geval. Tevens is de lus \\(Θ(1)\\)
            in het beste geval, nl. als het eerste element gelijk is aan
            <code class="lang-java">item</code>. Dat komt door de
            <a href="#code_earlyreturn.4">de vroege <code class="lang-java">return</code></a
            >. En je mag, simpelweg, ook beweren dat de looptijd van de lus \\(O(n)\\) is in alle gevallen, omdat
            \\(O(n)\\) alleen een lineaire <em>bovengrens</em> aangeeft.
          </p>
          <figure>
            <figcaption>Een constant kleinere lus.</figcaption>
            <pre><code class="lang-java">for (int i = 0; i &lt; 0.5 * n; i++) {
    iets += 3;
}</code></pre>
          </figure>
          <p>
            De lus wordt de helft van \\(n\\) keer doorlopen afhankelijk van de invoergrootte. Dat is nog steeds ‘een
            aantal keer onafhankelijk van \\(n\\)’, want de coëfficiënt \\(0.5\\) is maar een constante. Dus deze lus
            heeft een looptijd van \\(Θ(n)\\), in alle gevallen.
          </p>
          <p>
            Je herkent algoritmes met een logaritmische tijdcomplexiteit aan dat de data voor het probleem steeds verder
            gehalveerd wordt. Dat heet het
            <em class="term">repeating halving/doubling principle</em>: als in constante tijd telkens, afhankelijk van
            \\(n\\), het aantal te verwerken elementen wordt gehalveerd, dan heeft dat deel van het algoritme een
            groeiorde \\(log ⁡n\\).
          </p>
          <figure>
            <figcaption>Het repeating halving principe.</figcaption>
            <pre id="code_repeatinghalving" data-line="1"><code class="lang-java">for (int i = 1; i &lt;= n; i *= 2) {
    for (int j = 0; j &lt; n; j++) {
        countIterations++;
    }
}</code></pre>
          </figure>
          <p>
            Doordat de index <code class="lang-java">i</code> in het voorgaande broncodefragment
            <a href="#code_repeatinghalving.1">steeds sneller stijgt afhankelijk van <code class="lang-java">n</code></a
            >, waardoor het aantal benodigde iteraties steeds sneller afneemt (halveert), heeft de buitenste lus een
            looptijd van \\(Θ(\\log ⁡n)\\), in alle gevallen. De geneste lus heeft een looptijd van \\(Θ(n)\\), in alle
            gevallen. Dus het hele broncodefragment heeft een looptijd van \\(Θ(n \\log ⁡n)\\), in alle gevallen.
          </p>
          <component-section>
            <div slot="title">Opeenvolgende stukken hebben de maximale groeiorde</div>
            <p>
              Als je opeenvolgende op zichzelf staande stukken binnen dezelfde scope van de broncode van een
              algoritme-implementatie apart zou analyseren, kan je op twee complexiteitscategorieën uitkomen. Het
              algoritme als geheel heeft namelijk de maximale groeiorde van alle losse stukken. Als ze beide dezelfde
              groeiorde hebben, dan vereenvoudig je nog steeds tot één groeiorde.
            </p>
            <p>
              Wiskundig gezien zou je de complexiteitscategorieën van de delen moeten optellen om de
              complexiteitscategorie van het hele algoritme te weten. En dan geldt o.a. weer de regel van de dominante
              term.
            </p>
            <p>Gegeven algoritmes \\(a(n)\\), \\(b(n)\\), geldt \\(T_a(n) + T_b(n) = O(\\max(a(n), b(n)))\\)</p>
            <p>Dus stel \\(a = O(f(n))\\) en \\(b = O(f(n))\\), dan \\(O(a) + O(b) = O(a+b) = O(f(n))\\).</p>
            <p>En, bijv. \\(O(n^2) + O(n) = O(n^2)\\).</p>
          </component-section>
        </component-section>
        <component-section>
          <div slot="title">De groeiorde bepalen: een visuele, toegepaste uitleg</div>
          <p>
            Je hebt nu veel informatie verwerkt. ’t Beste leer je algoritme-analyse met \\(O(⋅)\\) door vaak naar
            voorbeelden te kijken. In de volgende video’s legt Kevin Drumm algoritme-analyse met \\(O(⋅)\\) uit
            ondersteund met visualisaties. ⚠️ Drumm gebruikt daarbij enkele voorbeelden van algoritmes en datastructuren
            die je later pas grondig leert. Vat de voorbeelden voor nu op als illustraties.
          </p>
          <p class="bron">Big O Part 1 - Linear Complexity</p>
          <component-video videoid="OMInLBKYAWg" title="Big O Part 1 - Linear Complexity"></component-video>
          <p class="bron">Big O Part 2 - Constant Complexity</p>
          <component-video videoid="1Md4Uo8LtHI" title="Big O Part 2 - Constant Complexity"></component-video>
          <p class="bron">Big O Part 3 - Quadratic Complexity</p>
          <component-video videoid="mIjuDg8ky4U" title="Big O Part 3 - Quadratic Complexity"></component-video>
          <p class="bron">Big O Part 4 - Logarithmic Complexity</p>
          <component-video videoid="Hatl0qrT0bI" title="Big O Part 4 - Logarithmic Complexity"></component-video>
          <p class="bron">Big O Part 5 – Linearithmic Complexity</p>
          <component-video videoid="i7CmolBf3HM" title="Big O Part 5 – Linearithmic Complexity"></component-video>
          <p class="bron">Big O Part 6 – Summary of Time Complexities</p>
          <component-video videoid="XiGedDZGOM8" title="Big O Part 6 – Summary of Time Complexities"></component-video>
          <p class="bron">Big O Part 7 – Space Complexity versus Time Complexity</p>
          <component-video
            videoid="bNjMFtHLioA"
            title="Big O Part 7 – Space Complexity versus Time Complexity"
          ></component-video>
          <p>
            Algoritme-analyse is niet per se zinvol voor ieder algoritme. Als het algoritme niet gebruikt wordt voor
            grote hoeveelheden data, is algoritme-analyse niet zinvol.
          </p>
          <p class="bron">
            <a href="https://lemire.me/blog/2013/07/11/big-o-notation-and-real-world-performance/"
              >Big-O notation and real-world performance</a
            >
          </p>
        </component-section>
        <component-section extra>
          <div slot="title">Betere manieren om algoritmes te analyseren</div>
          <p>
            Een moeilijkere maar krachtigere methode dan algoritme-analyse is het wiskundig bewijzen hoeveel van welke
            soorten instructies er precies uitgevoerd worden, gegeven strikt realistische invoerdata (bijvoorbeeld het
            gemiddelde geval) en een machinemodel. Zo kan je een veel nauwkeurigere \\(T(n)\\) van een algoritme
            berekenen. Met zo’n model kan je algoritmes ook, zonder meting, vergelijken tussen hardware. Je leert dit
            niet in deze leereenheid, maar je kan hierover meer lezen in aanvullende bronnen
            <cite data-id="6867189/EW26IVKM"></cite>.
          </p>
        </component-section>
      </component-section>
      <component-section>
        <div slot="title">Bijeenkomst 2020-09-01</div>
        <component-section>
          <div slot="title">Welkom (20:00)</div>
          <ul>
            <li>
              <p>Aanwezigheidsregistratie.</p>
            </li>
            <li>
              <p>De structuur van deze les.</p>
            </li>
          </ul>
        </component-section>
        <component-section>
          <div slot="title">Kennismaking (20:10)</div>
        </component-section>
        <component-section>
          <div slot="title">Studiehandleiding (20:40)</div>
        </component-section>
        <component-section>
          <div slot="title">Zelf bestuderen en basisopgaven maken (21:00)</div>
          <p>
            <a href="https://secure.ans-delft.nl/universities/13/courses/43101/assignments/79347/quiz/start"
              >Basisopgaven les 1: Algoritmes en complexiteit</a
            >
          </p>
        </component-section>
      </component-section>
      <component-section>
        <div slot="title">Bijeenkomst 2020-09-03</div>
        <component-section>
          <div slot="title">Voorbereiding</div>
          <a
            href="https://tasks.office.com/hannl.onmicrosoft.com/NL-NL/Home/Planner#/plantaskboard?groupId=fc6fdef6-3dbb-4155-a4ed-a65e4cdadd12&planId=CAby-9tdR06SEiFFGkHtIJcAB2iv"
            >Planner-plan</a
          >
        </component-section>
        <component-section>
          <div slot="title">Welkom (18:30)</div>
          <ul>
            <li>
              <p>Aanwezigheidsregistratie.</p>
            </li>
            <li>
              <p>De structuur van deze les.</p>
            </li>
          </ul>
        </component-section>
        <component-section>
          <div slot="title">Mindmap maken over algoritmes en complexiteit (18:45)</div>
          <component-section>
            <div slot="title">Bespreking</div>
          </component-section>
        </component-section>
        <component-section>
          <div slot="title">Samenoefeningen</div>
          <component-section>
            <div slot="title">Vereenvoudig naar groeiorde (19:15)</div>
            <component-exercise together>
              <p>Vereenvoudig de volgende looptijdfuncties \\(T(n)\\) naar groeiordes:</p>
              <ol>
                <li><p>\\(n^2\\)</p></li>
                <li><p>\\(n\\)</p></li>
                <li><p>\\(n^2+n\\)</p></li>
                <li><p>\\(n^2-n\\)</p></li>
                <li><p>\\(\\frac{n^3}{n-1}\\)</p></li>
              </ol>
              <div class="instructor_only answer">
                <details>
                  <summary>Voorbeeldantwoord</summary>
                  <ol>
                    <li><p>\\(n^2\\)</p></li>
                    <li><p>\\(n\\)</p></li>
                    <li>
                      <p>\\(n^2\\)</p>
                      <p>Vereenvoudig tot de dominante term.</p>
                    </li>
                    <li><p>\\(n^2\\)</p></li>
                    <li>
                      <p>\\(n^2\\)</p>
                      <p>
                        <a
                          href="https://www.symbolab.com/solver/step-by-step/simplify%20%5Cfrac%7Bn%5E%7B3%7D%7D%7Bn-1%7D"
                          >Vereenvoudig algebraïsch</a
                        >
                        tot \\(n^2 + n + \\frac{1}{n - 1} + 1\\), kies vervolgens de dominante term.
                      </p>
                    </li>
                  </ol>
                </details>
              </div>
            </component-exercise>
          </component-section>
          <component-section>
            <div slot="title">Looptijd omrekenen (19:30)</div>
            <component-exercise together>
              <p>
                Een algoritme heeft een looptijd van \\(0.5\\) ms bij een invoergrootte van \\(100\\). Wat is de
                looptijd van het algoritme bij een invoergrootte van \\(500\\), als de groeiorde ervan
              </p>
              <ol>
                <li><p>lineair</p></li>
                <li><p>linearitmisch</p></li>
                <li><p>kwadratisch</p></li>
                <li><p>kubisch</p></li>
              </ol>
              <p>
                is? Neem aan dat het geval gelijk is. Neem ook aan dat de echte looptijdfunctie \\(T(n)\\) gelijk is aan
                de groeiorde.
              </p>
              <div class="instructor_only answer">
                <details>
                  <summary>Voorbeeldantwoord</summary>
                  <ol>
                    <li>
                      <p>\\(2.5\\) ms</p>
                      <p>
                        \\(n=100\\) staat tot \\(n=500\\), zoals \\(0.5\\) ms staat tot de onbekende looptijd \\(t\\).
                        Om te berekenen wat \\(t\\) is bij \\(n=500\\) kan je m.a.w. de volgende vergelijking oplossen
                        d.m.v. kruislings vermenigvuldigen:
                      </p>
                      <p>\\(\\frac{100}{500} = \\frac{0.5}{t}\\)</p>
                      <p>\\(t100=500 × 0.5\\)</p>
                      <p>\\(t=\\frac{500 × 0.5}{100} = 2.5\\) ms</p>
                    </li>
                    <li>
                      <p>\\(3.37\\) ms</p>
                      <p>\\(\\frac{100 \\log(100)}{500 \\log(500)} = \\frac{0.5}{t}\\)</p>
                      <p>\\(t 100 \\log(100) = 500 \\log(500) 0.5\\)</p>
                      <p>\\(t=\\frac{500 \\log(500) 0.5}{100 \\log(100)} = 3.37\\) ms</p>
                    </li>
                    <li><p>\\(12.5\\) ms</p></li>
                    <li><p>\\(62.5\\) ms</p></li>
                  </ol>
                </details>
              </div>
            </component-exercise>
          </component-section>
          <component-section>
            <div slot="title">Looptijd omrekenen (20:00)</div>
            <component-exercise together>
              <p>
                Een algoritme heeft een looptijd van \\(3\\) s bij een invoergrootte van \\(40\\). Wat is de looptijd
                van het algoritme bij een invoergrootte van \\(60\\), als de groeiorde ervan
              </p>
              <ol>
                <li><p>lineair</p></li>
                <li><p>kwadratisch</p></li>
                <li><p>kubisch</p></li>
              </ol>
              <p>
                is? Neem aan dat het geval gelijk is. Neem ook aan dat de echte looptijdfunctie \\(T(n)\\) gelijk is aan
                de groeiorde.
              </p>
              <div class="instructor_only answer">
                <details>
                  <summary>Voorbeeldantwoord</summary>
                  <ol>
                    <li><p>\\(1.5 × 3 = 4.5\\) s</p></li>
                    <li><p>\\(1.5^2 × 3 = 6.8\\) s</p></li>
                    <li><p>\\(1.5^3 × 3 = 10.1\\) s</p></li>
                  </ol>
                </details>
              </div>
            </component-exercise>
          </component-section>
          <component-section>
            <div slot="title">Looptijd omrekenen (20:30)</div>
            <component-exercise together>
              <p>
                Een algoritme heeft een looptijd van \\(3\\) s bij een invoergrootte van \\(40\\) in alle gevallen. Het
                algoritme heeft een looptijd van \\(1\\) uur bij een invoergrootte van \\(7200\\) in alle gevallen. Wat
                is de groeiorde van het algoritme?
              </p>
              <div class="instructor_only answer">
                <details>
                  <summary>Voorbeeldantwoord</summary>
                  <p>
                    Een uur is \\(3600\\) s, dus er verstrijkt \\(3600/3 = 1200\\) keer zoveel looptijd als de invoer
                    \\(7200/40 = 180\\) keer zo groot is. Bij de groeiorde \\(n \\log(n)\\) wordt de looptijd ongeveer
                    \\(180 × \\log^{2}(180) ≈ 1349\\) keer langer. \\(n \\log(n)\\) blijkt de beste benadering van de
                    verhouding ‘\\(1200\\) keer’ tussen looptijden, als je alle groeiordes op deze manier als formules
                    gebruikt.
                  </p>
                </details>
              </div>
            </component-exercise>
          </component-section>
          <component-section>
            <div slot="title">Meten van groeisnelheid (20:45)</div>
            <component-exercise together>
              <ol>
                <li>
                  <p>
                    Programmeer een lus met een lineaire, kwadratische en exponentiële groeiorde, die iets constants
                    uitvoert.
                  </p>
                </li>
                <li>
                  <p>Meet de looptijden met geschikte waardes voor \\(n\\).</p>
                </li>
                <li>
                  <p>Exporteer deze looptijden naar een spreadsheet. Maak een werkblad per groeiorde.</p>
                </li>
                <li>
                  <p>
                    Bereken de
                    <a
                      href="https://www.ck12.org/algebra/identifying-function-models/lesson/Linear-Exponential-and-Quadratic-Models-BSC-ALG/"
                      >groeiverhoudingen</a
                    >
                    als extra kolommen.
                  </p>
                </li>
                <li>
                  <p>Maak lijngrafieken van je meetgegevens, en kijk of je de lijn kan matchen met de groeiorde.</p>
                </li>
              </ol>
              <component-section>
                <div slot="title">Bespreking (21:15)</div>
              </component-section>
            </component-exercise>
          </component-section>
        </component-section>
      </component-section>`,
    document.getElementById('lessonAlgo'),
  );
