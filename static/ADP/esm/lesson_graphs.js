import { html, render } from 'lit-html';

export const renderLessonGraphs = () =>
  render(
    html`<div slot="title">Grafen</div>
      <component-section>
        <div slot="title">Wat je leert</div>
        <component-intendedlearningoutcomes>
          <component-intendedlearningoutcome codes='["AD-begrip-standaardalgoritmes-datastructuren"]'
            >legt het wiskundige <strong>graaf</strong>-object uit.
          </component-intendedlearningoutcome>
          <component-intendedlearningoutcome codes='["AD-begrip-standaardalgoritmes-datastructuren"]'
            >legt standaard graaf-datastructuren uit (<strong>adjacency matrix</strong>,
            <strong>adjacency list</strong>).
          </component-intendedlearningoutcome>
          <component-intendedlearningoutcome codes='["AD-begrip-standaardalgoritmes-datastructuren"]'>
            legt het
            <strong>kortstepadenalgoritme</strong>
            van Dijkstra uit, t.a.v. ongewogen en gewogen grafen.
          </component-intendedlearningoutcome>
        </component-intendedlearningoutcomes>
        <component-miroboard idBoard="o9J_lfmZYpc" viewport="-755,-524,1100,858" title="Mind map"></component-miroboard>
      </component-section>
      <component-section>
        <div slot="title">Waarom je over grafen leert</div>
        <p>
          Grafen zijn veruit de flexibelste en breedst toepasbare adt’s die er zijn. Afhankelijk van wat voor type graaf
          je bedoelt kunnen de eigenschappen van de mogelijke graaf-datastructuren erg verschillen. Deze verschillen
          zijn echter allemaal wiskundig te modelleren, en ook buiten de informatica wordt dat veelvuldig gedaan.
        </p>
        <p class="praktijk">Routeplanners zijn afhankelijk van grafen.</p>
        <p class="praktijk">
          Je hebt al met Maven gewerkt. Net als andere package managers gebruikt Maven een graaf om afhankelijkheden
          tussen softwaremodules uit te rekenen.
        </p>
      </component-section>
      <component-section>
        <div slot="title">Grafen</div>
        <p>OpenDSA heeft een uitstekende samenvatting van basale grafentheorie, incl. demo’s.</p>
        <p class="bron">
          <a href="https://opendsa-server.cs.vt.edu/ODSA/Books/CS3/html/GraphIntro.html"
            >14.1. Graphs Chapter Introduction</a
          >
        </p>
        <p>
          Er zijn twee belangrijkste manieren om een graaf als datastructuur te implementeren, namelijk een
          <em class="term">adjacency list</em> en een <em class="term">adjacency matrix</em>.
        </p>
        <p class="bron">
          <a href="https://opendsa-server.cs.vt.edu/ODSA/Books/CS3/html/GraphImpl.html">14.2. Graph Implementations</a>
        </p>
        <p>
          In het Nederlands noemen we een
          <em class="term">vertex</em> een <em class="term">punt</em> en een <em class="term">edge</em> een
          <em class="term">lijn</em>. Een <em class="term">path</em> is gewoon een <em class="term">pad</em>.
        </p>
        <p>Voor een visuele uitleg, en om je Brits-Engels te trainen 😁, bekijk je Kevin Drumm’s video.</p>
        <p class="bron">Graph Data Structure 1. Terminology and Representation (algorithms)</p>
        <component-video
          videoid="c8P9kB1eun4"
          title="Graph Data Structure 1. Terminology and Representation (algorithms)"
        ></component-video>
      </component-section>
      <component-section>
        <div slot="title">Dijkstra’s algoritme voor kortste paden</div>
        <p>
          Er zijn twee standaardmanieren om een graaf
          <em class="term">af te lopen</em> (<em class="term">traverse, traversal</em>):
          <em class="term">breadth-first</em> en <em class="term">depth-first</em>. Dat doe je meestal op zoek naar iets
          specifieks, zoals een punt met een bepaalde waarde, dus dan heet dit search (met afkortingen
          <em class="term">BFS</em> voor breadth-first search en <em class="term">DFS</em> voor depth-first search).
        </p>
        <p class="bron">
          <a href="https://opendsa-server.cs.vt.edu/ODSA/Books/CS3/html/GraphTraversal.html">14.3. Graph Traversals</a>
        </p>
        <component-section>
          <div slot="title">BFS vs. DFS</div>
          <component-exercise>
            <p>Benoem zo veel mogelijk verschillen tussen BFS en DFS.</p>
          </component-exercise>
        </component-section>
        <component-section>
          <div slot="title">Single-source shortest paths</div>
          <p>
            Er zijn manieren om een search vanuit één punt \\(a\\) naar alle andere punten \\(v \\in V\\) te doen, en
            daarbij te bepalen hoeveel gewicht ieder pad \\(a → v\\) heeft. Het standaardalgoritme hiervoor is
            <em class="term">Dijkstra’s kortstepadenalgoritme vanuit één punt</em>. Als de graaf
            <em class="term">gewogen</em> is, heeft iedere lijn een eigen gewicht. Als het daarentegen een
            <em class="term">ongewogen</em> graaf is, kan je het gewicht op \\(1\\) stellen. In dat geval loopt het
            algoritme de graaf af als een BFS.
          </p>
          <p>OpenDSA legt het algoritme uit, en hoe je dit zou kunnen implementeren in Java</p>
          <p class="extra">
            Het stuk in deze bron over implementatie met een
            <em>min-heap</em>-datastructuur is extra.
          </p>
          <p class="bron">
            <a href="https://opendsa-server.cs.vt.edu/ODSA/Books/CS3/html/GraphShortest.html"
              >14.5. Shortest-Paths Problems</a
            >
          </p>
          <p class="bron">Graph Data Structure 4. Dijkstra’s Shortest Path Algorithm</p>
          <component-video
            videoid="pVfj6mxhdMw"
            title="Graph Data Structure 4. Dijkstra’s Shortest Path Algorithm"
          ></component-video>
          <p>
            Trouwens, als twee afstanden gelijk zijn en je wilt de volgende node kiezen, dan kan je vertrouwen op de
            werking van de
            <code>dequeue</code>-operatie van de priority queue die je (evt. in je hoofd) gebruikt. Zelfs als punten
            dezelfde prioriteit hebben, dan zijn ze in een bepaalde volgorde ge-<code>enqueue</code>d en zal je ze op
            die volgorde weer <code>dequeue</code>n.
          </p>
          <component-section>
            <div slot="title">Dijkstra’s kortstepadenalgoritme bij negatieve gewichten</div>
            <component-exercise>
              <p>
                Waarom zal Dijkstra’s kortstepadenalgoritme vanuit één punt niet meer correct zijn als een lijngewicht
                ook negatief kan zijn?
              </p>
            </component-exercise>
          </component-section>
        </component-section>
        <component-section extra>
          <div slot="title">All-pairs shortest paths</div>
          <p>Dijkstra’s algoritme is te generaliseren tot een algo dat de kortste paden tussen élk punt vindt.</p>
          <p class="bron">
            <a href="https://opendsa-server.cs.vt.edu/ODSA/Books/CS3/html/Floyd.html"
              >14.8. All-Pairs Shortest Paths
            </a>
          </p>
          <p class="bron">Graph Data Structure 4. Dijkstra’s Shortest Path Algorithm</p>
        </component-section>
      </component-section>
      <component-section>
        <div slot="title">Bijeenkomst 2020-11-17</div>
        <component-section>
          <div slot="title">Welkom (20:00)</div>
          <ul>
            <li>
              <p>Aanwezigheidsregistratie.</p>
            </li>
            <li>
              <p>De structuur van deze les.</p>
            </li>
          </ul>
        </component-section>
        <component-section>
          <div slot="title">Korte bespreking van de leerinhoud (20:05)</div>
        </component-section>
        <component-section>
          <div slot="title">Zelf bestuderen en basisopgaven maken (20:20)</div>
          <p>
            <a href="https://secure.ans-delft.nl/universities/13/courses/43101/assignments/103062/quiz/start"
              >Basisopgaven les 6: Grafen</a
            >
          </p>
        </component-section>
      </component-section>
      <component-section>
        <div slot="title">Bijeenkomst 2020-11-19</div>
        <component-section>
          <div slot="title">Welkom (18:30)</div>
          <ul>
            <li>
              <p>Aanwezigheidsregistratie.</p>
            </li>
            <li>
              <p>
                Hoe is het gegaan? Neem even de tijd om de
                <a href="https://secure.ans-delft.nl/universities/13/courses/43101/assignments/95815/quiz/start"
                  >vragenlijst ‘Je huidige leerresultaten’</a
                >
                (weer) in te vullen.
              </p>
            </li>
            <li>
              <p>De structuur van deze les.</p>
            </li>
          </ul>
        </component-section>
        <component-section>
          <div slot="title">Samenopgaven</div>
          <p>
            <a href="https://secure.ans-delft.nl/universities/13/courses/43101/assignments/105547/quiz/start"
              >Samenopgaven les 6: Grafen</a
            >
          </p>
          <component-section>
            <div slot="title">Dijkstra’s kortstepadenalgoritme vanuit één punt</div>
            <component-exercise together></component-exercise>
          </component-section>
          <component-section>
            <div slot="title">Dijkstra’s kortstepadenalgoritme vanuit één punt implementeren</div>
            <component-exercise together></component-exercise>
          </component-section>
        </component-section>
      </component-section>`,
    document.getElementById('lessonGraphs'),
  );
