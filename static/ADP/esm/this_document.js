import { citationData } from './citationdata.js';
import { renderDocument } from '../../esm/render_document.js';
import { renderLessonAlgo } from './lesson_algo.js';
import { renderLessonAdt } from './lesson_adt.js';
import { renderLessonRecursion } from './lesson_recursion.js';
import { renderLessonSorting } from './lesson_sorting.js';
import { renderLessonHashing } from './lesson_hashing.js';
import { renderLessonGraphs } from './lesson_graphs.js';
import { renderLessonTrees } from './lesson_trees.js';
import { renderLessonProgramminglanguages } from './lesson_programminglanguages.js';

renderLessonAlgo();
renderLessonAdt();
renderLessonRecursion();
renderLessonSorting();
renderLessonHashing();
renderLessonGraphs();
renderLessonTrees();
renderLessonProgramminglanguages();
renderDocument(citationData, '..');
