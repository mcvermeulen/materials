import { html, render } from 'lit-html';

export const renderLessonRecursion = () =>
  render(
    html`<div slot="title">Recursie en algoritme-ontwerpparadigma’s</div>
      <component-section>
        <div slot="title">Wat je leert</div>
        <component-intendedlearningoutcomes>
          <component-intendedlearningoutcome codes='["AD-begrip-standaardalgoritmes-datastructuren"]'>
            legt
            <strong>recursie</strong>
            uit.</component-intendedlearningoutcome
          >
          <component-intendedlearningoutcome codes='["AD-begrip-standaardalgoritmes-datastructuren"]'>
            legt
            <strong>recursie</strong>
            uit.</component-intendedlearningoutcome
          >
          <component-intendedlearningoutcome codes='["AD-begrip-standaardalgoritmes-datastructuren"]'>
            legt
            <strong>algoritme-ontwerpparadigma</strong>
            uit (<strong>Brute force</strong>, <strong>Divide and conquer</strong>, <strong>Greediness</strong>,
            <strong>Dynamic programming</strong>).</component-intendedlearningoutcome
          >
          <component-intendedlearningoutcome codes='["AD-productie-computationele-taak"]'
            >programmeert een recursieve functie uit.</component-intendedlearningoutcome
          >
        </component-intendedlearningoutcomes>
        <component-miroboard
          idBoard="o9J_kjBpmns"
          viewport="-474,-850,1652,1289"
          title="Mind map"
        ></component-miroboard>
      </component-section>
      <component-section>
        <div slot="title">Waarom?</div>
        <p>
          Recursie is voor algoritmes vaak erg belangrijk. Recursie zorgt ervoor dat functies beknopt kunnen worden
          opgeschreven, en dat je over het probleem dat een algoritme moet oplossen makkelijker kan nadenken.
        </p>
        <p>
          Algoritme-ontwerpparadigma’s geven je inzicht in algemene manieren om een algoritme te ontwikkelen en te
          begrijpen. Dat is een belangrijke vaardigheid zodra je uiteindelijk de standaardalgoritmes wel paraat hebt,
          maar geconfronteerd kan worden met nieuwe.
        </p>
      </component-section>
      <component-section>
        <div slot="title">Recursie</div>
        <p>Recursie houdt in dat een functie pas zijn waarde krijgt door zichzelf weer aan te roepen.</p>
        <p class="bron">
          <a href="http://jeffe.cs.illinois.edu/teaching/algorithms/book/01-recursion.pdf">Recursion</a>
          <cite data-id="6867189/PLHZEH2X" data-label="section" data-locator="1.1-1.3"></cite>
        </p>
        <p><em class="term">Recursie</em> zie je terug in broncode als een aanroep van een functie naar zichzelf.</p>
        <p>
          Wat je met recursie kan doen kan je ook met een lus doen. Zie bijv. de implementatie van
          <a href="https://rosettacode.org/wiki/Binary_search">binary search</a>.
        </p>
        <ol>
          <li>
            <p><em class="term">Basisgeval</em> (base case): hier eindigt de recursie.</p>
          </li>
          <li>
            <p><em class="term">Recursief geval</em> (recursive case)</p>
          </li>
        </ol>
        <p>
          Welk van bovenstaande gevallen van toepassing is, ligt aan de conditionele structuur (bijv.
          <code class="lang-java">if () {} else {}</code>). Een recursie is <em class="term">eindig</em> als het
          basisgeval werkelijk bereikt kan worden (dwz. de conditie daarvoor kan waar worden).
        </p>
        <!-- TODO: Stellingen over vertaling algoritme-implementaties van recursief <-> iteratief. -->
        <p>
          Een de looptijdfunctie van een recursief algoritme kan je wiskundig beschrijven met een
          <em class="term">recurrente betrekking</em> (<em class="term">recurrence</em>). De looptijdfunctie zo
          opschrijven is intuïtief. Een ander voordeel is dat je enkele standaard wiskundige procedures kan toepassen om
          algoritme-analyse te doen o.b.v. de looptijdfunctie, in plaats van de (ingewikkelde) recursieve broncode te
          moeten analyseren. Zulke wiskundige algoritme-analyse doen valt buiten de leerinhoud van ADP. Maar het is wel
          handig om te leren om een recursieve formule als een recurrente betrekking op te schrijven.
        </p>
        <p>Ook algoritmes zelf kan je soms met een recurrente betrekking beschrijven. Zie het volgende voorbeeld:</p>
        <figure>
          <figcaption>Recurrente betrekking voor vermenigvuldigen.</figcaption>
          <!-- prettier-ignore -->
          <p>$$x·y = \\begin{cases}
0                                     &amp; \\text{als } x = 0 \\\\
\\lfloor x/2 \\rfloor · (y + y)       &amp; \\text{als } x \\text{ even is} \\\\
\\lfloor x/2 \\rfloor · (y + y) + y   &amp; \\text{als } x \\text{ oneven is} \\\\
\\end{cases}$$</p>
        </figure>
        <p>In het voorgaande voorbeeld is vermenigvuldigen (althans, de operator \\(·\\)) recursief gedefinieerd.</p>
        <ul>
          <li>
            <p>
              De betrekking is te lezen als een
              <code class="lang-java">if () {} else if {}</code>-achtige conditionele structuur. De conditie staat aan
              de rechterkant, het conditionele blok aan de linkerkant.
            </p>
          </li>
          <li>
            <p>
              In de twee gevallen anders dan het basisgeval \\(x = 0\\) is \\(x·y\\) gedefinieerd als een recursieve
              uitdrukking. Die twee uitdrukkingen verwijzen namelijk weer naar zichzelf, door de \\(·\\) die er opnieuw
              in staat.
            </p>
          </li>
          <li>
            <p>
              \\(\\lfloor d \\rfloor\\) betekent: alle cijfers achter de komma van \\(d\\) weggooien, om zo alleen het
              gehele getal voor de komma te behouden. (Tussen deze haakjes mag in plaats van \\(d\\) iedere uitdrukking
              met een rationaal getal als waarde staan.)
            </p>
          </li>
        </ul>
        <component-section>
          <div slot="title">Vertaal een recurrente betrekking naar een functie in broncode</div>
          <component-exercise>
            <p>Vertaal de recurrente betrekking uit het voorgaande voorbeeld naar broncode.</p>
          </component-exercise>
        </component-section>
        <component-section>
          <div slot="title">Vertaal recursie naar iteratie</div>
          <component-exercise>
            <p>Iteratie ken je al: met een lus werken.</p>
            <p>
              Maak een tweede variant van de functie die je uitprogrammeerde voor de vorige opgave. Deze variant
              gebruikt iteratie in plaats van recursie.
            </p>
          </component-exercise>
        </component-section>
      </component-section>
      <component-section>
        <div slot="title">Algoritme-ontwerpparadigma’s</div>
        <p>
          Een algoritme kan <em class="term">incrementeel</em> te werk gaan. Het werkt dan op één positie binnen een
          data(structuur). Denk aan een index in een rij.
        </p>
        <p>
          Het eenvoudigste algoritme-ontwerpparadigme is
          <em class="term">brute force</em> (dommekracht). Brute force betekent dat een algoritme alle mogelijke paden
          naar een oplossing van een probleem berekent, ongeacht of het onnodig (bijv. dubbel) werk aan het doen is.
        </p>
        <p>
          Een alternatief hiervoor is het verdelen van het probleem, en op meerdere posities tegelijk werken. Het
          probleem kan dan verdeeld worden over meerdere processors en daardoor sneller worden opgelost. Een ander
          voordeel is dat het algoritme eenvoudiger kan worden, zoals bij recursie. Tot slot zou een voordeel kunnen
          zijn dat een deeloplossing van het probleem het mogelijk maakt een deel van de rest van het probleem links te
          laten liggen.
        </p>
        <component-section>
          <div slot="title">Divide-and-conquer</div>
          <p>
            Veel problemen hebben
            <em class="term">optimale substructuur</em>: om het hele probleem op te lossen, komt erop neer dat je
            (steeds) kleinere deelproblemen oplost. Je lost het probleem op door de deeloplossingen te combineren. Merge
            sort is hier straks een voorbeeld van.
          </p>
          <p>
            De drie stappen van
            <em class="term">divide-and-conquer</em> (verdeel en heers) zijn:
          </p>
          <ol>
            <li>
              <p><em>Divide</em>: opdelen probleem in delen van een datastructuur.</p>
            </li>
            <li>
              <p><em>Conquer</em>: verder opdelen, óf als deelprobleem inderdaad klein genoeg is, oplossen.</p>
            </li>
            <li>
              <p><em>Combine</em>: combineren van de oplossingen voor de deelproblemen.</p>
            </li>
          </ol>
          <p>
            Hoe klein een deelprobleem wordt hangt af van het algoritme zelf. Het kan bijv. \\(\\frac{1}{b}\\)<sup
              >e</sup
            >
            deel zijn, waarbij \\(b\\) bijv. \\(2\\) is (dat is zo bij merge sort).
          </p>
          <p>
            Misschien had je het al door: als je steeds kleinere subproblemen aan het oplossen bent, dan is recursie een
            natuurlijke vorm om het algoritme te ontwerpen.
          </p>
          <!-- TODO: -->
          <!--
            <p class="bron">
                <cite data-id="6867189/GMIDVX6P" data-label="section" data-locator="2.3"></cite>
                <cite data-id="6867189/GMIDVX6P" data-label="chapter" data-locator="4"></cite>
                <cite data-id="6867189/GMIDVX6P" data-label="section" data-locator="4-4.5"></cite>
                (Divide-and-Conquer)
            </p>
          -->
        </component-section>
        <component-section>
          <div slot="title">Dynamic programming</div>
          <p>
            <em class="term">Dynamic programming</em> is een andere manier om deeloplossingen te gebruiken dan
            divide-and-conquer. Dynamic programming wordt gebruikt als een probleem alleen op te delen is in
            <em>overlappende</em> deelproblemen, terwijl divide-and-conquer het probleem in gescheiden deelproblemen
            oplost. Dynamic programming wordt vaak gebruikt bij algoritmes die <em>optimalisatieproblemen</em> oplossen.
            Er zijn dan meerdere oplossingen correct. De oplossingen hebben een bepaalde waarde, en de
            <em>beste</em> oplossing heeft de laagste dan wel hoogste waarde. Dan zou je dynamic programming als volgt
            kunnen definiëren:
          </p>
          <ol>
            <li>
              <p>Stel de structuur vast van de optimale oplossing.</p>
            </li>
            <li>
              <p>Definieer de waarde van de optimale oplossing recursief.</p>
            </li>
            <li>
              <p>Bereken een steeds optimalere oplossing bottom-up.</p>
            </li>
            <li>
              <p>Structureer de optimale oplossing.</p>
            </li>
          </ol>
          <p>
            ‘Dynamic’ staat voor de notie dat het algoritme slim bepaalt welk deelprobleem het telkens gaat oplossen.
            Heel wat slimmer dan brute force, tenminste!
          </p>
          <p>
            Ook bij sommige andersoortige algoritmes, zoals bijv. de Rij van Fibonacci berekenen, zijn er meerdere
            stadia waarin het algoritme hetzelfde deelprobleem tegenkomt. Dan is het efficiënter om het deelprobleem
            niet meermaals op te gaan lossen, en dat kan met dynamic programming.
          </p>
          <p>
            Het bewaren van deeloplossingen heet
            <em class="term">memoisatie</em> (memoization). Door deeloplossingen te bewaren, wissel je looptijd in voor
            geheugenruimte.
          </p>
          <p>
            In de volgende bron legt Gayle Laakmann McDowell uit hoe dynamic programming met memoisatie er concreet
            uitziet, met een algoritme om de rij van Fibonacci te berekenen.
          </p>
          <p class="bron">Algorithms: Memoization and Dynamic Programming</p>
          <component-video
            videoid="P8Xa2BitN3I"
            title="Algorithms: Memoization and Dynamic Programming"
          ></component-video>
          <!-- TODO: -->
          <!-- 
            <p class="bron">
              <cite data-id="6867189/GMIDVX6P" data-label="page" data-locator="357-385"></cite>
              <cite data-id="6867189/GMIDVX6P" data-label="chapter" data-locator="15-15.1" data-suffix="incl. inleiding"></cite>
              <cite data-id="6867189/GMIDVX6P" data-label="section" data-locator="15.3"></cite>
              (Dynamic Programming)
          </p> 
        -->
        </component-section>
        <component-section>
          <div slot="title">Greediness</div>
          <p>
            Soms is het makkelijker om snel een oplossing te geven op basis van een deel van de de invoerdata. Welk
            deelprobleem het algoritme oplost (de volgorde daarvan) beinvloedt dan wel oplossing waarop het uitkomt voor
            het hele probleem. Desondanks kan het algoritme voor de optimale oplossing voor het deelprobleem gaan, in de
            verwachting dat dit ook leidt tot een optimale oplossing voor het hele probleem. Dit heet
            <em class="term">greediness</em> (gulzigheid).
          </p>
          <p>
            Een voorbeeld hiervan is het algoritme: geef het minimale aantal munten dat je nodig hebt als wisselgeld. We
            kunnen de munt die het meeste waard is kiezen die niet groter is dan het verschil dat overblijft. Voor dit
            soort problemen komt een greedy algoritme sneller op de optimale oplossing dan een dynamic
            programming-algoritme.
          </p>
          <!-- TODO: -->
          <!--
            <p class="bron">
              <cite data-id="6867189/GMIDVX6P"></cite>
              <cite data-id="6867189/GMIDVX6P" data-label="section" data-locator="16-16.2"></cite>
              (Greedy Algorithms)
          </p>
        -->
        </component-section>
      </component-section>
      <component-section>
        <div slot="title">Bijeenkomst 2020-10-06</div>
        <component-section>
          <div slot="title">Welkom (20:00)</div>
          <ul>
            <li>
              <p>Aanwezigheidsregistratie.</p>
            </li>
            <li>
              <p>Hoe het met Sander gaat.</p>
            </li>
            <li>
              <p>De structuur van deze les.</p>
            </li>
          </ul>
        </component-section>
        <component-section>
          <div slot="title">Opdracht Beroepsproduct compiler (20:10)</div>
          <p>Staat <a href="Beroepsproduct_compiler/">online</a>.</p>
          <p>
            In les 8 krijg je instructie over de opdracht. Les 8 pas, omdat alle kennis uit ADP hierin samenkomt, maar
            vooral de inhoud van het thema programmeertalen (les 8).
          </p>
          <p></p>
          <p>
            Het is slim om toch de opdrachttekst alvast te bestuderen, en voor jezelf na te gaan welke benodigde extra
            kennis je uit les 8 wilt halen.
          </p>
        </component-section>
        <component-section>
          <div slot="title">Zelf bestuderen en basisopgaven maken (20:15)</div>
          <p>
            <a href="https://secure.ans-delft.nl/universities/13/courses/43101/assignments/85956/quiz/start"
              >Basisopgaven les 3: Recursie en algoritme-ontwerpparadigma’s</a
            >
          </p>
        </component-section>
        <component-section>
          <div slot="title">Mindmap maken over recursie en algoritme-ontwerpparadigma’s (in tweetallen) (21:10)</div>
          <component-exercise together>
            <component-section>
              <div slot="title">Bespreking (21:20)</div>
            </component-section>
          </component-exercise>
        </component-section>
        <component-section>
          <div slot="title">Volgende bijeenkomst (21:28)</div>
          <ul>
            <li>
              <p>Tijdens onze volgende bijeenkomst ga ik laten zien hoe je performanceonderzoek kan doen.</p>
            </li>
            <li>
              <p>Vervolgens heb je gelegenheid om te werken aan de opdracht voor OP, Onderzoek naar performance.</p>
            </li>
          </ul>
        </component-section>
      </component-section>
      <component-section>
        <div slot="title">Bijeenkomst 2020-10-08</div>
        <component-section>
          <div slot="title">Extra voorbereiding</div>
          <ul>
            <li>
              <p>Mind map recursie en algoritme-ontwerpparadigma’s.</p>
            </li>
            <li>
              <p>Goedgekeurd voorstel voor software-oplossing Onderzoek naar performance.</p>
            </li>
          </ul>
        </component-section>
        <component-section>
          <div slot="title">Welkom (18:30)</div>
          <ul>
            <li>
              <p>Aanwezigheidsregistratie.</p>
            </li>
            <li>
              <p>
                Hoe is het gegaan? Neem even de tijd om de
                <a href="https://secure.ans-delft.nl/universities/13/courses/43101/assignments/95815/quiz/start"
                  >vragenlijst ‘Je huidige leerresultaten’</a
                >
                (weer) in te vullen.
              </p>
            </li>
            <li>
              <p>De structuur van deze les.</p>
            </li>
          </ul>
        </component-section>
        <component-section>
          <div slot="title">Samenopgaven</div>
          <component-section>
            <div slot="title">
              Bespreking <a href="#_1_2_10_7_1_Datastructuurtabel__tweetal___19_20_">Datastructuurtabel</a> (tweetal)
              (18:35)
            </div>
            <component-exercise together></component-exercise>
          </component-section>
          <component-section>
            <div slot="title">
              Bespreking
              <a href="#_1_2_10_7_2_The_queuest_against_corona__tweetal_">The queuest against corona</a> (tweetal)
              (18:45)
            </div>
            <component-exercise together>
              <p class="instructor_only">
                Mijn uitwerking staat op
                <a href="https://gitlab.com/han-aim/dt-sd-asd/adp/max_deelrij"
                  >https://gitlab.com/han-aim/dt-sd-asd/adp/max_deelrij</a
                >.
              </p>
            </component-exercise>
          </component-section>
          <component-section>
            <div slot="title">Ontwikkel een algoritme dat de deelrij met maximale som vindt (tweetal) (19:15)</div>
            <component-exercise together>
              <p>
                <a href="https://secure.ans-delft.nl/universities/13/courses/43101/assignments/85957/quiz/start"
                  >Samenopgaven les 3: Recursie en algoritme-ontwerpparadigma’s</a
                >
              </p>
              <p class="bron">
                <a href="https://web.cs.ucdavis.edu/~bai/ECS122A/Notes/Maxsubarray.pdf">The maximum-subarray problem</a>
              </p>
              <component-section>
                <div slot="title">Bespreking (19:45)</div>
                <p class="instructor_only">
                  Mijn uitwerking staat op
                  <a href="https://gitlab.com/han-aim/dt-sd-asd/adp/queuest-against-corona"
                    >https://gitlab.com/han-aim/dt-sd-asd/adp/queuest-against-corona</a
                  >.
                </p>
              </component-section>
            </component-exercise>
          </component-section>
        </component-section>
        <component-section>
          <div slot="title">📈 Performanceonderzoek (20:00)</div>
          <component-section>
            <div slot="title">Begrippenkader</div>
            <p>
              <em class="term">Profiling</em> is het verzamelen van statistische informatie over voor
              performance-onderzoek relevante soorten gebeurtenissen terwijl een software-systeem loopt.
            </p>
            <ul>
              <li>
                <p>Gebeurtenissen binnen het systeem</p>
                <ul>
                  <li>
                    <p>Hoeveel geheugenallocaties</p>
                  </li>
                  <li>
                    <p>Hoeveel threads</p>
                  </li>
                  <li>
                    <p>Hoeveel geheugenruimte in gebruik</p>
                  </li>
                  <li>
                    <p>...</p>
                  </li>
                </ul>
              </li>
              <li>
                <p>Gebeurtenissen buiten het systeem</p>
                <ul>
                  <li>
                    <p>Hoeveel keer swapfile aangesproken</p>
                  </li>
                  <li>
                    <p>Hoeveel threads actief in totaal</p>
                  </li>
                  <li>
                    <p>Hoeveel dataverkeer</p>
                  </li>
                  <li>
                    <p>Hoeveel stroomverbruik</p>
                  </li>
                  <li>
                    <p>...</p>
                  </li>
                </ul>
              </li>
            </ul>
            <p>
              <em class="term">Benchmarking</em> houdt in performance testen in vergelijking met een bepaalde baseline
              (normniveau) en/of alternatieven.
            </p>
            <p>
              Microbenchmarking betekent dat de computationele taak kortlopend is. Dat is dus zelden een hele use-case
              of functionele test, maar vaker bijv. een unit test of integratietest.
            </p>
            <p>
              <em class="term">Performance testing</em> is een verzamelbegrip. Om te <em>testen</em> heb je criteria
              nodig en een context (zoals de qas’en uit SKA). Dat maakt het verschil met performance engineering in
              brede zin. Je ontwikkelt niet (soms doelloos of prematuur), maar je bent vooral aan het onderzoeken.
            </p>
          </component-section>
          <component-section>
            <div slot="title">Keuze software-oplossing</div>
            <p>Voorbeelden van open-source software:</p>
            <ul>
              <li>
                <p><a href="https://github.com/dhis2">DHIS2</a>: publiek gezondheidsmanagementsysteem voor landen</p>
              </li>
              <li>
                <p>
                  <a href="https://github.com/NationalSecurityAgency/ghidra">Ghidra</a>: software reverse engineering
                  tool
                </p>
              </li>
            </ul>
          </component-section>
          <component-section>
            <div slot="title">Onderzoeken</div>
            <component-section>
              <div slot="title">Reikwijdte (onderzoeksprojectgrenzen) en randvoorwaarden</div>
              <p>
                De fase in softwarelevenscyclus waarbinnen je onderzoekt heeft invloed welke methoden je toe kan passen:
              </p>
              <ul>
                <li>
                  <p><em>Ontwikkelfase</em>: benchmarking.</p>
                </li>
                <li>
                  <p><em>Onderhoudsfase en operationeel</em>: profiling.</p>
                </li>
              </ul>
              <p>Natuurlijk kan je ook beide fases meenemen, als dat binnen je onderzoeksopdracht past.</p>
              <p><em>Separation of concerns</em>: besturingssysteem vs. de onderzochte software-systemen.</p>
              <p>Welke randvoorwaarden? Ga je uit van bepaalde hardware?</p>
              <p>
                Welke constraints (grenzen aan software-oplossing) gelden? Hieruit kunnen hardware en
                besturingssysteemgrenzen volgen.
              </p>
            </component-section>
            <component-section>
              <div slot="title">Welke componenten onderzoeken</div>
              <p>
                Afhankelijk van welke fase van de softwarelevenscyclus je wilt onderzoeken, kan je als aanknopingspunten
                de volgende zaken nemen:
              </p>
              <ul>
                <li>
                  <p>Ontwikkeling</p>
                  <ul>
                    <li>
                      <p>(Niet-performance) tests</p>
                    </li>
                  </ul>
                </li>
                <li>
                  <p>Operationeel</p>
                  <ul>
                    <li>
                      <p>Use-cases</p>
                      <p>Networked apps (bijv. webapps): monitoren van (productie)variant.</p>
                    </li>
                    <li>
                      <p>Bouwstraat</p>
                    </li>
                  </ul>
                </li>
              </ul>
            </component-section>
          </component-section>
          <component-section>
            <div slot="title">Performance tests schrijven</div>
            <p>
              Kan al met simple timing info
              <a href="https://www.baeldung.com/junit-test-execution-time#intellij%20junit"
                >geproduceerd door bijv. het JUnit test framework en/of IDE</a
              >.
            </p>
            <p>
              Kan nog beter met specifiek benchmarking framework (<em>harness</em>), zoals
              <a href="http://tutorials.jenkov.com/java-performance/jmh.html">JMH</a>. Voordelen:
            </p>
            <ul>
              <li>
                <p>
                  Kan optimalisaties door compiler onder controle houden, waardoor ‘nutteloze’ broncode niet
                  weggeoptimaliseerd word.
                </p>
              </li>
              <li>
                <p>Hanteert nauwkeurige timers.</p>
              </li>
              <li>
                <p>Herhaalt metingen automatisch, en geeft relevante statistieken (mediaan bijv.).</p>
              </li>
            </ul>
          </component-section>
          <component-section>
            <div slot="title">Algoritme-analyse vs. andere vormen van performance onderzoek</div>
            <ul>
              <li>
                <p>Abstract vs. concreet</p>
              </li>
              <li>
                <p>Algemene waarheid (fundamenteel onderzoek) vs. waarheid onder systeem (praktijkonderzoek)</p>
              </li>
              <li>
                <p>Schaalbaarheid vs. exacte prestatie in praktijkscenario.</p>
              </li>
            </ul>
          </component-section>
          <component-section>
            <div slot="title">
              Relatie tussen prestatie-efficiëntie (‘performance’) en andere kwaliteitseigenschappen
            </div>
            <ul>
              <li>
                <p><em>Schaalbaarheid</em>: algo-analyse nuttig!</p>
              </li>
              <li>
                <p><em>Onderhoudbaarheid</em>: hoe lang tests duren.</p>
              </li>
              <li>
                <p>
                  <em>Gebruikersvriendelijkeheid</em>: als gebruikers te lang moeten wachten denken ze dat ze iets niet
                  goed doen of dat er een probleem is opgetreden.
                </p>
              </li>
            </ul>
          </component-section>
          <component-section>
            <div slot="title">Scenario ontwerpen om te onderzoeken</div>
            <p>Normale vs. extreme scenario’s = gemiddelde geval vs. slechtste geval én grootte invoerdata.</p>
            <p>Data speelt een cruciale rol, zeker bij algo-analyse.</p>
            <p>Welke grootheid meet je? Geheugen vs. looptijd?</p>
            <p>Referentiedataset (test inputs).</p>
          </component-section>
          <component-section>
            <div slot="title">Resultaten</div>
            <p>Ruwe meetgegevens vs. rapportage</p>
            <p>Watervaldiagram</p>
            <p>Samenvattende statistieken vs. profile (= detailstatistieken)</p>
          </component-section>
          <component-section>
            <div slot="title">Tips</div>
            <p>
              Begin met de eenvoudigste performance tests. Beter een compleet onderzoek dan een perfect maar incompleet
              onderzoek!
            </p>
            <p>
              Neem ook een evt. <em>frontend</em> (webgebaseerde gebruikersinterface) mee. Verscheidene metrics in de
              developer tools van browser die te maken hebben met vertragingen in de GUI. Voel je in je onderzoek niet
              gebonden aan één programmeertaal, zoals waar de backend in geschreven is.
            </p>
            <p>
              Zoek naar gericht naar het gebruik van datastructuren, zeker die waarvan je al weet dat die niet efficiënt
              zijn (geïmplementeerd). Bijv.
              <code class="lang-java">Stack</code> uit Java standard library.
            </p>
          </component-section>
        </component-section>
        <component-section class="opdracht">
          <div slot="title">Werken aan Onderzoek naar performance (20:30)</div>
        </component-section>
      </component-section>`,
    document.getElementById('lessonRecursion'),
  );
